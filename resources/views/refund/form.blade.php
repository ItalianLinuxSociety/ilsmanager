<h2>Dati del rimborso</h2>

<div class="form-group row">
    <label for="amount" class="col-sm-4 col-form-label">Titolo</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="title" value="{{ $object ? $object->title : '' }}" placeholder="Rimborso per ...">
        <p>
            Inserisci un breve <b>titolo</b> che riassuma a tutti la tua necessità o il tuo scopo.<br />
            <small class="form-text text-muted">Chi può leggere questo campo: tutti.</small>
        </p>
    </div>
</div>
<div class="form-group row">
    <label for="date" class="col-sm-4 col-form-label">Data</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="date" value="{{ $object ? $object->date : date('Y-m-d') }}" required>
        <p>
            Inserisci una <b>data</b> indicativa di quando avrai bisogno di fare questa spesa.<br />
            Se non sai cosa inserire, chiarisci pure in descrizione.<br />
            Ti ringraziamo se stai pianificando la cosa con un certo anticipo!<br />
            <small class="form-text text-muted">Chi può leggere questo titolo: tutti.</small>
        </p>
    </div>
</div>
<div class="form-group row">
    <label for="amount" class="col-sm-4 col-form-label">Ammontare</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" step="0.01" name="amount" value="{{ $object ? $object->amount : '' }}" required @readonly( $object && !$object->refundStatus->isEditableByUser($currentuser) )>
        <p>
            Inserisci il <b>totale</b> della tua stima di spesa massima rimborsabile prevista, IVA inclusa.
            Se la tua spesa prevede più articoli o più preventivi, qui
            puoi inserire semplicemente il totale previsto. Potrai allegare
            le singole ricevute o i singoli scontrini nell'apposito campo.<br />
            <small class="form-text text-muted">Chi può leggere questo ammontare: tutti.</small>
        </p>
    </div>
</div>
@if ($currentuser->hasRole('admin'))
    <div class="form-group row">
        <label for="user_id" class="col-sm-4 col-form-label">Utente</label>
        <div class="col-sm-8">
            @include('user.select', ['name' => 'user_id', 'select' => ($object ? $object->user_id : $currentuser->id)])
            <p>
                <small class="form-text text-muted">Chi può leggere questo campo: tutti.</small>
            </p>
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
    <div class="col-sm-8">
        @include('section.select', ['name' => 'section_id', 'select' => ($object ? $object->section_id : 0)])
        <p>
            Se il tuo progetto coinvolge una <b>sezione locale</b> già esistente,
            selezionala, così potrai ricevere ulteriore revisione da loro, e accedere
            al loro fondo cassa. Tutto questo, solitamente, velocizza la tua richiesta.<br />
            <small class="form-text text-muted">Chi può leggere questo campo: tutti.</small>
        </p>
    </div>
</div>
<div class="form-group row multiple-files">
    <label for="file_quote" class="col-sm-4 col-form-label">Preventivi / Documentazione</label>
    <div class="col-sm-8">
        @if ($object)
            <ul>
                @foreach ($object->attachments_quotes as $attach)
                    <li><a href="{{ route('refund.quote.download', [ 'id' => $object->id, 'file' => $attach ] ) }}">{{ basename($attach) }}</a></li>
                @endforeach
            </ul>
        @endif

        <input type="file" name="file_quote[]" autocomplete="off" />
        <p>
        <small class="form-text text-muted">Si prega di allegare tutta la documentazione correlata: preventivi, stime, offerte...</small>
        <small class="form-text text-muted">Chi può accedere a questi preventivi: segreteria, referenti sezione locale.</small>
        </p>
    </div>
</div>
<!-- During creation, do not ask about receipts.
     First reason: to do not clogger the UX.
     Second reason: we encourage users in waiting for approvals first :D
       instead of pay-then-ask.
 -->
@if ($object)
<div class="form-group row multiple-files">
    <label for="file" class="col-sm-4 col-form-label">Ricevute / Fatture</label>
    <div class="col-sm-8">
        @if ($object)
            <ul>
                @foreach ($object->attachments_receipts as $attach)
                    <li><a href="{{ route('refund.receipt.download', [ 'id' => $object->id, 'file' => $attach ] ) }}">{{ basename($attach) }}</a></li>
                @endforeach
            </ul>
        @endif

        <input type="file" name="file_receipt[]" autocomplete="off" />
        <small class="form-text text-muted">Si prega di allegare tutta la documentazione correlata: fatture, scontrini, biglietti, ecc.</small>
        <small class="form-text text-muted">Chi può accedere a queste ricevute: segreteria, referenti sezione locale.</small>
    </div>
</div>
@endif
<div class="form-group row">
    <label for="notes" class="col-sm-4 col-form-label">Note richiedente</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="notes" placeholder="Scrivere qui una breve descrizione delle spese affrontate e di come desideri ricevere il rimborso (fornisci un IBAN, ecc.)" required>{{ $object ? $object->notes : '' }}</textarea>
        <p>
            <small class="form-text text-muted">Chi può leggere le tue note: segreteria, referenti sezione locale.</small>
        </p>
    </div>
</div>

@if ($object)
<hr />

<div class="form-group row">
    <label for="report_url" class="col-sm-4 col-form-label">Relazione Finale (URL)</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="report_url" placeholder="https://" value="{{ $object ? $object->report_url : '' }}" />
        <p>La relazione finale è una breve e graziosa pagina web che descriva a tutti come si è svolta la vostra attività. La puoi pubblicare su un qualsiasi sito (o forum ecc.) per descrivere cosa è andato bene, cosa sarebbe potuto andare meglio, come sono stati effettivamente utilizzati i fondi e descrivendo i risultati attesi e i risultati ottenuti. Non dimenticare qualche bella foto (fai punti bonus se sono in licenza libera!) e non dimenticarti di dare i giusti crediti e ringraziamenti alle persone e organizzazioni che hanno partecipato e vi hanno sostenuto moralmente o economicamente.</p>
        <p>
            <small class="form-text text-muted">Chi può leggere la relazione finale: tutti.</small>
        </p>
    </div>
</div>

<h2>Dati dalla Segreteria</h2>

<div class="form-group row">
    <label for="admin_notes" class="col-sm-4 col-form-label">Note Segreteria</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="admin_notes" placeholder="Informazioni dell'amministratore riguardo lo stato di avanzamento del rimborso" {{ $currentuser->hasRole('admin') ? '' : 'readonly' }}>{{ $object ? $object->admin_notes : '' }}</textarea>
    </div>
</div>
@endif
