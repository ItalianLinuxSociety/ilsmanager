@php
$start_of_year = $movement
  ? $movement->start_of_year
  : (int)request()->input('start_of_year', 0);

$bank_id = ($movement && $movement->bank)
  ? $movement->bank->id
  : (int)request()->input('bank_id', null);

$bank = $movement->bank ?? null;
if (!$bank && $bank_id) {
    $bank = App\Bank::where('id', $bank_id)->firstOrFail();
}

$date = $movement
  ? $movement->date
  : (string)request()->input('date', null);

// Allow to put an edit link for each Movement.
// Maybe undesired in single view. Maybe desired in review.
$readonly = $readonly ?? false;
$editlink = $editlink ?? false;
@endphp

<ul class="list-group">
    <li class="list-group-item">
        <div class="row">

            @if ($movement->id && $editlink)
            <div class="col-md-12">
                <h3><a href="{{ route('movement.edit', $movement->id) }}" title="Modifica Movimento {{$movement->id}}"><i class="fa-solid fa-pencil"></i> Movimento {{$movement->id}}</a></h3>
                <hr />
            </div>
            @endif

            @if (!$movement->id && $start_of_year)
                <input type="hidden" name="start_of_year" value="{{ $start_of_year }}" />
            @endif

            @if (!$movement->id && $bank_id)
                <input type="hidden" name="bank_id" value="{{ $bank_id }}" />
            @endif

            @if ($start_of_year)
            <div class="col-md-12">
                <div class="alert alert-info" role="alert">
                  <h3>Saldo iniziale al 1° gennaio {{ $start_of_year }} per cassa {{ $bank->name }}</h3>
                  <p>Questo è un movimento speciale da poter compilare manualmente e che rappresenta il saldo della cassa <b>{{ $bank->name }}</b> al 1° gennaio {{ $start_of_year}} ore 00:00:00.
                     Nei dettagli vanno indicate le relative voci di entrata e uscita del {{ $start_of_year - 1 }}, fra cui il totale quote associative {{ $start_of_year -1 }}, totale spese di spedizione {{ $start_of_year -1 }}, ecc. esattamente come dovrai presentarlo all'assemblea per fare approvare il trascorso bilancio {{ $start_of_year - 1 }}.
                  </p>
                </div>
            </div>
            @endif

            <div class="col-md-3">
                @if ($movement->id)
                    {{ $movement->date }}<br />
                    <small>{{ $movement->bank ? $movement->bank->name : '' }} · <code>{{$movement->identifier ?? ''}}</code></small>
                @else
                    <input type="text" name="date" placeholder="YYYY-MM-DD" value="{{ $date }}" />
                @endif
            </div>

            <div class="col-md-3">
                @if ($movement->id)
                    @if ($movement->amount_original !== null)
                        <s title="Importo presente nella transazione originaria">{{ $movement->amount_original_formatted }} {{ $movement->currency_original }}</s>
                        ·
                    @endif

                    <span title="Importo effettivamente presente sul nostro conto corrente - dopo aver applicato commissioni bancarie, ecc.">{{ $movement->amount_formatted }}</span>
                @else
                    <!-- Movement Amount. Pay attention to do not collide the name with the AccountRow's amount. -->
                    <input type="number" step="0.01" name="amount" placeholder="0.0" />
                @endif
            </div>
            <div class="col-md-6">
                @if ($movement->id)
                    <input type="text" readonly disabled value="{{ $movement->notes }}" placeholder="Causale" class="form-control" title="Causale originale" />
                @else
                <!-- Movement Notes. Pay attention to do not collide the name with the AccountRow's Notes. -->
                    <input type="text" name="notes" value="{{ $movement->notes }}" placeholder="Causale" />
                @endif

                <!-- Remove Movement: start -->
                @if( $movement->id && empty($movement->identifier) && !$readonly )
                <div class="form-check float-right text-right">
                    <p>
                    <input class="form-check-input" type="checkbox" name="remove[]" value="{{ $movement->id }}" id="remove{{ $movement->id }}">
                    <label class="form-check-label" for="remove{{ $movement->id }}">
                        Elimina
                    </label>
                    </p>
                </div>
                @endif
                <!-- Remove Movement: stop -->
            </div>
        </div>
    </li>

    <!-- Account Rows Sum warning: start -->
    @if( $movement && !$movement->findStubAccountRow() && !$movement->isAmountMatchingAccountRows() )
    <li class="list-group-item">
        <div class="alert alert-danger account-rows-sum-danger" role="alert">
          <p><b>Errore Sommatoria</b></p>
          <p>Correggere gli importi delle sottostanti righe contabili, affinché il loro totale corrisponda a quello del movimento bancario.<br />
             Scarto rilevato:
             <b class="account-rows-amount-diff-error">{{ sprintf('%.02f', $movement->getAccountRowsAmountDiffError() ) }}</b> EUR
          </p>
        </div>
    </li>
    @endif
    <!-- Account Rows Sum warning: end -->

    @if ($movement)
        <!-- Always show all AccountRow(s). Some of them are just proposed and unsaved. -->
        @foreach($movement->account_rows as $ar)
            @include('accountrow.editblock', [
                'ar' => $ar,
                'readonly' => $readonly,
            ])
        @endforeach
    @else
        @include('accountrow.editblock', ['ar' => null])
    @endif

    @if(!$readonly)
    <li class="list-group-item">
        <button class="btn btn-default btn-sm add-row"><i class="fa-solid fa-plus"></i></button>
        <ul class="hidden">
            @include('accountrow.editblock', ['ar' => null])
        </ul>
    </li>
    @endif
</ul>
