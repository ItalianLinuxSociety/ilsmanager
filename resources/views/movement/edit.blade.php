@extends('layouts.app')

@section('content')
<div class="container">

    @if ($object)
    <div class="row">
        <div class="col-lg-10">
            @if ($object->id)
                <h2>Modifica Movimento {{$object->id}}</h2>
            @else
                <h2>Crea Movimento</h2>
            @endif
        </div>
        <div class="col-lg-2">
            @include('movement.reviewcounter')
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('movement.update', ($object && $object->id) ? $object->id : 0) }}">
                @method('PUT')
                @csrf

                @include('movement.editblock', ['movement' => $object])

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            @if ($object && $object->id)
            <div class="card mt-3">
                <div class="card-header">Allegati</div>

                <div class="card-body">
                    <ul>
                        @foreach($object->attachments as $attach)
                            <li><a href="{{ route('movement.download', [ 'id' => $object->id, 'file' => $attach ]) }}">{{ basename($attach) }}</a></li>
                        @endforeach
                    </ul>

                    <form method="POST" action="{{ route('movement.attach', $object->id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <input type="file" class="form-control" name="file">
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary">Allega File</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif

            @if ($object && $object->amount > 0)
                <div class="card mt-3">
                    <div class="card-header">Ricevuta</div>

                    <div class="card-body">
                        @if($object->receipt)
                            <a class="btn btn-default" href="{{ $object->receipt->link }}">Scarica Ricevuta {{ $object->receipt->full_number }}</a>
                        @else
                            <form method="POST" action="{{ route('receipt.store') }}">
                                @csrf

                                <input type="hidden" name="movement_id" value="{{ $object->id }}">

                                <div class="form-group row">
                                    <label for="date" class="col-sm-4 col-form-label">Data</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="date" value="{{ date('Y-m-d') }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="header" class="col-sm-4 col-form-label">Intestazione</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="header"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="causal" class="col-sm-4 col-form-label">Causale</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="causal"></textarea>
                                    </div>
                                </div>

                                @if($object->amount >= 77.47)
                                    <div class="form-group row">
                                        <label for="stamp" class="col-sm-4 col-form-label">Marca da Bollo</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="stamp">
                                        </div>
                                    </div>
                                @endif

                                <hr>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Genera Ricevuta</button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            @elseif ($object && $object->id)
                <div class="card mt-3">
                    <div class="card-header">Rimborsi Spese</div>

                    <div class="card-body">
                        @if($object->refund->isEmpty() == false)
                            <ul>
                                @foreach($object->refund as $r)
                                    <li><a href="{{ route('refund.edit', $r->id) }}">{{ $r->printable_description }}</a></li>
                                @endforeach
                            </ul>
                        @else
                            <?php $pendings = App\Refund::where('refunded', false)->whereDoesntHave('movement')->get() ?>

                            @if($pendings->isEmpty())
                                <div class="alert alert-info">
                                    Non ci sono rimborsi spese in attesa.
                                </div>
                            @else
                                <form method="POST" action="{{ route('movement.refund', $object->id) }}">
                                    @csrf

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <legend class="col-form-label col-sm-4 pt-0">Rimborsi in Attesa</legend>
                                            <div class="col-sm-8">
                                                @foreach($pendings as $p)
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="refund_id[]" value="{{ $p->id }}">
                                                        <label class="form-check-label">
                                                            {{ $p->printable_description }}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Rimborsa</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
            @endif


            @if ($object->supportsRefresh())
            <div class="card mt-3">
                <div class="card-header">Strumenti</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('movement.refresh', $object->id ) }}">
                            @csrf
                            <button type="submit" class="btn btn-primary">Ricarica da PayPal</button>
                       </form>
                   </div>
                </div>
           </div>
           @endif

        </div>
    </div>
</div>
@endsection
