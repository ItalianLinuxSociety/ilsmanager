@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                @foreach(App\Bank::orderBy('name')->where('type', '<>', 'paypal')->get() as $bank)
                    <!-- start form upload movements file -->
                    <form class="form" method="post" action="{{ route('movement.store') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="store_from" value="file" />
                        <input type="hidden" name="bank_id" value="{{ $bank->id }}" />

                        <div class="form-group">
                            <p>Import da file <b>{{$bank->name}}</b> (ultimo aggiornamento: <a href="{{ route('movement-detailed.index') }}?bank_id={{ $bank->id }}&year={{ $year }}&with_account_rows=on&order_dir=desc" target="_blank">{{ $bank->last_update }}</a>):</p>
                            <p><input type="file" name="file" /></p>
                            <p><button type="submit" class="btn btn-primary">Importa File {{ $bank->name }}</button></p>
                        </div>
                    </form>
                    <hr />
                    <!-- end form upload movements file -->
                @endforeach

                <!-- start form PayPal API -->
                @if(PayPalAPIMovementsReader::hasValidConfig())
                    <form class="form" method="post" action="{{ route('movement.store') }}">
                        @csrf
                        <input type="hidden" name="store_from" value="paypal_api" />

                        <div class="form-group row">
                            <label for="date-from" class="col-sm-4 col-form-label">Da (opzionale)<br /><small>default: giorno prima dell'ultimo movimento</small></label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" name="date_from" id="date-from" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date-to" class="col-sm-4 col-form-label">A (opzionale)<br /><small>default: oggi</small></label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" name="date_to" id="date-to" />
                            </div>
                        </div>

                        <!--
                        If you want to use this thing, also decomment snippet from app/Actions/ImportPayPalMovements.php
                        <div class="form-group row">
                            <label for="debug-superlol-activator" class="col-sm-4 col-form-label">Attivare Debug Cosmico? (utile per generare unit test)</label>
                            <div class="col-sm-8">
                                <input type="checkbox" class="form-control" name="debug_superlol_activator" id="debug-superlol-activator" />
                            </div>
                        </div>
                        -->

                        <button type="submit" class="btn btn-primary">Importa da API PayPal</button>
                    </form>
                @endif
                <!-- end form PayPal API -->

            </div>
            <div class="col-md-3">
                @include('movement.reviewcounter')
            </div>
        </div>

        <hr>

        <nav class="nav mt-3">
            @foreach(App\Movement::selectRaw('YEAR(date) as year')->distinct()->orderBy('year', 'asc')->pluck('year') as $nav_year)
                <a class="nav-link{{ $year == $nav_year ? ' active font-weight-bold' : '' }}"
                   href="{{ route('movement.index', ['year' => $nav_year, 'user_id' => request('user_id'), 'section_id' => request('section_id')]) }}">
                    {{ $nav_year }}
                </a>
            @endforeach
        </nav>

        <form action="{{ route('movement.index') }}" method="get" class="form-inline mt-3">
            <input type="hidden" name="year" value="{{ request('year', date('Y')) }}">
            <div class="form-group mb-2">
                <label for="user_id">Socio:</label>
                <select class="form-control mx-sm-3" name="user_id" id="user_id">
                    <option value="">Tutti</option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}"{{ request('user_id') == $user->id ? ' selected' : '' }}>{{ $user->printableName }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2">
                <label for="section_id">Sezione Locale:</label>
                <select class="form-control mx-sm-3" name="section_id" id="section_id">
                    <option value="">Tutte</option>
                    @foreach($sections as $section)
                        <option value="{{ $section->id }}"{{ request('section_id') == $section->id ? ' selected' : '' }}>{{ $section->city }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Filtra</button>
        </form>

        <canvas id="chart"></canvas>

        <div class="row mt-3">
            <div class="col-md-12">
                <h2>Riepilogo Entrate/Uscite {{$year}}</h2>
                {!! $financialSummary->render() !!}
            </div>
        </div>

        @php
            // Find banks without an initial movement for this year, to quickly create them.
            $banks_without_start_of_year = App\Bank::query()
              ->whereDoesntHave('movements', function( $query ) use ( $year ) {
                $query->where('start_of_year', $year);
              } )
              ->get();
        @endphp
        @if (!$banks_without_start_of_year->isEmpty())
            <div class="row mt-4">
                <div class="col-md-12">
                    <h2>Creazione Saldo Iniziale 1 gennaio {{ $year }}</h2>
                    <p>Non è noto il saldo iniziale al 1 gennaio {{ $year }}. Inserisci ora i relativi dati:</p>
                    @foreach ($banks_without_start_of_year as $bank_without_start_of_year)
                        <p><a href="{{ route( 'movement.create', [
                            'bank_id'       => $bank_without_start_of_year->id,
                            'date'          => "$year-01-01",
                            'notes'         => sprintf( "Saldo iniziale al 1 gennaio %d in %s", $year, $bank_without_start_of_year->name ),
                            'start_of_year' => $year,
                       ] ) }}" class="btn btn-secondary">Crea Saldo Iniziale {{ $year }} in {{ $bank_without_start_of_year->name }}</a></p>
                    @endforeach
                </div>
            </div>
        @endif

        @php
            // Find start of year special movements.
            $movements_with_start_of_year = App\Movement::query()
              ->where('start_of_year', $year)
              ->get();
        @endphp
        @if (!$movements_with_start_of_year->isEmpty())
            <div class="row mt-4">
                <div class="col-md-12">
                    <h2>Saldo Iniziale 1 gennaio {{ $year }}</h2>
                    <p>I seguenti movimenti rappresentano il saldo iniziale al 1 gennaio {{ $year }} nei rispettivi conti correnti:</p>
                    <table class="table dataTable">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Cassa</th>
                            <th>Saldo Iniziale</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($movements_with_start_of_year as $movement_with_start_of_year)
                            <tr>
                                <td><a href="{{ route( 'movement.edit', $movement_with_start_of_year->id ) }}"><i class="fa-solid fa-pencil"></i></a></td>
                                <th>{{ $movement_with_start_of_year->bank->name }}</th>
                                <td>{{ $movement_with_start_of_year->amount_formatted }} EUR</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

        <div class="row mt-4">
            <div class="col-md-12">
                <h2>Dettagli Entrate/Uscite {{$year}}</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Valore</th>
                        <th>Account</th>
                        <th>Note</th>
                        <th>Visualizza</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($objects as $row)
                        <tr class="{{
                          $row->movement->start_of_year
                            ? 'table-secondary'
                            : (($row->amount_in > 0)
                                ? 'table-success'
                                : 'table-danger')
                        }}">
                            <td>{{ $row->movement->date }}</td>
                            <td>{{ $row->amount_formatted }}</td>
                            <td>{{ $row->account ? $row->account->printable_name : 'Utente non definito' }}</td>
                            <td>{{ $row->notes }}</td>
                            <td><a href="{{ route('movement.edit', $row->movement->id) }}"><i class="fa-solid fa-pencil"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <script>
        const labels = ['saldo iniz.', 'gen', 'feb', 'mar', 'apr', 'mag', 'giu', 'lug', 'ago', 'set', 'ott', 'nov', 'dic'];
        const data = {
            labels: labels,
            datasets: [
                {
                    label: 'Entrate reali',
                    data: @json( array_values($real_incomes) ),
                    borderColor: '#00aa00',
                    backgroundColor: 'rgba(50, 170, 50, 0.8)',
                },
                {
                    label: 'Entrate previste',
                    data: @json( array_values($forecast_incomes) ),
                    borderColor: '#00aa00',
                    backgroundColor: 'rgba(50, 170, 50, 0.3)',
                },
                {
                    label: 'Uscite reali',
                    data: @json( array_values($real_costs) ),
                    borderColor: '#aa0000',
                    backgroundColor: 'rgba(190, 30, 30, 0.8)',
                },
                {
                    label: 'Uscite previste',
                    data: @json( array_values($forecast_costs) ),
                    borderColor: '#aa0000',
                    backgroundColor: 'rgba(190, 30, 30, 0.3)',
                },
                {
                    label: 'Saldo reale',
                    data: @json( array_values($real_revenue) ),
                    borderColor: '#4490e7',
                    type: 'line',
                    borderDash: [20, 20],
                    backgroundColor: 'rgba(14, 45, 81, 0.4)',
                },
                {
                    label: 'Saldo previsto',
                    display: false,
                    data: @json( array_values($forecast_revenue) ),
                    borderColor: '#4490e755',
                    type: 'line',
                    borderDash: [20, 20],
                    backgroundColor: 'rgba(14, 45, 81, 0.1)',
                }
            ]
        };

        const config = {
            type: 'bar',
            data: data,
            options: {
                responsive: true
            },
        };

        addEventListener("DOMContentLoaded", (event) => {
            const ctx = document.getElementById('chart');

            new Chart(ctx, config);
        });

    </script>
@endsection
