@component('mail::message')

Siamo spiacenti di informarti che la tua iscrizione a {{ App\Config::getConfig('association_name') }} è stata sospesa.

@if ($last_fee && $last_fee_date)
L'ultima quota pagata copre soltanto l'anno {{$last_fee->year}}, pagata in data {{$last_fee_date->format('Y-m-d')}}.
@endif

@include('email.commons.how_to_pay_user_fee', [
    'user' => $user,
])

@endcomponent
