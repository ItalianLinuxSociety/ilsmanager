@component('mail::message')

# Convocazione assemblea di {{ App\Config::getConfig('association_name') }}

Gentile <b>{{ $userDisplayName }}</b>,\
La prossima assemblea di {{ App\Config::getConfig('association_name') }} è convocata per il giorno <b>{{ $assemblyDate }}</b>. Per favore segna la tua partecipazione nel gestionale.

**<{{ $assemblyLink }}>**

----

{!! $assemblyText !!}

----

## Informazioni

Stai ricevendo questo messaggio perché potresti essere in regola con la quota associativa e potresti avere diritto di voto ma non sei nell'elenco dei partecipanti.

Ti chiediamo di segnare la tua partecipazione. Se non puoi partecipare, ti chiediamo di delegare un'altra persona (puoi farlo sempre dalla stessa pagina) altrimenti potremmo non raggiungere il quorum necessario e potremmo dover convocare una nuova assemblea.
Potresti ricevere altre notifiche se non segnerai la tua presenza e se non delegherai nessuno.

Rivedi anche i tuoi dati personali affinché siano aggiornati. Rivedi anche lo stato della tua quota associativa.

## Come partecipare

Ricordati di segnare la tua partecipazione, oppure delega un'altra persona, direttamente da questa pagina. Grazie!

**<{{ $assemblyLink }}>**

@endcomponent
