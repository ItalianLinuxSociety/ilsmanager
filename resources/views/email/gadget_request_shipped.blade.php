<!-- resources/views/email/gadget_request_shipped.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>I tuoi gadget sono stati spediti</title>
</head>
<body>
<h1>I tuoi gadget sono stati spediti!</h1>
<p>Siamo lieti di informarti che la tua richiesta di gadget è stata spedita all'indirizzo da te indicato:</p>
<ul>
    <li><strong>Nome:</strong> {{ $gadgetRequest->name }}</li>
    <li><strong>Cognome:</strong> {{ $gadgetRequest->surname }}</li>
    <li><strong>Indirizzo:</strong> {{ $gadgetRequest->street }}</li>
    <li><strong>CAP:</strong> {{ $gadgetRequest->postal_code }}</li>
    <li><strong>Città:</strong> {{ $gadgetRequest->city }} ({{ $gadgetRequest->province }})</li>
</ul>
<p>È possibile che arrivino tramite una lettera con posta ordinaria, quindi ti chiediamo un po' di pazienza e di fiducia nel servizio postale.</p>
<p>Nel frattempo, considera una donazione, oggi, per permetterci di continuare ad erogare questo bellissimo servizio,
   nonostante i costi che affrontiamo e il tanto tempo libero che investiamo in queste attività. Grazie per la tua generosità di oggi.<br />
   <a href="{{ App\Config::getConfig('donation_url') }}">Dona a {{ App\Config::getConfig('association_name') }}</a></p>
</body>
</html>
