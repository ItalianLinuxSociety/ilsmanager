<html>
    <head>
        <style>

        .header {
            font-size: 0.7em;
            height: 120px;
        }

        .header img {
            float: left;
            width: 100px;
            height: 100px;
        }

        </style>
    </head>
    <body>
        <div class="header">
            <img src="{{ public_path('images/logo_pdf.png') }}" style="margin-right:10px;">
            <p>
                <strong>{{ App\Config::getConfig('association_name') }}</strong><br>
                {!! nl2br(App\Config::getConfig('association_address')) !!}
            </p>
        </div>

        <h2>Ricevuta {{ $object->number }} del {{ date('d/m/Y', strtotime($object->date)) }}</h2>

        <p>
            È stata ricevuta la somma di <strong>euro {{ $object->movement->amountOriginalOrAnyFormatted }}</strong>,
            tramite <strong>{{ $object->movement->bank->name }}</strong> con causale:<br><br>
            {!! nl2br($object->causal) !!}
        </p>

        <br>

        @if(!empty($object->stamp))
            <p>
                Imposta di bollo assolta sull'originale<br>
                ID {{ $object->stamp }}
            </p>
        @endif

        <br>
        <br>
        <br>
        <br>

        <p>
            <i>Documento non fiscale, a quietanza del pagamento.</i>
        </p>
    </body>
</html>
