@php
$is_section_editable = $is_section_editable ?? false;
@endphp

<h3>Utenza e accesso</h3>
<div class="form-group row">
    <label for="user-name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" id="user-name" value="{{ $object ? $object->name : '' }}" {{ $object && $object->type == 'guest' ? '' : 'required' }}>
    </div>
</div>
<div class="form-group row">
    <label for="user-surname" class="col-sm-4 col-form-label">Cognome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="surname" id="user-surname" value="{{ $object ? $object->surname : '' }}" {{ $object && $object->type == 'guest' ? '' : 'required' }}>
    </div>
</div>

<div class="form-group row">
    <label for="username" class="col-sm-4 col-form-label">Nickname</label>
    <div class="col-sm-8">
        @if($currentuser->hasRole('admin'))
            <input type="text" class="form-control" name="username" id="username" value="{{ $object ? $object->username : '' }}">
        @else
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->username : '' }}">
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label">Password</label>
    <div class="col-sm-8">
        <input type="password" class="form-control" name="password" id="password" placeholder="Lascia in bianco per non modificare" autocomplete="off">
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">E-Mail</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" id="email" value="{{ $object ? $object->email : '' }}" required>
    </div>
</div>

<h3 class="mt-4">Contatti diretti</h3>
<div class="form-group row">
    <label for="phone" class="col-sm-4 col-form-label">Telefono</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="phone" id="phone" value="{{ $object ? $object->phone : '' }}">
        <small class="form-text text-muted">Il telefono è condiviso esclusivamente con la segreteria per contattarti per questioni importanti (e.g. assemblee) in caso non si riesca a raggiungerti dalla tua casella di posta.</small>
    </div>
</div>
<div class="form-group row">
    <label for="website" class="col-sm-4 col-form-label">Sito Web</label>
    <div class="col-sm-8">
        <input type="url" class="form-control" name="website" id="website" value="{{ $object ? $object->website : '' }}">
        <small class="form-text text-muted">Se il sito è dotato di feed RSS, sarà automaticamente incluso in <a href="https://planet.linux.it/ils/">Planet ILS</a>. L'indice viene aggiornato una volta alla settimana.</small>
    </div>
</div>

@if(App\Config::getConfig('custom_email_aliases') == '1')
    <h3 class="mt-4">Casella di posta</h3>
    <div class="alert alert-secondary">
        Tutte le persone associate hanno a disposizione una casella di posta ma non è fornito alcun backup.
    </div>
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">E-Mail Personale</legend>
            <div class="col-sm-8">
                <div class="form-check">
                    <label class="form-check-label" for="alias">
                        <input class="form-check-input" type="radio" name="email_behaviour" id="alias" value="alias" {{ $object && $object->getConfig('email_behaviour') ? 'checked' : '' }}>
                            Alias di posta: le email saranno forwardate all'indirizzo sopra configurato
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label" for="email_behaviour">
                        <input class="form-check-input" type="radio" id="email_behaviour" name="email_behaviour" value="inbox" {{ $object && $object->getConfig('email_behaviour') == 'inbox' ? 'checked' : '' }}>
                        {!! nl2br(App\Config::getConfig('custom_email_aliases_message')) !!}
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="email_password" class="col-sm-4 col-form-label">Password E-Mail Personale</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" name="email_password" id="email_password" placeholder="Lascia in bianco per non modificare">
        </div>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-8">
            <small class="form-text text-muted">Se scegli di avere una inbox locale, devi definire una password. Questa <strong>non</strong> corrisponde alla password di ILSManager.</small>
            <small class="form-text text-muted">Queste configuzioni vengono applicate sul mail server ogni 3 ore: se le modifichi, aspetta un poco prima di provarle!</small>
        </div>
    </div>

@endif

<h3 class="mt-4">Informazioni legali</h3>

<fieldset class="form-group">
    <div class="row">
        <legend class="col-form-label col-sm-4 pt-0">Entità legale</legend>
        <div class="col-sm-8">
            @foreach(App\LegalEntity::orderByDefault()->get() as $legal_entity)
                <div class="form-check">
                    <label class="form-check-label" for="legal-entity-{{ $legal_entity->slug }}">
                        <input class="form-check-input" type="radio" name="legal_entity_id" id="legal-entity-{{ $legal_entity->slug }}" value="{{ $legal_entity->id }}" @checked( $object && $object->legal_entity_id == $legal_entity->id )>
                            {{ $legal_entity->name }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>
</fieldset>

@if (App\Section::exists())
    <div class="form-group row">
        <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
        <div class="col-sm-8">
            @include('section.select', [
                'select' => $object ? $object->section_id : 0,
                'name' => 'section_id',
                'disabled' => !$is_section_editable,
                'id' => 'section_id',
            ])

            @if (!$is_section_editable)
            <div class="alert alert-info mt-2">
                <p>La tua utenza non può cambiare sezione locale di riferimento in autonomia. Per cambiarla, <a href="mailto:{{App\Config::getConfig('association_email')}}">contatta la segreteria</a>. Grazie!</p>
            </div>
            @endif
        </div>
    </div>

    <hr>
@endif

<div class="form-group row">
    <label for="taxcode" class="col-sm-4 col-form-label">Codice Fiscale</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="taxcode" id="taxcode" value="{{ $object ? $object->taxcode : '' }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="birth_place" class="col-sm-4 col-form-label">Luogo di Nascita</label>
    <div class="col-sm-5">
        <input type="text" class="form-control" name="birth_place" id="birth_place" value="{{ $object ? $object->birth_place : '' }}" required>
    </div>
    <div class="col-sm-3">
        @include('commons.provinces', ['name' => 'birth_prov', 'value' => $object ? $object->birth_prov : null, 'required' => true])
    </div>
</div>
<div class="form-group row">
    <label for="birth_date" class="col-sm-4 col-form-label">Data di Nascita</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="birth_date" id="birth_date" value="{{ $object ? $object->birth_date : '' }}" placeholder="YYYY-MM-DD" required>
    </div>
</div>

<hr>

<div class="form-group row">
    <label for="address_street" class="col-sm-4 col-form-label">Indirizzo di Residenza</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="address_street" id="address_street" value="{{ $object ? $object->address_street : '' }}" placeholder="e.g. Via Roma 42" required>
    </div>
</div>
<div class="form-group row">
    <label for="address_place" class="col-sm-4 col-form-label">Città di Residenza</label>
    <div class="col-sm-3">
        <input type="text" class="form-control" name="address_place" id="address_place" value="{{ $object ? $object->address_place : '' }}" placeholder="Città" required>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control" name="address_zip" value="{{ $object ? $object->address_zip : '' }}" placeholder="CAP" required>
    </div>
    <div class="col-sm-3">
        @include('commons.provinces', ['name' => 'address_prov', 'value' => $object ? $object->address_prov : null, 'required' => true])
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-4">Iscrizione Registro Volontari</label>
    <div class="col-sm-8">
        <div class="form-check">
            @php
            $volunteer_checked = $object && $object->volunteer;
            @endphp
            <label>
                <input class="form-check-input" type="radio" name="volunteer" value="1" @checked($volunteer_checked)>
                Sì, voglio che il mio nome sia presente nel Registro dei Volontari
                perché intendo organizzare in prima persona attività in modo personale, spontaneo, gratuito
                <b>e</b> richiedo copertura assicurativa personale (a spese aggiuntive coperte dall'organizzazione)
                contro infortuni connessi alla mia attività di volontariato
                <b>e</b> confermo che <b>non</b> riceverò alcuna remunerazione in alcun modo (neanche indirettamente) per queste mie attività.
                Scelgo questa preferenza in particolare se sono referente di una sezione locale o membro del consiglio direttivo.
                <span class="text-muted">La segreteria ti farà sapere quando questa tua richiesta sarà accolta
                e quando il tuo nome sarà inserito effettivamente nel Registro dei Volontari.</span>
            </label>
            <label>
                <input class="form-check-input" type="radio" name="volunteer" value="0" @checked(!$volunteer_checked)>
                No, non c'è bisogno di iscrivermi anche nel Registro dei Volontari. Grazie.
            </label>
        </div>
    </div>
</div>

<h3 class="mt-4">Preferenze di spedizione</h3>
<p>Se è stata annunciata una spedizione massiva di gadget a tutte le persone associate, puoi decidere se inserire qui i tuoi dati di spedizione.
   In caso contrario lascia pure in bianco questi campi, per minimizzare i dati che l'organizzazione deve gestire e proteggere. Grazie!
</p>
<div class="card">
    <div class="card-body">
        <div class="form-group row">
            <label for="shipping_name" class="col-sm-4 col-form-label">Spedizione Presso</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="shipping_name" id="shipping_name" value="{{ $object ? $object->shipping_name : '' }}" placeholder="e.g. c/o Azienda XYZ">
            </div>
        </div>
        <div class="form-group row">
            <label for="shipping_street" class="col-sm-4 col-form-label">Indirizzo di Spedizione</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="shipping_street" id="shipping_street" value="{{ $object ? $object->shipping_street : '' }}" placeholder="e.g. Via Roma 42">
            </div>
        </div>
        <div class="form-group row">
            <label for="shipping_place" class="col-sm-4 col-form-label">Città di Spedizione</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="shipping_place" id="shipping_place" value="{{ $object ? $object->shipping_place : '' }}" placeholder="Città">
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control" name="shipping_zip" value="{{ $object ? $object->shipping_zip : '' }}" placeholder="CAP">
            </div>
            <div class="col-sm-3">
                @include('commons.provinces', ['name' => 'shipping_prov', 'value' => $object ? $object->shipping_prov : null])
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-4 col-sm-8">
                <small class="form-text text-muted">Se l'indirizzo di spedizione non viene esplicitato, viene usato quello di residenza.</small>
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label for="tshirt-size" class="col-sm-4 col-form-label">Taglia</label>
            <div class="col-sm-8">
                <select name="size" id="tshirt-size" class="form-control">
                    <option value="none">Non Selezionato</option>
                    @foreach(['S', 'M', 'L', 'XL', 'XXL'] as $size)
                        <option value="{{ $size }}" {{ $object && $object->size == $size ? 'selected' : '' }}>{{ $size }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<hr>

@if($currentuser->hasRole('admin'))
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Tipologia di iscrizione</legend>
            <div class="col-sm-8">
                @foreach(App\UserType::defaults_by_identifier() as $type)
                    <div class="form-check">
                        <label class="form-check-label" for="{{ $type->identifier }}">
                            <input class="form-check-input" type="radio" name="type" id="{{ $type->identifier }}" value="{{ $type->identifier }}" {{ $object && $object->type == $type->identifier ? 'checked' : '' }}>
                                {{ $type->label }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Stato</legend>
            <div class="col-sm-8">
                @foreach(App\User::statuses() as $status)
                <div class="form-check">
                        <label class="form-check-label" for="{{ $status->identifier }}">
                            <input class="form-check-input" type="radio" name="status" id="{{ $status->identifier }}" value="{{ $status->identifier }}" {{ $object && $object->status == $status->identifier ? 'checked' : '' }}>
                                {{ $status->label }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Ruolo</legend>
            <div class="col-sm-8">
                @foreach(App\Role::orderBy('name', 'asc')->get() as $role)
                <div class="form-check">
                <label class="form-check-label" for="{{ $role->id }}">
                            <input class="form-check-input" type="checkbox" name="roles[]" id="{{ $role->id }}" value="{{ $role->id }}" {{ $object && $object->roles()->where('roles.id', $role->id)->first() ? 'checked' : '' }}>
                                {{ $role->name }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="notes" class="col-sm-4 col-form-label">Note</label>
        <div class="col-sm-8">
            <textarea class="form-control" name="notes" id="notes">{{ $object ? $object->notes : '' }}</textarea>
        </div>
    </div>

    <hr>

    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Registrazione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="request_at" value="{{ $object ? $object->request_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>
    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Approvazione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="approved_at" value="{{ $object ? $object->approved_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>
    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Espulsione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="expelled_at" value="{{ $object ? $object->expelled_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>

    @if($object)
        <hr>
        <a href="{{ route('user.bypass', $object->id) }}" class="btn btn-danger">Impersona</a>
    @endif
@else
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tipo</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->human_type : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Stato</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->human_status : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Data di Registrazione</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->request_at : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Data di Approvazione</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->approved_at : '' }}">
        </div>
    </div>
@endcan
