@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\User::class)
        <div class="row">
            <div class="col-md-12">
                <h6>I soci vengono approvati una volta l'anno durante l'assemblea per via dello statuto, l'accettazione delle quote é manuale e avviene periodicamente verificando i versamenti sul conto bancario o PayPal.</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Registra Nuovo</button>
                <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Socio</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('user.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('user.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

            <div class="row mt-3">
                <div class="col-12 col-md-3">
                    <!-- TODO: create a special filter for this virtual status.
                         This count includes:
                            active
                            +
                            pending with at least one recent fee
                     -->
                    <div class="ils-card-call-to-action">
                        <div class="card" style="background: #d1ecf1">
                            <div class="card-body">
                                 <h5 class="card-title">Tutte le utenze attive</h5>
                                 <h4>{{ $allUsersThatCanLoginCount }}</h4>
    	                      </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-3">
                    <a class="ils-card-call-to-action" href="{{ url()->current() }}?status=pending#filters">
                        <div class="card" style="background: #fff3cd">
                            <div class="card-body">
                                 <h5 class="card-title"><abbr title="Soci in attesa di approvazione da parte dell'assemblea">In attesa</abbr></h5>
                                 <h4>{{ $pendingUsersCount }}</h4>
    	                      </div>
                        </div>
                    </a>
                </div>

                <div class="col-12 col-md-3">
                    <a class="ils-card-call-to-action" href="{{ url()->current() }}?status=active#filters">
                        <div class="card" style="background: #d4edda">
                            <div class="card-body">
                                 <h5 class="card-title"><abbr title="Tutti i soci, associazioni comprese, in regola, quindi con diritto di voto">Soci</h5>
                                 <h4>{{ $activeUsersCount }}</h4>
    	                      </div>
                        </div>
                    </a>
                </div>

                @foreach ($activeLegalEntityCounts as $legal_entity_count)
                <div class="col-12 col-md-3">
                    <a class="ils-card-call-to-action" href="{{ url()->current() }}?status=active&legal_entity={{ $legal_entity_count->legalEntity->slug }}">
                        <div class="card" style="background: #d4edda">
                            <div class="card-body">
                                 <h5 class="card-title">soci di tipo <abbr title="{{ $legal_entity_count->legalEntity->name_plural }}"><b>{{ $legal_entity_count->legalEntity->name_short }}</b></abbr></h5>
                                 <h4>{{ $legal_entity_count->count }}</h4>
    	                      </div>
                        </div>
                    </a>
                </div>
                @endforeach

            </div>

    @if($currentuser->hasRole('admin'))

            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="user-charts" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#age-users" role="tab" aria-controls="age-users" aria-selected="true">Età soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  href="#prov" role="tab" aria-controls="prov" aria-selected="false">Province dei soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sections" role="tab" aria-controls="sections" aria-selected="false">Sezioni locali</a>
                                </li>
                            </ul>
                        </div>

                        @php
                        $year = array();
                        $by_year = array();
                        $prov = array();
                        $by_prov = array();
                        $section_by = array();
                        $section = array();
                        $section_by = array();
                        foreach(App\User::where('status', 'active')->where('type', 'regular')->with('section')->get() as $user) {
                            $_year = date('Y', strtotime($user->birth_date));
                            $year[$_year] = $_year;
                            if (!isset($by_year[$_year])) {
                                $by_year[$_year] = 1;
                            } else {
                                $by_year[$_year]++;
                            }

                            $_prov = $user->birth_prov;
                            if ($user->address_prov) {
                                $_prov = $user->address_prov;
                            }
                            $prov[$_prov] = $_prov;

                            if (!isset($by_prov[$_prov])) {
                                $by_prov[$_prov] = 1;
                            } else {
                                $by_prov[$_prov]++;
                            }

                            $_section = $user->section->city ?? null;
                            if ($_section) {
                                $section[$_section] = $_section;

                                if (!isset($section_by[$_section])) {
                                    $section_by[$_section] = 1;
                                } else {
                                    $section_by[$_section]++;
                                }
                            }
                        }

                        ksort($section_by);
                        ksort($section);
                        ksort($by_prov);
                        ksort($prov);
                        ksort($by_year);
                        ksort($year);

                        @endphp

                        <div class="card-body">
                            <div class="tab-content mt-3">
                                <div class="tab-pane active" id="age-users" role="tabpanel">
                                    <div>
                                        <canvas id="age-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('age-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $year ) ),
                                                datasets: [{
                                                    label: '# per anno di nascita',
                                                    data: @json( array_values( $by_year ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="prov" role="tabpanel">
                                    <div>
                                        <canvas id="prov-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('prov-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $prov ) ),
                                                datasets: [{
                                                    label: '# per provincia',
                                                    data: @json( array_values( $by_prov ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="sections" role="tabpanel">
                                    <div>
                                        <canvas id="sections-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('sections-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $section ) ),
                                                datasets: [{
                                                    label: '# per sezione',
                                                    data: @json( array_values( $section_by ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <form method="GET">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="user_status" class="mr-2">Filtra per stato</label>
                                <select name="status" class="form-control" id="user_status">
                                    <option value="-" @selected( !$statusFilter )>Tutti</option>
                                    @foreach(\App\User::statuses() as $status)
                                        <option value="{{ $status->identifier }}" @selected( $statusFilter === $status->identifier )>{{ $status->label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="user_type" class="mr-2">Filtra per tipo di iscrizione</label>
                                <select name="type" class="form-control" id="user_type">
                                    <option value="" @selected( empty($typeFilter) )>Tutti</option>
                                    @foreach(\App\UserType::defaults() as $userType)
                                        <option value="{{ $userType->identifier }}" @selected( $userType->identifier === $typeFilter )>{{ $userType->label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="legal_entity" class="mr-2">Filtra per tipo di entità legale</label>
                                <select name="legal_entity" class="form-control" id="legal_entity">
                                    <option value="" @selected( $legalEntityFilter === null )>Tutte</option>
                                    <option value="-" @selected( $legalEntityFilter === false )>Valore assente</option>
                                    @foreach(App\LegalEntity::orderByDefault()->get() as $legal_entity)
                                    <option value="{{ $legal_entity->slug }}" @selected( $legalEntityFilter && $legalEntityFilter->id === $legal_entity->id )>
                                        {{ $legal_entity->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="user_section" class="mr-2">Filtra per sezione</label>
                                <select name="section"  class="form-control" id="user_section">
                                    <option value="" {{ $sectionFilter == null ? 'selected' : '' }}>Tutte</option>
                                    <option value="-none-" @selected( $sectionFilter === '-none-' )>Nessuna</option>
                                    @foreach(\App\Section::query()->orderBy('city')->get() as $section)
                                        <option value="{{ $section->id }}" @selected( $sectionFilter == $section->id )>{{ $section->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="residence_prov" class="mr-2">Filtra per provincia residenza</label>
                                <select name="residence_prov"  class="form-control" id="residence_prov">
                                    <option value="" {{ $residenceProvFilter == null ? 'selected' : '' }}>Tutte</option>
                                    @foreach(allProvinces() as $prov)
                                        <option value="{{ $prov[0] }}" {{ $residenceProvFilter == $prov[0] ? 'selected' : '' }}>{{ $prov[1] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="role">Filtra per Ruolo</label>
                                <select name="role" id="role" class="form-control">
                                    <option value="">Tutti</option>
                                    @foreach(App\Role::get() as $role)
                                        <option value="{{ $role->name }}" {{ request('role') == $role->name ? 'selected' : '' }}>
                                        {{ ucfirst($role->name) }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="col pt-4">
                                <button type="submit" class="btn btn-primary mb-2">Filtra</button>
                            </div>
                    </form>

                        </div>

                </div>
                <a name="filters"></a>
                <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cognome</th>
                                        <th>Nickname</th>

                                        <!-- Legal entity.
                                             Show only if we are filtering by this value,
                                             because we have not infinite space in the page.
                                        -->
                                        @if ($legalEntityFilter)
                                        <th>Entità legale</th>
                                        @endif

                                        <th>Sezione</th>
                                        <th>Provincia residenza</th>

                                        @if($currentuser->hasRole('admin'))
                                            <th>Email</th>
                                            <th>Quota</th>
                                            <th>Stato</th>
                                            <th></th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($objects as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->surname }}</td>
                                            <td>{{ $user->expelled_at ? 'ESPULSO' : $user->username }}</td>

                                            <!-- Legal entity.
                                                 Show only if we are filtering by this value,
                                                 because we have not infinite space in the page.
                                            -->
                                            @if ($legalEntityFilter)
                                            <td>
                                                @if ($user->legalEntity)
                                                    <abbr title="{{ $user->legalEntity->name }}">{{ $user->legalEntity->name_short }}</abbr>
                                                @endif
                                            </td>
                                            @endif

                                            <td>{{ $user->section ? $user->section->city : '' }}</td>
                                            <td>{{ $user->address_prov }}</td>

                                            @if($currentuser->hasRole('admin'))
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->fees()->max('year') }}</td>
                                                <td>{{ $user->human_status }}</td>
                                                <td><a href="{{ route('user.edit', $user->id) }}"><i class="fa-solid fa-pencil"></i></a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3">
                    {{ $objects->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
