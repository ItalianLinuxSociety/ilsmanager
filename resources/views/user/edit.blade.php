@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="POST" action="{{ route('user.update', $object->id) }}">
                @method('PUT')
                @csrf

                @include('user.form', ['object' => $object])

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            @if ($currentuser->hasRole('admin') && $object->status == 'pending')
                <form method="POST" action="{{ route('user.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger">Elimina</button>
                        </div>
                    </div>
                </form>
            @endif
        </div>
        <div class="col-md-3 offset-md-1">
            <h5>Quote Versate</h5>

            <!-- Have we fees? -->
            @if ($object->fees()->exists())
                <ul class="list-group">
                    @foreach ($object->fees()->orderBy('year', 'desc')->get() as $fee)
                        <li class="list-group-item">
                            {{ $fee->year }}

                            @if ($currentuser->hasRole('admin') && isset($fee->account_row))
                                <a href="{{ route('movement.edit', $fee->account_row->movement_id) }}" title="Modifica Movimento {{ $fee->account_row->movement_id }}"><i class="fa-solid fa-pencil"></i></a>
                            @endif

                            @if ($fee->receipt())
                                <span class="float-right"><a href="{{ $fee->receipt()->link }}">Ricevuta {{ $fee->receipt()->fullNumber }}</a></span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @else
                @if ($object->actsLikeMember)

                    <!-- Banner 'You have probably paid recently': start -->
                    @if ($probably_recently_paid)
                        @include('commons/you-have-recently-paid-please-wait')
                    @endif
                    <!-- Banner 'You have probably paid recently': end -->

                    <!-- Be scary with members without a Fee! lol -->
                    <div class="alert alert-danger">
                        <p>Nessuna quota versata sinora.</p>

                        @if ($paypal_fee_url)
                        <div><a class="btn btn-primary {{ $probably_recently_paid ? 'disabled' : '' }}" href="{{ $paypal_fee_url }}" title="Versa quota associativa con PayPal" target="_blank">Versa quota con PayPal</a></div>
                        @endif
                    </div>
                @else
                    <!-- Be welcoming with people that are NOT supposed to be members. -->
                    <div class="alert alert-info">
                        <p>Non hai richiesto di associarti. In ogni caso, puoi farlo compilando i dati del tuo profilo e versando la quota associativa.</p>
                        @if ($paypal_fee_url)
                        <div><a class="btn btn-primary" href="{{ $paypal_fee_url }}" title="Versa quota associativa con PayPal" target="_blank">Versa quota con PayPal</a></div>
                        @endif
                    </div>
                @endif
            @endif

            <h5 class="mt-4">Donazioni Effettuate</h5>
            @if ($object->donations()->exists())
                <ul class="list-group">
                    <!-- TODO: order by movement.date -->
                    @foreach ($object->donations()->orderBy('movements.date', 'desc')->get() as $donation)
                        <li class="list-group-item">
                            @if ($donation->movement && $donation->movement->receipt)
                                <span class="float-right">
                                    <a href="{{ $donation->movement->receipt->link }}">Ricevuta {{ $donation->movement->receipt->number }}</a>
                                </span>
                                @endif
                            {{ $donation->movement->date->format('d-m-Y') }}

                            @if ($currentuser->hasRole('admin'))
                                <a href="{{ route('movement.edit', $donation->movement->id) }}" title="Modifica Movimento {{ $donation->movement->id }}"><i class="fa-solid fa-pencil"></i></a>
                            @endif
                            <br>

                            &euro; {{ $donation->movement->amountOriginalOrAnyFormatted }}
                        </li>
                    @endforeach
                </ul>
            @endif
            <div class="alert alert-secondary">
                In questo spazio archiviamo con cura le ricevute delle tue ulteriori donazioni spontanee,
                per esempio quando usi la funzione <a href="{{ $paypal_donate_url ?? '#' }}">dona con PayPal</a>.
            </div>
        </div>
    </div>
</div>
@endsection
