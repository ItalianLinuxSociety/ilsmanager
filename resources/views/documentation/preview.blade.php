@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item {{ empty($folder) ? 'active' : '' }}"><a href="{{ route('doc.index') }}">Documentazione</a></li>
                    <?php $accumulate = [] ?>
                    @foreach(explode('/', $folder) as $bread)
                        <?php $accumulate[] = $bread ?>
                        @if(empty($bread))
                            @continue
                        @endif
                        <li class="breadcrumb-item"><a href="{{ route('doc.index', ['folder' => join('/', $accumulate)]) }}">{{ $bread }}</a></li>
                    @endforeach
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <h1>{{ $file }} <a class="icon-right" href="{{ route('doc.download', ['file' => '/' . $file]) }}"><i class="fa-solid fa-download"></i></a></h1>
            <hr>
            {!! $content !!}
        </div>
    </div>
</div>
@endsection
