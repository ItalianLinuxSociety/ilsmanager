@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Nuova Richiesta di Spedizione Gadget</h1>

        <div>
            <p>Potremmo avere ancora a disposizione qualche gadget da poter spedire direttamente a casa tua (o ad un indirizzo di spedizione comodo per te) a spese nostre.</p>
            <p>Ti ringraziamo per il tuo aiuto nel diffondere questi materiali nelle tue cerchie. Ricordati che la maggior parte dei materiali
               che riceverai è progettato, prodotto e spedito nel nostro tempo libero, e a spese nostre, da persone volontarie.
               Grazie per il tuo tempo per valorizzare al meglio questi materiali.</p>
            <p>Compila i dati della tua richiesta di spedizione. Inserisci un tuo indirizzo di ricezione valido.</p>
        </div>

        <form action="{{ route('gadget-request.store') }}" method="POST">
            @csrf

            @include('gadget-request.form', ['object' => $default_object, 'gadgetTypes' => $gadgetTypes])

            <button type="submit" class="btn btn-primary">Invia Richiesta</button>
        </form>
    </div>
@endsection
