@php
$assembly = $object;
@endphp

@extends('layouts.app')

@section('content')
<div class="container">

    <h2>{{ $assembly->title }}</h2>

    @if ($assembly->isOpen() || $assembly->isPending())
    @cannot('proveRightToVote', \App\Assembly::class)
        <div class="alert alert-warning" role="alert">
            Non hai i requisiti di voto per votare in un'assemblea.
        </div>
    @endcannot
    @endif

    @if ($assembly->isPending())
        <div class="alert alert-info" role="alert">
            Questa assemblea non è ancora aperta. Probabilmente è ancora presto.
        </div>
    @elseif ($assembly->isClosed())
        @can('update', $assembly)
            <div class="alert alert-warning" role="alert">
                Questa è un'assemblea conclusa. In qualità di amministratore, puoi modificarla ma solo se sai cosa stai facendo (e.g. per correggere ovvi refusi).
            </div>
        @else
            <div class="alert alert-info" role="alert">
                Questa è un'assemble conclusa.
            </div>
        @endcan
    @endif

                    <div class="card">
                        <div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-6">

                                        @include('assembly.votations', ['assembly' => $assembly])

                                        @can('update', $assembly)
                                            <form method="POST" action="{{ route('assembly.update', $assembly->id) }}">
                                                @method('PUT')
                                                @csrf

                                                @include('assembly.form', ['object' => $assembly])

                                                <div class="form-group row">
                                                    <div class="col-sm-10">
                                                        <button type="submit" class="btn btn-primary">Salva</button>
                                                    </div>
                                                </div>
                                            </form>

                                            <hr>

                                            @include('assembly.managevotations', ['assembly' => $assembly])
                                        @else
                                            <p>
                                                {!! $assembly->announceRendered !!}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        @can('participate', $assembly)
                                            <div class="row mb-3">
                                                <div class="col-md-12">
                                                    <p>Per poter votare in questa assemblea devi segnalare la tua partecipazione oppure delegare un'altra persona. Se si vuole delegare ma non si può partecipare alla assemblea si può revocare la partecipazione e assegnare la delega.</p>
                                                    @if ($user_participation)
                                                        <form method="POST" action="{{ route('assembly.unpartecipate', $assembly->id) }}">
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger {{ Auth::user()->can('unparticipate', $assembly) ? '' : 'disabled' }}">Revoca Partecipazione</button>
                                                            <!-- We don't do security through obscurity. The button is always here. Backend is secure. -->
                                                        </form>
                                                    @else
                                                        <form method="POST" action="{{ route('assembly.partecipate', $assembly->id) }}">
                                                            @csrf
                                                            <button type="submit" class="btn btn-primary {{ Auth::user()->can('participate', $assembly) ? '' : 'disabled' }}">Segnala Partecipazione</button>
                                                            <!-- We don't do security through obscurity. The button is always here. Backend is secure. -->
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p>Partecipanti totali: <b>{{ count($assembly->users) }}</b></p>
                                                    <ul class="list-group">
                                                        @foreach ($assembly_users as $participant)
                                                            <li class="list-group-item">
                                                                {{ $loop->iteration }} &middot;

                                                                @can('update', $participant)
                                                                   <a href="{{ route('user.edit', $participant->user_id) }}" target="_blank">
                                                                   {{ $participant->user->printableName }}
                                                                   </a>
                                                                @else
                                                                   {{ $participant->user->printableName }}
                                                                @endcan

                                                                @if (!$user_participation && Auth::user()->can('delegate', [ $assembly, $participant->user ]))
                                                                    <form method="POST" action="{{ route('assembly.delegate', ['assembly_id' => $assembly->id, 'delegate_id' => $participant->user_id]) }}" class="float-right">
                                                                        @csrf
                                                                        <button type="submit" class="btn">Delega</button>
                                                                    </form>
                                                                @elseif($participant->delegate)
                                                                    tramite delega a
                                                                    @can('update', $participant->delegate)
                                                                        <a href="{{ route('user.edit', $participant->delegate->id) }}" target="_blank">
                                                                            {{ $participant->delegate->printableName }}
                                                                        </a>
                                                                    @else
                                                                        {{ $participant->delegate->printableName }}
                                                                    @endcan
                                                                @endif

                                                                @if (!$assembly->isClosed())
                                                                <div>
                                                                    @include( 'assembly/vote-capability-badges', [
                                                                        'participant' => $participant,
                                                                        'assembly' => $assembly,
                                                                    ] )
                                                                </div>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @else
                                            <!-- Else: you cannot participate. -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3>Presenze</h3>
                                                    <ul class="list-group">
                                                        @foreach ($assembly_users as $participant)
                                                            <li class="list-group-item">
                                                                {{ $loop->iteration }} &middot; {{ $participant->user->printableName }}

                                                                @if ($participant->delegate)
                                                                    tramite delega a {{ $participant->delegate->printableName }}
                                                                @endif

                                                                @if (!$assembly->isClosed())
                                                                <div>
                                                                    @include( 'assembly/vote-capability-badges', [
                                                                        'participant' => $participant,
                                                                        'assembly' => $assembly,
                                                                    ] )
                                                                </div>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
</div>
@endsection
