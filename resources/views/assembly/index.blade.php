@extends('layouts.app')

@section('content')
<div class="container">

    @can('create', App\Assembly::class)
        <div class="row">
            <div class="col-md-12 mb-3">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createAssembly">Registra Nuova</button>
                <div class="modal fade" id="createAssembly" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">Convoca Assemblea</h3>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('assembly.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('assembly.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <h2 class="mt-3">Tutte le Assemblee</h2>

    @foreach($objects as $assembly)
        <div class="card mt-3">
            <div class="card-header">
                <h3>
                    <a href="{{ route('assembly.show', $assembly->id) }}">{{ $assembly->title }}</a>

                    @if ($assembly->isOpen())
                       · assemblea aperta
                    @endif
                </h3>
            </div>
            <div class="card-body">
                {!! $assembly->announceRendered !!}
            </div>
        </div>
    @endforeach

    <div class="pagination-wrapper">
        {{ $objects->appends(request()->query())->links() }}
    </div>
</div>
@endsection
