@if ($participant->delegate)
    @include( 'assembly/vote-capability-badge', [
        'user' => $participant->user,
        'assembly' => $assembly,
        'userRoleLabel' => __("delegante"),
    ])
    @include( 'assembly/vote-capability-badge', [
        'user' => $participant->delegate,
        'assembly' => $assembly,
        'userRoleLabel' => __("delegato/a"),
    ])
@else
    @include( 'assembly/vote-capability-badge', [
        'user' => $participant->user,
        'assembly' => $assembly,
        'userRoleLabel' => "",
    ])
@endif
