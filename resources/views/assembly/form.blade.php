<div class="form-group row">
    <label for="assembly-date" class="col-sm-4 col-form-label">Data</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="date" id="assembly-date" value="{{ $object ? $object->date->format('Y-m-d') : '' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="assembly-status" class="col-sm-4 col-form-label">Stato</label>
    <div class="col-sm-8">
        <select class="form-control" name="status" id="assembly-status" autocomplete="off" required>
            <option value="pending" @selected( $object && $object->status === 'pending' )>In Attesa</option>
            <option value="open" @selected( $object && $object->status === 'open' )>Aperta</option>
            <option value="closed" @selected( $object && $object->status === 'closed' )>Chiusa</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="assembly-announce" class="col-sm-4 col-form-label">Convocazione</label>
    <div class="col-sm-12">
        <textarea class="form-control" name="announce" id="assembly-announce" rows="10" width="100%" required>{{ $object ? $object->announce : '' }}</textarea>
    </div>
</div>
