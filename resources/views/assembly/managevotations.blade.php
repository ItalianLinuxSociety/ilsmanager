@if($assembly->status != 'closed')
    @foreach($assembly->votations as $votation)
        <p>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editVotation{{ $votation->id }}">Edita Votazione: {{ substr($votation->question, 0, 20) }}...</button>
        </p>

        <div class="modal fade" id="editVotation{{ $votation->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modifica Votazione</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <form method="POST" action="{{ route('votation.update', $votation->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            @include('votation.form', ['object' => $votation])
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Salva</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endif

@can('addVotation', $assembly)
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createVotation">Nuova Votazione</button>
    <div class="modal fade" id="createVotation" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nuova Votazione</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('votation.store') }}">
                    @csrf
                    <div class="modal-body">
                        @include('votation.form', ['object' => null, 'assembly_id' => $assembly->id])
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endcan
