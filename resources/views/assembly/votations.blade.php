<?php $runnings = $assembly->votations()->where('status', 'running')->get() ?>
@if($runnings->isEmpty() == false)
    @foreach($runnings as $running_votation)

        @if($running_votation->hasVoted($currentuser))
            <div class="alert alert-info">
                <div>{!! $running_votation->questionRendered !!}</div>
                <p class="mt-2">Hai già votato per questa votazione in corso. In attesa dei risultati.<br>
                Sono stati espressi <b>{{ $running_votation->votes()->count() }}</b> voti.</p>
            </div>
        @else

            @if ($user_voting_for_me)
                <div class="alert alert-info" role="alert">
                    Hai scelto di delegare il tuo voto a: <b>{{ $user_voting_for_me->printableName }}</b>.
                    Attendi che questa persona esprima il tuo voto dal suo account.
                </div>
            @elseif (!$user_participation)
                @can('participate', $assembly)
                    <div class="alert alert-info" role="alert">
                        Per poter votare, segnala la tua partecipazione all'assemblea.
                    </div>
                @else
                    <!-- The top of the page probably already says that you cannot vote. -->
                @endcan
            @endif

            <!-- Vote button.
                 We do not do security through obscurity.
                 We always show the button and just enable/disable it,
                 accordingly to the backend policy VotationPolicy#vote().
            -->
            <p><button type="button" class="btn {{ Auth::user()->can('vote', $running_votation) ? 'btn-success' : 'disabled' }}" data-toggle="modal" data-target="#runningVotation{{ $running_votation->id }}">Votazione in corso! Vota!</button></p>

            <div class="modal fade" id="runningVotation{{ $running_votation->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Votazione in Corso</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('vote.store') }}">
                            @csrf
                            <div class="modal-body">
                                @include('vote.form', ['votation' => $running_votation])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

    <hr>
@endif

<?php $closed_votations = $assembly->votations()->where('status', 'closed')->get() ?>
@if($closed_votations->isEmpty() == false)
    @foreach($closed_votations as $closed_votation)
        <p>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#closedVotation{{ $closed_votation->id }}">Esito Votazione: {{ substr($closed_votation->question, 0, 20) }}...</button>
        </p>

        <div class="modal fade" id="closedVotation{{ $closed_votation->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Votazione Conclusa</h5>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('votation.result', ['votation' => $closed_votation])
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <hr>
@endif
