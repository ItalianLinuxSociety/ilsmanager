@if ($user->can('proveRightToVote', $assembly))
    <span class="badge badge-info">✅ {{ $userRoleLabel }} ha diritto di voto</span>
@else
    <span class="badge badge-danger">❌ {{ $userRoleLabel }} non ha diritto di voto</span>
@endif
