<tr>
    <td>
        <input type="hidden" name="rule_id[]" value="{{ $rule ? $rule->id : 'new' }}">
        <input type="text" class="form-control" name="rule[]" value="{{ $rule ? $rule->rule : '' }}">
    </td>
    <td>
        @include('account.select', ['select' => $rule ? $rule->account_id : 0])
    </td>
    <td>
        <input type="text" class="form-control" name="notes[]" value="{{ $rule ? $rule->notes : '' }}">
    </td>
    <td>
        <div class="form-check float-right">
            <input class="form-check-input" type="checkbox" name="remove[]" value="{{ $rule ? $rule->id : '' }}" id="remove{{ $rule ? $rule->id : 'new' }}">
            <label class="form-check-label" for="remove{{ $rule ? $rule->id : 'new' }}">
                Elimina
            </label>
        </div>
    </td>
</tr>
