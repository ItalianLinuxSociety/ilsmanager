<!-- This page is:
         'page/how-to-pay.blade.php'.
     Please keep the info in sync with:
         'commons/how_to_pay_user_fee.blade.php'
-->
@extends('layouts.app')

@section('content')

<div class="container">

    <h2>Versare la quota associativa a {{ App\Config::getConfig('association_name') }}</h2>

    <p>Versare annualmente la tua quota associativa ti permette di far parte di {{ App\Config::getConfig('association_name') }} e sostenere nuove attività di volontariato.</p>
    <p>Ti consigliamo di utilizzare lo strumento a te più comodo, fra questi:</p>

    @foreach ($banks as $bank)
    <div class="card">
        <div class="card-body">

        @if ($bank->isPayPal)

            <h3>PayPal</h3>
            <p>Puoi inviare velocemente {{ $fee_amount }} EUR anche con PayPal:</p>
		        <p><a class="btn btn-primary" href="{{ $bank->getPayPalFeeURL( [ 'user' => $user ] ) }}">Paga con {{ $bank->name }}</a>

        @else

            <h3>Bonifico bancario</h3>
            <p>Puoi inviare un bonifico bancario di <b>{{ $fee_amount }} EUR</b> con questa causale consigliata, a questo IBAN:<br />
              <blockquote>"{{ \App\AccountRow::createFeeNote( $user_example ) }}"</blockquote>
              <blockquote><b>{{ $bank->identifier }}</b></blockquote>
            </p>

        @endif

        </div>
    </div>
    @endforeach

    <h3 class="mt-3">Cosa succede dopo il pagamento</h3>

    <p>La tua quota può richiedere fino a {{ \App\Config::getConfig('secretary_fee_maxdays') }} giorni per essere processata.</p>
    <p>Dopo aver pagato, se stai pensando di impostare un ulteriore rinnovo automatico, consigliamo di impostarlo ai primi giorni di ogni gennaio, così riceverai meno notifiche.</p>

    @if (\App\Config::getConfig('secretary_volunteer'))
        <p>Una volta che hai effettuato il pagamento ti chiediamo un po' di pazienza: la segreteria è gestita totalmente nel nostro tempo libero di volontariato.</p>
    @endif

    <p>Se hai già pagato e se sono trascorsi più di {{ \App\Config::getConfig('secretary_fee_maxdays') }} giorni e ancora non trovi la tua ricevuta sul tuo profilo, contattaci:
       <a href="mailto:{{ App\Config::getConfig('association_email') }}">{{ App\Config::getConfig('association_email') }}</a>.
    </p>

</div>

@endsection
