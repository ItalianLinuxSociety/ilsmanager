<ul class="list-group">
    @foreach ($summary_rows as $row)
        @include('partials.financial_summary_row', [
            'row' => $row,
            'deep' => 0,
        ])
    @endforeach

    @include('partials.financial_summary_row_foot', [
        'in' => $total_in,
        'out' => $total_out,
        'sum' => $total_sum,
        'title' => "Totale annuale",
        'description' => "Questi totali <u>non</u> considerano il bilancio di inizio anno."
    ] )

    @if ($include_start_of_year)
        @include('partials.financial_summary_row_foot', [
            'in' => $startofyear_in,
            'out' => $startofyear_out,
            'sum' => $startofyear_sum,
            'title' => "Saldo iniziale",
            'description' => "Questi totali considerano soltanto il bilancio a inizio anno."
        ] )
    @endif

    @if ($include_start_of_year)
        @include('partials.financial_summary_row_foot', [
            'in' => $totalbalance_in,
            'out' => $totalbalance_out,
            'sum' => $totalbalance_sum,
            'title' => "Saldo totale",
            'description' => "Questi totali considerano anche il bilancio a inizio anno ed è quindi il tuo saldo previsto."
        ] )
    @endif
</ul>