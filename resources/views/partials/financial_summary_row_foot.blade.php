<li class="list-group-item">
    <div class="row">
        <div class="col-md-6"><strong>{{ $title }}</strong></div>
        <div class="col-md-2">
            <span class="float-right badge {{ $in === '0.00' ? '' : 'badge-success' }}"><strong>{{ $in }} €</strong></span>
        </div>
        <div class="col-md-2">
            <span class="float-right badge {{ $out === '0.00' ? '' : 'badge-danger' }}"><strong>{{ $out }} €</strong></span>
        </div>
        <div class="col-md-2">
            <span class="float-right"><strong>{{ $sum }} €</strong></span>
        </div>
    </div>
    @if ($description)
        <div class="text-muted">{!! $description !!}</div>
    @endif
</li>