<li class="list-group-item">
    <div class="row {{ $row->selected ? '' : 'text-muted' }}">
        <div class="col-md-{{ 6 - $deep }} offset-md-{{ $deep }}">{{ $row->name }}</div>

        <div class="col-md-2">
            <span class="float-right badge {{ $row->in === '0.00' ? '' : 'badge-success' }}">{{ $row->in }} €</span>
        </div>
        <div class="col-md-2">
            <span class="float-right {{ $row->out === '0.00' ? '': 'badge-danger' }}">{{ $row->out }} €</span>
        </div>
        <div class="col-md-2">
            <span class="float-right">{{ $row->sum }} €</span>
        </div>
    </div>
</li>

@foreach ($row->children as $child)
    @include('partials.financial_summary_row', [
        'row' => $child,
        'deep' => $deep + 1,
    ])
@endforeach