<input type="hidden" name="votation_id" value="{{ $votation->id }}">

<p>
    {!! $votation->questionRendered !!}
</p>
<p>
    Puoi scegliere al massimo
    @if($votation->max_options == 1)
        1 opzione.
    @else
        {{ $votation->max_options }} opzioni.
    @endif
</p>

@foreach($votation->assembly->delegations($currentuser) as $voter)
    <hr>

    <div class="row">
        <div class="col-md-12">
            <p>
                Voto per {{ $voter->printable_name }}
            </p>
        </div>

        <div class="col-md-12 max-selections" data-max-selections="{{ $votation->max_options }}">
            @foreach($votation->options as $option)
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{ $option }}" name="vote_{{ $voter->id }}[]"> {{ $option }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>
@endforeach
