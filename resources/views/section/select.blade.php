<?php
if (!isset($name)){
    $name = 'section[]';
}

if (!isset($select)) {
    $select = null;
}

$attributes = '';
if (!empty($id)) {
    $attributes .= sprintf( ' id="%s"', htmlspecialchars($id));
}
?>

<select class="form-control" name="{{ $name }}" autocomplete="off" @disabled($disabled ?? false) {!! $attributes !!}>
    <option value="" {{ (!$select) ? 'selected' : '' }}>Nessuna</option>
    @foreach(App\Section::orderBy('city', 'asc')->get() as $section)
        <option value="{{ $section->id }}" {{ $select == $section->id ? 'selected' : '' }}>{{ $section->city }} ({{ $section->prov }})</option>
    @endforeach
</select>

