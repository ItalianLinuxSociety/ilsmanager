<!-- The $order_dir can be customized by request()->input('order_dir'),
     and it's designed to be controlled by:
        EditController#getOrderDirection().

     The $available_order_dir is designed to be controlled by:
        EditController#getAvailablesOrderDirections()
-->
<div class="form-group">
    <label for="order_dir">Ordinamento:</label>
    <select id="order_dir" name="order_dir" class="form-control">
        @foreach($availables_order_dir as $k => $v)
            <option value="{{ $k }}" @selected($k === $order_dir)>{{ $v }}</option>
        @endforeach
    </select>
</div>