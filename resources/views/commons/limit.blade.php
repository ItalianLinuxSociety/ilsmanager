<!-- The $limit can be customized by request()->input('limit'),
     and it's designed to be controlled by:
        EditController#getPageElementsLimit()
-->
<div class="form-group">
    <label for="limit">Limite:</label>
    <input type="number" id="limit" name="limit" class="form-control" value="{{ $limit }}">
</div>