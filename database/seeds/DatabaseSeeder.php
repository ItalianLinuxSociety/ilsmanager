<?php

use App\Account;
use App\AccountRow;
use App\AccountRule;
use App\Assembly;
use App\Bank;
use App\Fee;
use App\LegalEntity;
use App\Movement;
use App\Refund;
use App\Role;
use App\Section;
use App\User;
use App\Votation;
use App\Vote;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Cached example bank unicredit.
     */
    private ?Bank $bank_unicredit;

    /**
     * Cached example bank PayPal.
     */
    private ?Bank $bank_paypal;

    /**
     * Cached bank account, to identify a member fee.
     */
    private ?Account $bank_account_userfees;

    /**
     * Cached bank account, to identify a Bank fee .
     */
    private ?Account $bank_account_bankfees;

    /**
     * Cached bank account, to identify a generic donation.
     */
    private ?Account $bank_account_donations;

    /**
     * Cached bank account, to identify a generic donation.
     */
    private ?Account $bank_account_webstuff;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Avoid to sent 1 billion emails.
        // Also because developers may test with stuff like "mailtrap"
        // and some runs of the seeder kills your mailtrap quotas lol.
        Mail::fake();

        // Maximum number of extra nonsense users.
        // Trust me, don't increase this too much :D
        $MAX_NONSENSE_USERS = 100;

        $time = strtotime('-1 year', time());
        $previous_year = date('Y', $time);

        // Be sure to create all these roles.
        $this->getOrCreateRole('admin');
        $this->getOrCreateRole('member'); // Note: this role is probably unused in the app
        $this->getOrCreateRole('referent');
        $this->getOrCreateRole('shipper');

        // Be sure to create all these Bank accounts.
        // We should especially create these:
        //   - Banche (with bank_costs = true)
        //   - Donazioni (with donations = true)
        //   - Quote Associative (with fees = true)
        // Since these are used in the unit tests and by other parts.
        $accounts = [
            'Soci' => [
                'Quote Associative',
            ],
            'Attività Istituzionale' => [
                'Sponsorizzazioni',
                'Donazioni',
            ],
            'Beni e Servizi' => [
                'Stampa',
                'Materiale Promozionale',
                'Posta e Spedizioni',
                'Banche',
                'Commercialista',
                'Consulenze',
                'Servizi Web',
                'Hardware',
            ],
            'Imposte e Tasse' => [],
        ];
        foreach ($accounts as $parent => $sub_accounts) {
            $p = Account::where('name', $parent)->first();
            if (! $p) {
                $p = new Account;
                $p->name = $parent;
                $p->parent_id = null;
                $p->save();
            }
            foreach ($sub_accounts as $sa) {
                $s = Account::where('name', $sa)->first();
                if (! $s) {
                    $s = new Account;
                    $s->name = $sa;
                    $s->parent_id = $p->id;
                    $s->save();
                }
            }
        }
        Account::where('name', 'Quote Associative')->update(['fees' => true]);
        Account::where('name', 'Donazioni')->update(['donations' => true]);
        Account::where('name', 'Banche')->update(['bank_costs' => true]);

        // Cache well-known Bank Accounts to avoid N+1 performance issues.
        $this->bank_account_bankfees = Account::where('name', 'Banche')->firstOrFail();
        $this->bank_account_donations = Account::where('name', 'Donazioni')->firstOrFail();
        $this->bank_account_userfees = Account::where('name', 'Quote Associative')->firstOrFail();
        $this->bank_account_webstuff = Account::where('name', 'Servizi Web')->firstOrFail();

        // Create Bank Unicredit.
        $this->bank_unicredit = Bank::where('type', 'unicredit')->first();
        if (! $this->bank_unicredit) {
            $this->bank_unicredit = new Bank;
            $this->bank_unicredit->name = 'Unicredit';
            $this->bank_unicredit->type = 'unicredit';
            $this->bank_unicredit->identifier = 'IT74G0200812609000100129899';
            $this->bank_unicredit->save();

            $r = new AccountRule;
            $r->bank_id = $this->bank_unicredit->id;
            $r->rule = 'IMPOSTA BOLLO CONTO CORRENTE';
            $r->account_id = $this->bank_account_bankfees->id;
            $r->notes = 'Imposta bollo';
            $r->save();

            // Create example Movement matching AccountRule regex.
            $movement = new Movement;
            $movement->amount = 100;
            $movement->bank_id = $this->bank_unicredit->id;
            $movement->date = $this->createRandomDateTimeInYearButNotInFuture(date('Y'));
            $movement->notes = 'IMPOSTA BOLLO CONTO CORRENTE';
            $movement->save();

            // Do not add the related AccountRow please.
        }

        // Create Bank PayPal.
        $this->bank_paypal = Bank::where('type', 'paypal')->first();
        if (! $this->bank_paypal) {
            $this->bank_paypal = new Bank;
            $this->bank_paypal->name = 'PayPal';
            $this->bank_paypal->type = 'paypal';
            $this->bank_paypal->identifier = 'direttore@linux.it';
            $this->bank_paypal->save();

            $r = new AccountRule;
            $r->bank_id = $this->bank_paypal->id;
            $r->rule = '^Hetzner Online';
            $r->account_id = $this->bank_account_webstuff->id;
            $r->notes = 'VPS';
            $r->save();

            // Create example Movement matching AccountRule regex.
            $movement = new Movement;
            $movement->amount = 200;
            $movement->bank_id = $this->bank_paypal->id;
            $movement->date = $this->createRandomDateTimeInYearButNotInFuture(date('Y'));
            $movement->notes = 'Hetzner Online Fattura';
            $movement->save();

            // Do not add the related AccountRow please.
        }

        // Create example sections.
        $this->getOrCreateAllKnownExampleSections();

        // Create Legal Entities.
        $this->createDefaultLegalEntities();

        // Create Users and some example Movements and AccountRow.
        // Traditional way :D
        if (User::all()->isEmpty()) {
            $user = new User;
            $user->username = 'admin';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Amministratore';
            $user->surname = 'Amministrotti';
            $user->email = 'admin@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = $this->createRandomBirthDate();
            $user->save();
            $this->grantUserRole($user, 'admin');
            $this->generateUserMovementFees($user, 8);
            $user_referent_roma = $user;

            // Create Referent Roma Stuff.
            $movement = new Movement;
            $movement->amount = 150;
            $movement->bank_id = $this->bank_paypal->id;
            $movement->date = $this->createRandomDateTimeInYearButNotInFuture(date('Y'));
            $movement->notes = 'Donazione sezione locale di Roma';
            $movement->save();
            $ar = new AccountRow;
            $ar->movement_id = $movement->id;
            $ar->user_id = $user_referent_roma->id;
            $ar->account_id = $this->bank_account_donations->id;
            $ar->notes = $movement->notes;
            $ar->amount = $movement->amount;
            $ar->section_id = $user_referent_roma->section_id;
            $ar->save();
            $movement->generateReceipt();

            $user = new User;
            $user->username = 'membro.membrotti';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Membro';
            $user->surname = 'Membrotti';
            $user->email = 'member@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = $this->createRandomBirthDate();
            $user->save();
            $this->grantUserRole($user, 'member'); // Note: this role is probably unused in the app
            $user_member = $user;
            $this->generateUserMovementFees($user, 3);

            $user = new User;
            $user->username = 'referent';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Referente';
            $user->surname = 'Referentotti';
            $user->email = 'referent@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = $this->createRandomBirthDate();
            $user->save();
            $this->grantUserRole($user, 'referent');
            $user_referent_milano = $user;
            $this->generateUserMovementFees($user, 4);

            // Referent stuff: Refund.
            $refund = new Refund;
            $refund->amount = 101;
            $refund->user_id = $user_referent_milano->id; // Utente referente
            $refund->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $refund->date = $this->createRandomDateInYearButNotInFuture(date('Y') - 1);
            $refund->refunded = 1;
            $refund->notes = 'Rimborso acquisto cacciaviti per Milano';
            $refund->save();

            // Referent stuff: Movement of that Refund.
            $movement = new Movement;
            $movement->amount = -($refund->amount);
            $movement->bank_id = $this->bank_paypal->id;
            $movement->date = $this->createRandomDateTimeInYearButNotInFuture(date('Y'));
            $movement->notes = 'Rimborso Milano';
            $movement->save();
            $ar = new AccountRow;
            $ar->movement_id = $movement->id;
            $ar->user_id = $user_referent_milano->id;
            $ar->account_id = null; // TODO: set something useful
            $ar->notes = 'Rimborso sede Milano';
            $ar->amount = $movement->amount;
            $ar->section_id = $refund->section_id;
            $ar->save();

            $refund = new Refund;
            $refund->amount = 150;
            $refund->user_id = $user_referent_milano->id;
            $refund->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $refund->date = $this->createRandomDateInYearButNotInFuture(date('Y') - 1);
            $refund->notes = 'Rimborso per acquisto hardware sezione locale di Milano';
            $refund->save();

            $user = new User;
            $user->username = 'attendente.attendotti';
            $user->password = Hash::make($user->username);
            $user->status = 'pending';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Attendente';
            $user->surname = 'Attendotti';
            $user->email = 'pending@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = $this->createRandomBirthDate();
            $user->save();
            $this->grantUserRole($user, 'member'); // Note: this role is probably unused in the app
            $user_pending = $user;
            $this->generateUserMovementFees($user, 1);

            $user = new User;
            $user->username = 'sospeso.sospesotti';
            $user->password = Hash::make($user->username);
            $user->status = 'suspended';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Sospeso';
            $user->surname = 'Sospesotti';
            $user->email = 'suspended@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_suspended = $user;

            $user = new User;
            $user->username = 'espulso.espulsotti';
            $user->password = Hash::make($user->username);
            $user->status = 'expelled';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Espulso';
            $user->surname = 'Espulsotti';
            $user->email = 'expelled@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $user_expelled = $user;

            $user = new User;
            $user->username = 'eliminato.eliminotti';
            $user->password = Hash::make($user->username);
            $user->status = 'dropped';
            $user->type = 'regular';
            $user->legal_entity_id = LegalEntity::getIndividualId();
            $user->name = 'Eliminato';
            $user->surname = 'Eliminotti';
            $user->email = 'dropped@example.com';
            $user->notes = '';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $user_dropped = $user;

            $user = new User;
            $user->username = 'museo.dei.musei';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'association';
            $user->legal_entity_id = LegalEntity::ofSlug('it-associazione-generica-o-culturale')->first()->id;
            $user->name = 'Associazione';
            $user->surname = 'Museo dei Musei';
            $user->email = 'association@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = $this->createRandomBirthDate();
            $user->save();
            $this->grantUserRole($user, 'member'); // Note: this role is probably unused in the app
            $user_association = $user;
            $this->generateUserMovementFees($user, 3);

            // A guest is a minimal user that is not a member, but can login.
            $user = new User;
            $user->name = 'Ospite';
            $user->username = 'guest.guest';
            $user->password = Hash::make($user->username);
            $user->status = 'limited';
            $user->type = 'guest';
            $user->email = 'ospite.ospitato@example.com';
            $user->notes = '';
            $user->request_at = date('Y-m-d');
            $user->save();
        }

        // Create Users and some example Movements and AccountRow.
        // Modern way :D flooding. asd
        $this->generateAndPersistSillyUsers($MAX_NONSENSE_USERS);

        if (Assembly::all()->isEmpty()) {
            $assembly = new Assembly;
            $assembly->date = '2099-12-31';
            $assembly->status = 'pending';
            $assembly->announce = 'Assemblea futura';
            $assembly->save();

            $assembly = new Assembly;
            $date = new DateTime(now());
            $interval = new DateInterval('P1M');
            $date->add($interval);
            $assembly->date = $date->format('Y-m-d');
            $assembly->status = 'open';
            $assembly->announce = 'Assemblea attiva';
            $assembly->save();

            $assembly = new Assembly;
            $assembly->date = '0001-01-01';
            $assembly->status = 'closed';
            $assembly->announce = 'Assemblea chiusa';
            $assembly->save();
        }

        if (Votation::all()->isEmpty()) {
            // TODO: Add some Votations with multiple answers (not just multiple options).
            $votation = new Votation;
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in attesa';
            $votation->status = 'pending';
            $votation->options = ['a', 'b', 'c'];
            $votation->save();

            $votation = new Votation;
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in corso';
            $votation->status = 'running';
            $votation->options = ['d', 'e', 'f'];
            $votation->save();

            $votation = new Votation;
            $votation->assembly_id = 2;
            $votation->question = 'Votazione chiusa';
            $votation->status = 'closed';
            $votation->options = ['g', 'h', 'i'];
            $votation->save();
        }

        if (Vote::all()->isEmpty()) {
            $max_votations = 5;
            $max_users_in_votation = 5;

            // Find votation Candidates.
            $possible_votations = Votation::inRandomOrder()->take($max_votations)->get()->all();
            while ($possible_votation = array_pop($possible_votations)) {

                // Find user candidates.
                $possible_users = User::inRandomOrder()->take($max_users_in_votation)->get()->all();
                while ($possible_user = array_pop($possible_users)) {

                    // Pick a random answer.
                    // TODO: Check if the Votation supports multiple answers.
                    $random_votation_options = $possible_votation->options;
                    shuffle($random_votation_options);
                    $vote_answer = array_slice($random_votation_options, 0, 1);

                    // Create vote.
                    $vote = new Vote;
                    $vote->votation_id = $possible_votation->id;
                    $vote->user_id = $possible_user->id;
                    $vote->answer = $vote_answer;
                    $vote->save();
                }
            }
        }
    }

    /**
     * Create an incredible high amount of users with silly details.
     */
    private function generateAndPersistSillyUsers($max_users)
    {
        // We start a transaction to speedup performances and conclude in 1 second instead of 1 minute.
        // Otherwise MySQL/MariaDB proceedes with autocommit that is super-slow.
        DB::beginTransaction();
        $section_generator_circular = $this->getGeneratorCircularSections();
        $user_generator = $this->generateUsersWithSillyNames($max_users);
        $i = 1;
        foreach ($user_generator as $silly_user) {

            // Pick a Section. This can be NULL.
            $section = $this->nextPlease($section_generator_circular);
            if ($section) {
                $silly_user->section_id = $section->id;
                $silly_user->address_prov = $section->prov;
            }

            Log::debug(sprintf('Creating user [%d/%d] %s',
                $i,
                $max_users,
                $silly_user->username));

            try {
                $silly_user->save();
            } catch (Exception $e) {
                Log::warning(sprintf('Failed creating user %s - probably already created - stop user seeding',
                    $silly_user->username));
                break;
            }

            // This MAY create some fees and MAY create some donations.
            $this->generateRandomUserMovements($silly_user);

            // Eventually randomly promote, like the assembly said "yes" for some users.
            $silly_user = $silly_user->fresh();
            if ($silly_user->regularFee()) {
                if (random_int(0, 1) === 1) {
                    $silly_user->status = 'active';
                    $silly_user->save();
                }
            }

            $i++;
        }
        DB::commit();
    }

    /**
     * Get or create a Role by name.
     *
     * @param  string  $name
     * @return Role
     */
    private function getOrCreateRole($name)
    {
        // Get already-existing role.
        $role = Role::where('name', $name)->first();
        if ($role) {
            return $role;
        }

        // Create Role on-the-fly.
        $role = new Role;
        $role->name = $name;

        return $role->save();
    }

    private function grantUserRole(User $user, string $role_name)
    {
        $role_id = $this->getOrCreateRole($role_name)->id;
        $user->roles()->attach($role_id);
    }

    private function getOrCreateSectionByCityAndProv(string $city, string $prov)
    {

        // Get already-existing Section.
        $section = Section::where('city', $city)->where('prov', $prov)->first();
        if ($section) {
            return $section;
        }

        // Create Section on-the-fly.
        $section = new Section;
        $section->city = $city;
        $section->prov = $prov;

        return $section->save();
    }

    /**
     * This silly method continues to generate Users. You may want to stop.
     */
    private function generateUsersWithSillyNames(int $max_users)
    {
        // Pre-name.
        // Luciana, Gianluciana, ecc.
        $silly_name_pre_prefixes = [
            '',
            'gian',
            'pier',
        ];

        // Name prefix
        // Luciano, Luciana, Poldo, Polda, ecc.
        $silly_name_prefixes = [
            'anastasi',
            'angel',
            'bepp',
            'beat',
            'carmel',
            'fiorenz',
            'focaccin',
            'gin',
            'linuxar',
            'lucian',
            'pold',
            'pollicin',
            'puffett',
            'sandr',
            'santificat',
            'zorr',
        ];

        // Name suffix
        // Luciana, Luciano, Lucianu (lol), ecc.
        $silly_name_suffixes = [
            'a',
            'o',
            'u',
        ];

        // Surname suffix (based on name prefix lol)
        // Lucianacci, Lucianoni, ecc.
        $silly_surname_suffixes = [
            'acci',
            'erletti',
            'i',
            'ini',
            'issimi',
            'ori',
            'oni',
            'otti',
            'ucci',
        ];

        $i = 0;
        foreach ($silly_name_prefixes as $silly_name_prefix) {
            foreach ($silly_name_suffixes as $silly_name_suffix) {
                foreach ($silly_surname_suffixes as $silly_surname_suffix) {
                    foreach ($silly_name_pre_prefixes as $silly_name_pre_prefix) {

                        // Kill-switch.
                        if ($i >= $max_users) {
                            return;
                        }

                        $user = new User;
                        $user->name = ucfirst(strtolower($silly_name_pre_prefix . $silly_name_prefix . $silly_name_suffix));
                        $user->surname = ucfirst(strtolower($silly_name_prefix . $silly_surname_suffix));
                        $user->username = strtolower($user->name . '.' . $user->surname);
                        $user->password = Hash::make($user->username);
                        $user->email = $user->username . '@localhost.local';
                        $user->type = 'regular';
                        $user->legal_entity_id = LegalEntity::getIndividualId();
                        $user->birth_prov = 'FI'; // Just something.
                        $user->address_prov = 'MI'; // Just something
                        $user->birth_date = $this->createRandomBirthDate();
                        $user->request_at = date('Y-m-d');
                        $user->status = 'pending'; // TODO: make this the default in the entity itself
                        $user->created_at = Carbon::now()->subDays(random_int(1, 365 * 10));

                        // DO NOT CHANGE - THE WHOLE PROJECT IS HEAVILY BASED ON THE FOLLOWING TOKEN
                        $user->notes = 'A este usuario le gusta mucho el pingüino ;) ;) y el asd!!';

                        yield $user;
                        $i++;
                    }
                }
            }
        }
    }

    private function generateRandomUserMovements(User $user)
    {
        $max_fees = random_int(0, 10);
        $max_donations = random_int(0, 2);
        $start_year = date('Y');

        // Sometime, create just old movements.
        // So we create just old user activity, and inactive users.
        if (random_int(1, 10) <= 2) {
            $start_year -= random_int(0, 4);
        }

        $this->generateUserMovementFees($user, $max_fees, $start_year);
        $this->generateUserMovementDonations($user, $max_donations, $start_year);
    }

    private function generateUserMovementFees(User $user, int $max_movements, ?int $start_year = null)
    {
        $y = $start_year ?? date('Y');
        for ($i = 0; $i < $max_movements; $i++) {
            $this->generateUserMovementFeeForYear($user, $y--);
        }
    }

    private function generateUserMovementDonations(User $user, int $max_movements, ?int $start_year = null)
    {
        $y = $start_year ?? date('Y');
        for ($i = 0; $i < $max_movements; $i++) {
            $this->generateUserMovementDonationForYear($user, $y--);
        }
    }

    /**
     * Generate a single Movement that pays a donation for a specific year,
     * eventually in a specific Movement.
     */
    private function generateUserMovementDonationForYear(User $user, int $year, ?Movement $movement = null)
    {
        if (! $movement) {
            $movement = new Movement;
            $movement->amount = random_int(1, 100);
            $movement->notes = "Donazione da {$user->printableName}";
            $movement->bank_id = $this->nextPlease($this->getGeneratorCircularBanks())->id;
            $movement->date = $this->createRandomDateTimeInYearButNotInFuture($year);
            $movement->save();
        }

        $ar = new AccountRow;
        $ar->movement_id = $movement->id;
        $ar->account_id = $this->bank_account_donations->id;
        $ar->user_id = $user->id;
        $ar->amount = $movement->getAccountRowsAmountDiffError();
        $ar->notes = $movement->notes;
        $ar->save();

        $movement->generateReceipt();

        // Refresh review flag.
        $movement = $movement->fresh();
        $movement->validate(); // This MUST not explode :D
        $movement->save();
    }

    /**
     * Generate a single Movement that pays a user fee for a specific year.
     */
    private function generateUserMovementFeeForYear(User $user, int $year)
    {
        $ORIGINAL_AMOUNT = $user->feeAmount;
        $TAX = 1.20;
        $RECEIVED_AMOUNT = $ORIGINAL_AMOUNT - $TAX;

        // Pick Bank candidate.
        $bank = $this->nextPlease($this->getGeneratorCircularBanks());

        $movement = new Movement;
        $movement->amount = $RECEIVED_AMOUNT;
        $movement->amount_original = $ORIGINAL_AMOUNT;
        $movement->bank_id = $bank->id;
        $movement->date = $this->createRandomDateTimeInYearButNotInFuture($year);
        $movement->notes = AccountRow::createFeeNote($user, $year);
        $movement->save();

        // User Fee
        $ar = new AccountRow;
        $ar->movement_id = $movement->id;
        $ar->account_id = $this->bank_account_userfees->id;
        $ar->amount = $ORIGINAL_AMOUNT;
        $ar->user_id = $user->id;
        $ar->notes = $movement->notes;
        $ar->save();

        // Bank Fee
        $ar = new AccountRow;
        $ar->movement_id = $movement->id;
        $ar->account_id = $this->bank_account_bankfees->id;
        $ar->amount = -$TAX;
        $ar->notes = "Spese bancarie {$bank->name}";
        $ar->save();

        $movement->generateReceipt();

        // Refresh review flag.
        $movement = $movement->fresh();
        $movement->validate(); // This must not explode :D
        $movement->save();
    }

    /**
     * Get an infinite amount of the same Local sections.
     *
     * @generator
     */
    private function getGeneratorCircularSections()
    {
        static $cache;
        if (! $cache) {
            $cache = $this->generateCircularGenerator($this->getOrCreateAllKnownExampleSections());
        }

        return $cache;
    }

    /**
     * Get an infinite amount of the same Banks.
     *
     * @generator
     */
    private function getGeneratorCircularBanks()
    {
        static $cache;
        if (! $cache) {
            $cache = $this->generateCircularGenerator($this->getExampleBanks());
        }

        return $cache;
    }

    /**
     * Get an infinite amount of the same Local sections, and null.
     *
     * @generator
     */
    private function getGeneratorCircularSectionsAndNull()
    {
        static $cache;
        if (! $cache) {
            $cache = $this->generateCircularGenerator($this->arrayPlusNull($this->getOrCreateAllKnownExampleSections()));
        }

        return $cache;
    }

    /**
     * @return array
     */
    private function getExampleBanks()
    {
        return [
            $this->bank_unicredit,
            $this->bank_paypal,
        ];
    }

    /**
     * Get or create some local sections.
     *
     * @return array
     */
    private function getOrCreateAllKnownExampleSections()
    {
        return [
            $this->getOrCreateSectionByCityAndProv('Roma', 'RM'),
            $this->getOrCreateSectionByCityAndProv('Milano', 'MI'),
        ];
    }

    /**
     * Create all the default Legal Entities.
     * @return void
     */
    private function createDefaultLegalEntities(): void
    {
        foreach (LegalEntity::getDefaults() as $legal_entity) {
            $legal_entity_existing = LegalEntity::ofSlug($legal_entity->slug)->first();
            if (!$legal_entity_existing) {
                $legal_entity->save();
            }
        }
    }

    /**
     * Get an infinite amount of the very same things.
     *
     * @generator
     */
    private function generateCircularGenerator(array $my_array)
    {
        $n = count($my_array);
        $i = 0;
        while (true) {
            yield $my_array[$i++ % $n];
        }
    }

    /**
     * Create a random birth DATE for users, never in the future.
     *
     * @return string
     */
    private function createRandomBirthDate()
    {
        $CURRENT_YEAR = date('Y');
        $MIN_BIRTH_YEAR = $CURRENT_YEAR - 105;
        $MAX_BIRTH_YEAR = $CURRENT_YEAR - 18;

        return $this->createRandomDateInYearButNotInFuture(random_int($MIN_BIRTH_YEAR, $MAX_BIRTH_YEAR));
    }

    /**
     * Create a random DATE in a specific year, but not in the future.
     *
     * @return string
     */
    private function createRandomDateInYearButNotInFuture(int $year)
    {
        $datetime = $this->createRandomDateTimeInYearButNotInFuture($year);

        return substr($datetime, 0, 10);
    }

    /**
     * Create a random DATETIME in a specific year, but not in the future.
     *
     * @return string
     */
    private function createRandomDateTimeInYearButNotInFuture(int $year)
    {
        $current_year = (int) date('Y');
        $current_month = (int) date('m');
        $current_day = (int) date('d');

        // Avoid random month in the future.
        if ($year === $current_year) {
            $random_month = random_int(1, $current_month);
        } else {
            $random_month = random_int(1, 12);
        }

        // Avoid random day in the future.
        if ($current_year === $year && $current_month === $random_month) {
            $random_day = random_int(1, $current_day);
        } else {
            $random_day = random_int(1, 28);
        }

        return sprintf('%d-%02d-%02d %02d:%02d:%02d',
            $year,             // Y
            $random_month,     // m
            $random_day,       // d
            random_int(0, 23), // H
            random_int(0, 59), // i
            random_int(0, 59)  // s
        );
    }

    /**
     * Take an array, returns it with null as last value.
     *
     * @return array
     */
    private function arrayPlusNull(array $v)
    {
        $v[] = null;

        return $v;
    }

    /**
     * Given a generator, get the next and advance the cursor.
     */
    private function nextPlease($generator)
    {
        $generator->next();
        $v = $generator->current();

        return $v;
    }
}
