<?php

use App\Role;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    const MISSING_ROLE = 'shipper';

    public function up()
    {
        Schema::create('gadget_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('email')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('postal_code');
            $table->string('city');
            $table->string('province');
            $table->text('distribution_location')->nullable();
            $table->boolean('linuxsi_sticker')->default(false);
            $table->boolean('subscribe_newsletter')->default(false);
            $table->enum('status', ['pending', 'valid', 'preparing', 'shipped', 'cancelled'])->default('pending');
            $table->string('token', 64);
            $table->timestamps();
        });

        // Only if we already have roles... add a shipper.
        $admin = Role::first();
        if ($admin) {
            $shipper = Role::where('name', self::MISSING_ROLE)->get();
            if (!$shipper) {
                ( new Role() )
                    ->setName(self::MISSING_ROLE)
                    ->save();
            }
        }
    }

    public function down()
    {
        Schema::dropIfExists('gadget_requests');

        // Eventually drop again the shipper role.
        $shipper = Role::where('name', self::MISSING_ROLE)->first();
        if ($shipper) {
            $shipper->delete();
        }
    }
};
