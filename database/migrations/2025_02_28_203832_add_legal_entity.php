<?php

use App\LegalEntity;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Create table.
        Schema::create('legal_entities', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 64)->unique();
            $table->string('name', 64);
            $table->string('name_plural', 64);
            $table->string('name_short', 32);
            $table->integer('order')->default(0);
            $table->integer('min_limit')->nullable();
            $table->integer('max_limit')->nullable();
            $table->integer('min_percentage_limit')->nullable();
            $table->integer('max_percentage_limit')->nullable();
             // Speedup "order by order, slug".
             // So we allow manual ordering, or automatic ordering (e.g. if 'order=0').
            $table->index(['order', 'slug']);
            $table->timestamps();
            $table->softDeletes();
        });

        // Users: add foreign key. This implicitly also create an index.
        // The index is called 'users_legal_entity_id_foreign' by Laravel.
        Schema::table('users', function (Blueprint $table) {
            $table
                ->unsignedBigInteger('legal_entity_id')
                ->nullable()
                ->after('type');

            $table
                ->foreign(['legal_entity_id'])
                ->references('id')
                ->on('legal_entities')
                ->restrictOnUpdate()
                ->restrictOnDelete();
        });

        // Create few useful entries.
        // This is done here, and not in the seeder, since this is useful
        // also in production.
        // This is done in a transaction as micro-optimization.
        DB::transaction(function () {
            // Persist the default values.
            foreach (LegalEntity::getDefaults() as $legal_entity) {
                $legal_entity->save();
            }

            $individual = LegalEntity::getIndividual();
            User::where('type', 'regular')
                ->update([
                    'legal_entity_id' => $individual->id,
                ]);

            $aps = LegalEntity::ofSlug('it-aps')->first();
            User::where('type', 'association')
                ->whereIn('surname', ['APS', 'Associazione di promozione sociale'])
                ->update([
                    'legal_entity_id' => $aps->id,
                ]);

            $odv = LegalEntity::ofSlug('it-odv')->first();
            User::where('type', 'association')
                ->where('surname', 'Organizzazione di Volontariato')
                ->update([
                    'legal_entity_id' => $odv->id,
                ]);

            $adv = LegalEntity::ofSlug('it-associazione-generica-o-culturale')->first();
            User::where('type', 'association')
                ->whereIn('surname', ['Associazione','Associazione culturale'])
                ->update([
                    'legal_entity_id' => $adv->id,
                ]);

            $anr = LegalEntity::ofSlug('it-associazione-non-riconosciuta')->first();
            User::where('type', 'association')
                ->where('surname', 'Associazione non riconosciuta')
                ->update([
                    'legal_entity_id' => $anr->id,
                ]);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['legal_entity_id']);
            $table->dropColumn('legal_entity_id');
        });

        Schema::drop('legal_entities');
    }
};
