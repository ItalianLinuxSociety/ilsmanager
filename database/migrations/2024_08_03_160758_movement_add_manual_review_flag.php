<?php

use app\Movement;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('movements', function (Blueprint $table) {
            // New Movements should be considered not-reviewed.
            $table->tinyinteger('reviewed')->default(0);

            // Speedup Review page that does this lookup.
            $table->index(['reviewed']);
        });

        // Consider orphan movements as not-reviewed.
        Movement::whereDoesntHave('account_rows')
            ->update(['reviewed' => 0]);

        // Consider populated movements as reviewed.
        Movement::whereHas('account_rows')
            ->update(['reviewed' => 1]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('movements', function (Blueprint $table) {
            $table->dropIndex(['reviewed']);
            $table->dropColumn('reviewed');
        });
    }
};
