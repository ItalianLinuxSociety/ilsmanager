<?php

namespace App\Actions;

use App\Account;
use App\AccountRow;
use App\Movement;
use App\User;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PayPalAPIMovementsReader;

/**
 * @author Valerio Bozzolan
 */
class ImportPayPalMovements
{
    /**
     * @param  $args  array movement:   Movement|null In case you want to refresh a specific Movement.
                          date_start: string|null   Example: 2024-12-31
                          date_to:    string|null   Example: 2024-12-31
     */
    public function handle(array $args = [])
    {
        $movement = $args['movement'] ?? null;
        $date_start = $args['date_start'] ?? null;
        $date_to = $args['date_to'] ?? null;

        // if something goes wrong this throws a cute exception
        $config = PayPalAPIMovementsReader::validatedConfig();

        // we have to know the exact PayPal bank ID
        $bank_id = $config['bankid'];

        $fee_sum = 0.0;

        // Allow to refresh just a specific Movement.
        $transaction_id = null;
        if ($movement) {
            // Get the PayPal Transaction ID.
            $transaction_id = $movement->identifier;

            // Funny PayPal API. Without a date it screams violently.
            // TODO: evaluate if we should substract some days, to find movements
            // moved by the bank a bit early (for unknown bank reasons...).
            $date_start = $movement->date->toDateTime();
        }

        // If we are NOT interested about a specific Transaction, guess a good date
        // inspecting the last imported Movement.
        if (! $transaction_id) {
            if ($date_start) {
                $date_start = DateTime::createFromFormat('Y-m-d', $date_start);
            } else {
                // Guess a meaningful Date From, taking last PayPal movement.
                // We don't want reviewed movements,
                // since we are interested in fetching new remote transactions.
                $latest = Movement::where('bank_id', $bank_id)
                  //->where('reviewed', 1)
                    ->orderBy('date', 'desc')
                    ->first();

                // Don't lookup the exact date. Look a little earlier.
                // So we increase possibilities to get consolidated stuff that was maybe skipped before.
                if ($latest) {
                    $date_start = $latest->date->copy()->subDays(3);
                }
            }

            // Just something!
            if (! $date_start) {
                $date_start = new DateTime;
                $date_start->modify('1 January');
            }

            // At the moment the user cannot pick an exact time from the frontend,
            // so, import since midnight, to have reliable import, not based on current time,
            // so, there are no risks to skip a movement at 8 A.M. because I'm importing them at 9 A.M.
            $date_start->setTime(0, 0);

            // Allow user to set a custom Date To, but ignore the time.
            if ($date_to) {
                $date_to = DateTime::createFromFormat('Y-m-d', $date_to);
                $date_to->setTime(23, 59);
            }
        }

        DB::beginTransaction();

        PayPalAPIMovementsReader::process([
            'transactionId' => $transaction_id,
            'startDate' => $date_start,
            'endDate' => $date_to,
            'itemCallback' => function ($response_data) use ($bank_id) {
                $id = $response_data['id'];
                $date = $response_data['date'];
                $amount_gro = $response_data['amount_gross'];
                $amount_net = $response_data['amount_net'];
                $amount_in_original_currency = $response_data['amount_in_original_currency'];
                $currency_conversion = $response_data['currency_conversion'];
                $currency_original = $response_data['currency_original'];
                $fee = $response_data['amount_fee'];
                $notes = $response_data['notes'];
                $notes_xl = $response_data['notes_xl'];

                $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $date);

                // Import the original PayPal transaction.
                // Net amount:   what we receive in our bank after PayPal fees..
                // Gross amount: what the user really paid, shown in their receipt.
                // So if we receive a payment of 25 EUR, the net is ~23.80 EUR, and the gross is 25 EUR,
                // so the receipt shows 25 EUR and in our bank we get ~23.80EUR,
                // so it creates:
                //  1 AccountRow with 25   EUR "Saldo quota $YEAR Name Surname"
                //  1 AccountRow with 1.20 EUR "Spese bancarie"
                $movement = new Movement;
                $movement->amount = $amount_net;
                $movement->bank_id = $bank_id;
                $movement->identifier = $id;
                $movement->date = $date->format('Y-m-d H:i:s');
                $movement->notes = $notes_xl;

                // Also persist the gross amount when is meaningful.
                if ($amount_gro !== null && $amount_gro !== $amount_net) {
                    $movement->amount_original = $amount_gro;
                }

                // Eventually save the original currency, to indicate that
                // PayPal has done a currency conversion.
                // P.S. at the moment we assume that all movements have the very same
                // local currency, so we populate this field only if originally it was NOT
                // under our local currency.
                if ($currency_conversion) {
                    $movement->currency_original = $currency_original;
                    $movement->amount_original = $amount_in_original_currency;
                }

                // Do not import twice PayPal movements.
                // But stay ready to refresh already-imported ones.
                $existing_movement = $movement->alreadyTrackedMovement();
                if ($existing_movement && $existing_movement->isExactlySameMovement($movement)) {

                    Log::debug(sprintf('Refreshing data of Movement %d', $existing_movement->id));
                    $existing_movement->amount = $movement->amount;
                    $existing_movement->amount_original = $movement->amount_original;
                    $existing_movement->currency_original = $movement->currency_original;
                    $existing_movement->date = $movement->date;
                    $existing_movement->notes = $movement->notes;
                    $existing_movement->save();

                } else {
                    // Persist the Movement, so we have an ID.
                    // Note that we are inside a transaction so we can still rollback.
                    $movement->save();

                    // Import the Net amount. So if the original donation was 25 EURO, this is 25.
                    $ar = new AccountRow;
                    $ar->movement_id = $movement->id;
                    $ar->notes = $notes_xl;
                    $ar->amount = $amount_gro;
                    $ar->save();

                    // Eventually import the fee.
                    if ($fee && $fee !== '0.00') {
                        $ar = new AccountRow;
                        $ar->movement_id = $movement->id;
                        $ar->account_id = Account::where('bank_costs', true)->first()->id;
                        $ar->notes = 'Commissioni PayPal';
                        $ar->amount = $fee;
                        $ar->save();
                    }
                }
            },
        ]);

        DB::commit();
    }
}
