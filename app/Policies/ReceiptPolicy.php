<?php

namespace App\Policies;

use App\Receipt;
use App\User;
use App\Fee;

use Illuminate\Auth\Access\HandlesAuthorization;

class ReceiptPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // There is not a list of receipts. But, in case, don't show to everyone.
        return $user->hasRole('admin');
    }

    public function view(User $user, Receipt $model)
    {
        if ($user->hasRole('admin')) {
            return true;
        }

        $fee = Fee::whereHas('account_row', function($subquery) use ($model) {
            $subquery->where('movement_id', $model->movement_id);
        })->first();

        if (!$fee) {
            return false;
        }

        return $fee->user_id == $user->id;
    }

    public function create(User $user, Receipt $receipt = null)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Receipt $model)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Receipt $model)
    {
        return $user->hasRole('admin');
    }
}
