<?php

namespace App\Policies;

use App\Movement;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MovementDetailedPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // Only administrators can list movements.
        // In the future this may be set to just "true"
        // and anonymize the movements to normal users.
        return $user->hasRole('admin');
    }

    public function view(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }

    public function create(User $user, Movement $movement = null)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Movement $movement)
    {
        return $user->hasRole('admin');
    }
}
