<?php

namespace App\Policies;

use App\User;
use App\Movement;
use Illuminate\Auth\Access\HandlesAuthorization;

class MovementPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // Only administrators can list movements.
        // In the future this may be set to just "true"
        // and anonymize the movements to normal users.
        return $user->hasRole('admin');
    }

    public function view(User $user, Movement $movement)
    {
        return $this->index($user);
    }

    public function create(User $user, Movement $movement = null)
    {
        return $this->index($user);
    }

    public function update(User $user, Movement $movement)
    {
        return $this->index($user);
    }

    public function delete(User $user, Movement $movement)
    {
        return $this->index($user);
    }
}
