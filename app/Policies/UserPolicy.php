<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // The user list is visible only to voting members.
        // These voting members must be able to approve the pending users during the assembly.
        return $user->isVotingMember;
    }

    public function view(User $user, User $model)
    {
        return $user->hasRole('admin') || $user->id == $model->id;
    }

    public function create(User $user, User $model = null)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, User $model)
    {
        return $user->hasRole('admin') || $user->id == $model->id;
    }

    public function delete(User $user, User $model)
    {
        return $user->hasRole('admin');
    }

}
