<?php

namespace App\Policies;

use App\User;
use App\Assembly;
use App\Votation;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotationPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // Everybody can see all votations as default.
        return true;
    }

    public function view(User $user, Votation $model)
    {
        return true;
    }

    public function create(User $user, Votation $votation = null)
    {
        // If we have the Assembly, be even stricter.
        if ($votation) {

            // Get the parent Assembly.
            // During creation we may have not the entity.
            $assembly = $votation->assembly;
            if (!$assembly && $votation->assembly_id) {
                $assembly = Assembly::find($votation->assembly_id);
            }

            // Check if the Assembly allows to add a Votation right now.
            // e.g. deny if the Assembly is closed, etc.
            if ($assembly) {
                if (!$user->can('addVotation', $assembly)) {
                    return false;
                }
            }
        }

        return $user->hasRole('admin');
    }

    public function update(User $user, Votation $model)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Votation $model)
    {
        return $user->hasRole('admin');
    }

    /**
     * Check if the User can vote a specified Votation.
     */
    public function vote(User $user, Votation $votation)
    {
        // Make sure that the votation is running,
        // and delegate to the AssemblyPolicy#vote().
        return $votation->isRunning() && $user->can('vote', $votation->assembly);
    }
}
