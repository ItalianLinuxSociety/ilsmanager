<?php

namespace App\Policies;

use App\Refund;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefundPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // Everybody can list all refunds.
        // The page is already anonymized to do not disclose non-personal data.
        // So "true" is a safe permissive value.
        return true;
    }

    public function view(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id;
    }

    public function create(User $user, Refund $refund = null)
    {
        return true;
    }

    public function update(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id ||
            ($user->hasRole('referent') && $user->section_id === $refund->section_id);
    }

    public function delete(User $user, Refund $refund)
    {
        return $user->hasRole('admin') || $refund->user_id == $user->id;
    }

    public function review(User $user, ?Refund $refund = null)
    {
        return $user->hasRole('admin');
    }
}
