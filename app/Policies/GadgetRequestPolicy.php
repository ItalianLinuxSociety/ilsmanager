<?php

namespace App\Policies;

use App\GadgetRequest;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GadgetRequestPolicy
{
    use HandlesAuthorization;

    public function index(?User $user = null)
    {
        // Everyone can access the gadget requests page.
        // It's already anonymized to do not disclose non-personal data.
        // So this is correct to be very permissive.
        return true;
    }

    public function view(User $user, GadgetRequest $gadgetRequest)
    {
        return $user->hasRole(['admin','shipper']) || $gadgetRequest->isUserAuthor($user);;
    }

    public function create(User $user = null, GadgetRequest $gadgetRequest = null)
    {
        return true;
    }

    public function update(User $user, GadgetRequest $gadgetRequest)
    {
        return $user->hasRole(['admin','shipper']) || $gadgetRequest->isUserAuthor($user);
    }

    public function review(User $user, GadgetRequest $gadgetRequest)
    {
        // People who can change status.
        return $user->hasRole(['admin','shipper']);
    }

    public function delete(User $user, GadgetRequest $gadgetRequest)
    {
        return $user->hasRole(['admin','shipper']) || $gadgetRequest->isUserAuthor($user);
    }
}
