<?php

namespace App\Policies;

use App\User;
use App\Vote;
use App\Votation;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // There is not such index of all votes...
        // Anyway, only active users should be able to see votes.
        return $user->isVotingMember;
    }

    public function view(User $user, Vote $vote)
    {
        // There is not the possibility to see a single vote...
        // Anyway, only active users should be able to see votes.
        // In the future we may have secret Votations, and so,
        // only the author should see that.
        // But again, everything here is silly, since we do not
        // have such view "single Vote view".
        return $user->isVotingMember;
        //  && $user->id === $vote->user_id;
    }

    public function create(User $user, Vote $vote = null)
    {
        // This is very core, very generic, not related to any specific Votation.
        if ($vote) {

            // Get the parent Votation.
            // Micro-optimization: during creation we may have not the entity.
            $votation = $vote->votation;
            if (!$votation && $vote->votation_id) {
                $votation = Votation::find($vote->votation_id);
            }

            // Check if the Votation allows to express your vote there.
            // e.g. deny if the parent Assembly is closed, etc.
            if ($votation) {
                if (!$user->can('vote', $votation)) {
                    return false;
                }
            }

        }

        return $user->isVotingMember;
    }

    public function update(User $user, Vote $vote)
    {
        // The Vote update policy is, first, based on the create policy.
        return $user->can('create', Vote::class)
            // Then, delegate to the VotationPolicy#vote().
            && $user->can('vote', $vote->votation);
    }

    public function delete(User $user, Vote $model)
    {
        // Votes cannot be removed, for archival reasons.
        return false;
    }
}
