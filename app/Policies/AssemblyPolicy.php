<?php

namespace App\Policies;

use App\Assembly;
use App\User;
use App\Vote;
use App\Votation;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssemblyPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // Everyone can list all assemblies.
        // Generally the list do not contain personal data to be protected.
        return true;
    }

    public function view(User $user, Assembly $model)
    {
        // All members can view the details of every Assembly.
        // Allow also potential members to view these details,
        // since very probably they need this info to vote in the future.
        return $user->actsLikeMember;
    }

    public function create(User $user, ?Assembly $assembly = null)
    {
        // Only administrators can create new Assemblies.
        return $user->hasRole('admin');
    }

    public function update(User $user, Assembly $assembly)
    {
        // Be very strict.
        // Note that normal Users should participate an Assembly, never updating it.
        return $user->hasRole('admin');
    }

    public function delete(User $user, Assembly $model)
    {
        // Be very strict.
        return $user->hasRole('admin');
    }

    /**
     * Whenever the user can change critical voting options.
     */
    public function updateVotingOptions(User $user, Assembly $assembly)
    {
        // Do not allow anybody to update archived Assemblies.
        return $this->update($user, $assembly) && ! $assembly->isClosed();
    }

    /**
     * Whenever the user can *potentially* vote something in any generic Assembly.
     * Note that more specific things should be checked instead.
     * This policy does NOT consider the Assembly status.
     * To vote, use the VotationPolicy instead.
     */
    public function proveRightToVote(User $user)
    {
        // At the moment this is just a shortcut.
        return $user->isVotingMember;
    }

    /**
     * Whenever the user can actively interact with this assembly,
     * as potential voter, in whatever way like participate, unparticipate,
     * vote, delegate, etc.
     * This policy considers the Assembly status. So you cannot interact with closed Assemblies.
     * This is very generic, as base for more permissions.
     */
    public function interactAsVoter(User $user, Assembly $assembly)
    {
        return $assembly->isOpen() && $this->proveRightToVote($user);
    }

    /**
     * Whenever the user can add themselves as participant.
     * Note that people who cannot vote must not participate.
     */
    public function participate(User $user, Assembly $assembly)
    {
        // At the moment this is just a shortcut.
        return $this->interactAsVoter($user, $assembly);
    }

    /**
     * Whenever the user can remove themselves as participant.
     * This also can drop the delegations of this user.
     */
    public function unparticipate(User $user, Assembly $assembly)
    {
        // This policy is very open:
        // Allow everyone to *un*participate an Assembly.
        // Note that people can join while the Assembly is open,
        // so they can also unjoin while is open (or even pending).
        // Don't allow to change a closed Assembly.
        // We generally allow ANYONE from unparticipating,
        // so, who missed the voting privileges and participated by mistake
        // can still unparticipate.
        if ($assembly->isClosed()) {
            return false;
        }

        // Don't allow to un-participate if you already submitted a Vote.
        $has_voted = Vote::query()
            ->whereAssembly($assembly)
            ->whereUser($user)
            ->exists();

        if ($has_voted) {
            return false;
        }

        return true;
    }

    /**
     * Check if the user can vote in this assembly.
     * Note that more specific things should be checked instead.
     * Use the VotationPolicy instead. This is very generic.
     */
    public function vote(User $user, Assembly $assembly)
    {
        // Do not allow to vote somebody that cannot even participate.
        if (! $this->participate($user, $assembly)) {
            return false;
        }

        // Only allow users who are directly participating.
        $direct_participation =
            $assembly
                ->users()
                ->onlyDirectParticipant()
                ->whereUser($user)
                ->sharedLock()
                ->exists();

        return $direct_participation;
    }

    /**
     * Check if the user can delegate their vote to somebody participating in this assembly.
     */
    public function delegate(User $user, Assembly $assembly, User $delegate_user)
    {
        // Do not allow to delegate yourself lol.
        if ($user->id == $delegate_user->id) {
            return false;
        }

        // Do not allow to delegate if you cannot even try to participate.
        if (! $this->participate($user, $assembly)) {
            return false;
        }

        // Do not also allow to delegate to somebody that cannot participate.
        if (! $this->participate($delegate_user, $assembly)) {
            return false;
        }

        // Do not allow to delegate to somebody who is not directly participating.
        $delegate_participation =
            $assembly
                ->users()
                ->onlyDirectParticipant()
                ->whereUser($delegate_user)
                ->sharedLock()
                ->first();

        if (! $delegate_participation) {
            return false;
        }

        // The delegated user may not be able to receive another one, for legal limits.
        $delegations_received_count =
            $assembly
                ->users()
                ->whereDelegateUser($delegate_user)
                ->sharedLock()
                ->count();

        return $delegations_received_count < $assembly->getMaxAllowedDelegations();
    }

    /**
     * Check if the user can drop their current delegation.
     */
    public function undelegate(User $user, Assembly $assembly)
    {
        // This is just a shortcut.
        return $this->unparticipate($user, $assembly);
    }

    /**
     * Check if an admin can add a Votation in this Assembly.
     */
    public function addVotation(User $user, Assembly $assembly)
    {
        return $this->updateVotingOptions($user, $assembly);
    }
}
