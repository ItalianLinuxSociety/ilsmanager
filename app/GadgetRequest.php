<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use function env;
use function route;

class GadgetRequest extends Model
{
    use ILSModel;
    use HasFactory;

    public function getEntityNameSingular(): string
    {
        return 'Richiesta Spedizione Gadget';
    }

    public function getEntityNamePlural(): string
    {
        return 'Richieste Spedizione Gadget';
    }

    public function getBaseViewName()
    {
        return 'gadget-request';
    }

    /**
     * Check if this feature is enabled in your installation, or not.
     * @return bool
     */
    public static function is_feature_enabled()
    {
        // If your installation does not plan to ship anything,
        // well, just define this in your environment variables.
        return env('ENABLE_GADGET_REQUESTS', true);
    }

    /**
     * Check whenever a specified user can edit this entity.
     * @param User $user Mandatory user
     * @return bool|null
     */
    public function isUserAuthor(User $user) {
        return isset($this->user->id) && $this->user->id == $user->id;
    }

    protected $fillable = [
        'user_id',
        'email',
        'name',
        'surname',
        'postal_code',
        'city',
        'province',
        'street',
        'distribution_location',
        'subscribe_newsletter',
        'status_id',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($gadgetRequest) {
            $gadgetRequest->token = Str::random(64);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gadgetTypes()
    {
        return $this->belongsToMany(GadgetType::class, 'gadget_request_gadget_type', 'gadget_request_id', 'gadget_type_id');
    }

    public function status()
    {
        return $this->belongsTo(GadgetRequestStatus::class);
    }

    /**
     * @return string
     */
    public function getWhateverEmailAttribute()
    {
        if (isset($this->email)) {
            return $this->email;
        }
        if (isset($this->user)) {
            return $this->user->customEmail;
        }
        // This should never happen.
        // Let's scream, to catch lack of server-side validation.
        throw new Exception("Missing email");
    }

    /**
     * Get the edit URL of this entity.
     * @return string
     */
    public function getEditURLAttribute()
    {
        if (!$this->user) {
            return route('gadget-request.show-by-token', ['token' => $this->token]);
        }
        return $this->adminEditURL;
    }

    /**
     * Get a copy-pastable shipping address.
     * This is what you expect on the letter.
     * @return string
     */
    public function getShippingAddressRawAttribute()
    {
        return $this->name . " " . $this->surname . "\n" .
               $this->street . "\n" .
               $this->postal_code . " " . $this->city . " (" . $this->province. ")";
    }

    /**
     * Send an email or fail violently.
     * If possible, try official user methods, then, use whatever email provided.
     * This method may throw.
     * @param wild|Notification|Mailable
     */
    public function sendMailable($mailable): void
    {
        if ($this->user) {
            $this->user->sendMailable($mailable);
        } else {
            Mail::to($this->whateverEmail)->send($mailable);
        }
    }

    /**
     * Get the tracking URL for this gadget request.
     * Before using this method you MUST check that a tracking_code is available.
     * @return string URL
     */
    public function getPostalTrackerURLAttribute()
    {
        return self::postal_multi_tracker_URL( [ $this->tracking_code ] );
    }

    /**
     * Get an URL tracking some postal letters/packages.
     * @param array $codes Array of postal tracking codes
     * @return string URL
     */
    public static function postal_multi_tracker_URL( $codes )
    {
        // We assume that this tracking URL supports multiple tracking codes.
        $codes_flat = implode( ',', $codes );
        $url_format = Config::getConfig('gadget_requests_multi_tracker_url_format');
        return sprintf( $url_format, $codes_flat );
    }

}
