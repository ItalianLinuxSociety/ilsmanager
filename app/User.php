<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\HasApiTokens;

use App\Config;
use App\UserType;

class User extends Authenticatable
{
    use HasApiTokens;
    use ILSModel;
    use Notifiable;

    const STATUS_ACTIVE = 'active';

    public function getEntityNameSingular(): string
    {
        return 'Utenza';
    }

    public function getEntityNamePlural(): string {
        return 'Utenze';
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        return $this->username;
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function configs()
    {
        return $this->morphMany('App\Config', 'subject');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function fees()
    {
        return $this->hasMany('App\Fee');
    }

    public function accountRows()
    {
        return $this->hasMany('App\AccountRow');
    }

    public function donations()
    {
        $account_donation = Account::where('donations', 1)->firstOrFail();

        return $this->accountRows()
            ->where('account_id', $account_donation->id)
            ->join('movements', 'account_rows.movement_id', '=', 'movements.id');
    }

    public function refunds()
    {
        return $this->hasMany('App\Refund');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function legalEntity()
    {
        return $this->belongsTo(LegalEntity::class);
    }

    /**
     * Scope a query to only include users that acts like a member,
     * including active users (indeed) and pending users with
     * at least one fee.
     */
    public function scopeActLikeMember(Builder $query): void
    {
        $query->where(function($query_p) {
            // Active members always can login.
            $query_p->orWhere(function($query_1) {
                $query_1->active();
            });

            // Pending members can login only with a recent fee.
            $query_p->orWhere(function($query_2) {
                $query_2->pendingWithPaidFee();
            });
        });
    }

    /**
     * Scope a query to only include active users.
     */
    public function scopeActive(Builder $query): void
    {
        // This calls scopeOfStatus().
        $query->ofStatus('active');
    }

    /**
     * Scope a query to only include pending users.
     */
    public function scopePending(Builder $query): void
    {
        // This calls scopeOfStatus().
        $query->ofStatus('pending');
    }

    /**
     * Scope a query to only include pending users that can login,
     * or that can act like a member, since they paid their fee.
     */
    public function scopePendingWithPaidFee(Builder $query): void
    {
        $query->pending();
        $query->hasFeeAtYearOrFuture();
    }

    /**
     * Scope a query to only include pending users that cannot login,
     * since they have not paid their fee.
     */
    public function scopePendingWithoutPaidFee(Builder $query): void
    {
        $query->pending();
        $query->whereNot(function($sub_query) {
            // Exclude users that absolutely have not any recent fee.
            $sub_query->hasFeeAtYearOrFuture(date('Y') - 2);
        });
    }

    /**
     * Scope a query to only include users with a specific status.
     * https://laravel.com/docs/11.x/eloquent#dynamic-scopes
     */
    public function scopeOfStatus(Builder $query, string $status): void
    {
        $query->where('status', $status);
    }

    /**
     * Scope a query to only include users with a specific type.
     * https://laravel.com/docs/11.x/eloquent#dynamic-scopes
     */
    public function scopeOfType(Builder $query, string $type): void
    {
        $query->where('type', $type);
    }

    /**
     * Scope a query to only include users with a specific LegalEntity.
     */
    public function scopeOfLegalEntity(Builder $query, ?LegalEntity $legal_entity): void
    {
        if ($legal_entity) {
            $query->where('legal_entity_id', $legal_entity->id);
        } else {
            $query->whereNull('legal_entity_id');
        }
    }

    /**
     * Scope a query to only include users with a specific role.
     * https://laravel.com/docs/11.x/eloquent#dynamic-scopes
     */
    public function scopeOfRole(Builder $query, string $role_name): void
    {
        $query->whereHas('roles', function ($sub_query) use ($role_name) {
            $sub_query->where('name', $role_name);
        });
    }

    /**
     * Scope a query to only include users WITH a fee on a specific year
     * or in any future year.
     * As default it considers the current year.
     */
    public function scopeHasFeeAtYearOrFuture(Builder $query, ?int $year = null): void
    {
        if (!$year) {
            $year = date('Y');
        }
        $query->whereHas('fees', function ($sub_query) use ($year) {
            $sub_query->where('year', '>=', $year);
        });
    }

    /**
     * Scope a query to only include users WITH a fee on a specific year.
     */
    public function scopeHasFee(Builder $query, int $year): void
    {
        $query->whereHas('fees', function ($sub_query) use ($year) {
            $sub_query->where('year', $year);
        });
    }

    /**
     * Scope a query to only include users WITHOUT a fee on a specific year.
     */
    public function scopeHasNotFee(Builder $query, int $year): void
    {
        $query->whereDoesntHave('fees', function ($sub_query) use ($year) {
            $sub_query->where('year', $year);
        });
    }

    /**
     * Scope a query to only include users that do not have a notification name.
     */
    public function scopeDoesntHaveNotification(Builder $query, string $command_name): void
    {
        $query->whereDoesntHave('notifications', function($sub_query) use ($command_name) {
            $sub_query->commandName($command_name);
        });
    }

    public function scopeOrderByPrintableName(Builder $query): void
    {
        $query
            ->orderBy('surname')
            ->orderBy('name');
    }

    public function getPrintableNameAttribute()
    {
        return sprintf('%s %s', $this->surname, $this->name);
    }

    public function getFullAddressAttribute()
    {
        return sprintf("%s\n%s (%s)", $this->address_street, $this->address_place, $this->address_prov);
    }

    public function getCustomEmailAttribute()
    {
        if (Config::getConfig('custom_email_aliases') == '1') {
            return sprintf('%s@%s', $this->username, Config::getConfig('custom_email_domain'));
        }
        else {
            return null;
        }
    }

    /**
     * Get the UserType object of this user.
     * @return UserType
     */
    public function getUserTypeAttribute()
    {
        return UserType::find_by_identifier_or_fail($this->type);
    }

    public function getHumanStatusAttribute()
    {
        $statuses = self::statuses();

        foreach($statuses as $s) {
            if ($s->identifier == $this->status)
                return $s->label;
        }

        Log::error('Stato utente non riconosciuto: ' . $this->status);
        return '';
    }

    /**
     * Check whenever this user generally should pay a Fee.
     *
     * @return bool
     */
    public function getActsLikeMemberAttribute()
    {
        // This is generally false for 'guest'.
        return $this->userType->member;
    }

    /**
     * Check whenever this user is a profile that should be able to vote.
     *
     * @return bool
     */
    public function getIsVotingMemberAttribute(): bool
    {
        // At the moment only status=active users can add a vote.
        $is_active = $this->isStatus(self::STATUS_ACTIVE);

        // We exclude the type=guest for extra hardening.
        // Allow-list is more secure than deny-list.
        $is_member = $this->actsLikeMember;

        return $is_active && $is_member;
    }

    /**
     * Get the fee that this user is expected to pay, yearly.
     * Note that this may have no sense for guests.
     * Before reading this, you may want to call the attribute "actsLikeMember"
     * first.
     * @return string
     */
    public function getFeeAmountAttribute()
    {
        static $cache = 0;
        if ($cache === 0) {
            // This is 'regular' or 'association' or 'guest' in most cases.
            $type_identifier = $this->userType->identifier;

            // It does not exist an option for "guests" fee.
            // So, just fallback to the default one.
            if (!$this->userType->member) {
                $type_identifier = UserType::DEFAULT_MEMBER_IDENTIFIER;
            }

            $cache = Config::getConfig($type_identifier . '_annual_fee');
        }
        return $cache;
    }

    /**
     * Check if this user is myself.
     *
     * @return bool
     */
    public function getIsMyselfAttribute(): bool
    {
        if (!Auth::check()) {
            return false;
        }
        return Auth::user()->id === $this->id;
    }

    public function firstMissingFeeYear()
    {
        $this_year = date('Y');

        $last_fee_year = null;
        $last_fee = $this->fees()->orderBy('year', 'desc')->first();
        if (!is_null($last_fee)) {
            $last_fee_year = $last_fee->year;
        }

        // If the account has been suspended, do not start from the last Fee, that may be 10 years in the past.
        //
        // Practical Desired Examples (assuming year is 2024):
        //   ActiveUser with last fee 1999: +1 (NOTE: the user MAY want to be suspended in this case)
        //   ActiveUser with last fee 2023: +1
        //   ActiveUser with last fee 2024: +1
        //   ActiveUser with last fee 2099: +1
        //   Suspended  with last fee 1999: start from this year
        //   Suspended  with last fee 2023: start from this year
        //   Suspended  with last fee 2024: +1 (don't care corner case - usually users are not suspended with recent fees... but this makes sense)
        //   Suspended  with last fee 2099: +1 (don't care corner case - usually users are not suspended with recent fees... but keeping your good score makes sense)
        //   ActiveUser without fee:        start from this year
        //   Suspended  without fee:        start from this year
        if (!$this->isStatus(self::STATUS_ACTIVE) && $last_fee_year && $last_fee_year < $this_year) {
            $last_fee_year = null;
        }

        if ($last_fee_year) {
            /*
                Qui c'è un errore deliberato, dovrebbe essere
                if ($last_fee->year < $this_year)
                Viene temporaneamente mantenuto così fino al rinnovo dello
                Statuto, sarà da correggere nel prossimo futuro
            */
            if ($last_fee < $this_year)
                return $this_year;
            else
                return $last_fee_year + 1;
        }
        else {
            return $this->getFirstQuotaValidityYear();
        }
    }

    public function getFirstQuotaValidityYear()
    {
        /*
            I nuovi iscritti (dunque: senza nessuna quota precedente) che si
            iscrivono da ottobre hanno la quota valida già per l'anno successivo.
            Questa è una cosa deliberata, per incoraggiare le persone ad iscriversi
            durante il Linux Day (ad ottobre).
            - Roberto Guido
        */
        $this_year = date('Y');
        $this_month = date('m');
        if ($this_month >= 10) {
            return $this_year + 1;
        }
        return $this_year;
    }

    public function regularFee()
    {
        return ($this->fees()->where('year', '>=', date('Y'))->first() != null);
    }

    public function hasRole($roles)
    {
        if (is_array($roles) == false) {
            $roles = [$roles];
        }

        foreach($this->roles as $user_role) {
            foreach($roles as $search_role) {
                if ($user_role->name == $search_role) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if this user has a specific status value.
     */
    protected function isStatus(string $status): bool
    {
        return $this->status === $status;
    }

    public static function statuses()
    {
        return [
            (object) [
                'identifier' => 'pending',
                'label' => 'In Attesa di Approvazione Assemblea'
            ],
            (object) [
                'identifier' => self::STATUS_ACTIVE,
                'label' => 'Attivo'
            ],
            (object) [
                'identifier' => 'suspended',
                'label' => 'Sospeso'
            ],
            (object) [
                'identifier' => 'expelled',
                'label' => 'Espulso'
            ],
            (object) [
                'identifier' => 'dropped',
                'label' => 'Scaduto'
            ],
            // This is a guest.
            (object) [
                'identifier' => 'limited',
                'label' => 'Limitato',
            ],
        ];
    }

    /**
     * Send an email via static mailable class name, or fail violently.
     * This method will automatically try to first send an email to the primary
     * custom email (like foo@linux.it) and then, if that mailbox is not maybe
     * activated and returns a server failure or something, this will automatically
     * retry a new attempt on the original user email.
     * If no email was sent, an Exception is thrown, so you should try-catch this.
     * @param string $class Name of the email class to be used.
     *                      The user object will be added as first argument.
     */
    public function sendMail(string $class_name): void
    {
        $mailable = new $class_name($this);
        $this->sendMailable($mailable);
    }

    /**
     * Send a mailable or fail violently.
     * This method will automatically try to first send an email to the primary
     * custom email (like foo@linux.it) and then, if that mailbox is not maybe
     * activated and returns a server failure or something, this will automatically
     * retry a new attempt on the original user email.
     * If no email was sent, an Exception is thrown, so you should try-catch this.
     * @param wild|Notification|Mailable
     */
    public function sendMailable($mailable): void
    {
        // First, try on the custom email.
        // If this first attempt fail, silently fail.
        // so, we will immediately retry on the original email.
        $custom_email = $this->custom_email;
        if (!empty($custom_email)) {
            try {
                // If this works, no need to do other attempts. Immediately return.
                $this->sendMailableToAddressOrFail($mailable, $custom_email);
                return;
            } catch(\Exception $e) {
                Log::warning(sprintf("%s: will retry with original email", $e->getMessage()));
            }
        }

        // As last resource, try on the original user email.
        // This is the last attempt so this MAY throw, and it
        // SHOULD throw in case of any mailserver failure so you can catch this failure.
        $this->sendMailableToAddressOrFail($mailable, $this->email);
    }

    /**
     * Send an email to an address or fail violently.
     */
    private function sendMailableToAddressOrFail($mailable, string $address): void
    {
        try {
            Mail::to($address)->send($mailable);
        } catch(\Exception $e) {
            // Wrap into a nicer error message.
            // Scream violently, since this may be a critical communication.
            // Please do not suppress this exception from here. Instead, catch.
            throw $this->createCannotContactUserException($e, $mailable, $address);
        }
    }

    public function getConfig($name)
    {
        $c = $this->configs()->where('name', $name)->first();
        if (is_null($c))
            return null;
        else
            return $c->value;
    }

    public function setConfig($name, $value)
    {
        $c = $this->configs()->where('name', $name)->first();
        if (is_null($c)) {
            $c = new Config();
            $c->name = $name;
            $c->subject_id = $this->id;
            $c->subject_type = 'App\User';
        }

        $c->value = $value;
        $c->save();
    }

    /******************************************************* CanResetPassword */

    public function sendPasswordResetNotification($token)
    {
        $this->notifyOrFailDesperate(new ResetPasswordNotification($token));
    }

    /**
     * Send a notification aggressively and desperately or fail violently.
     * This method will automatically try to first send an email to the primary
     * custom email (like foo@linux.it) and then, if that mailbox is not maybe
     * activated and returns a server failure or something, this will automatically
     * retry a new attempt on the original user email.
     * If no email was sent, an Exception is thrown, so you should try-catch this.
     */
    public function notifyOrFailDesperate(\Illuminate\Notifications\Notification $notifiable): void
    {
        // First, try on the custom email.
        // If this first attempt fail, silently fail.
        // so, we will immediately retry on the original email.
        $custom_email = $this->custom_email;
        if (!empty($custom_email)) {
            try {
                // Unfortunately Laravel seems a bit stupid in this case where we need to
                // send a notification on a custom email :D :D :D Apologies for this hack.
                $user_fake = clone $this;
                $user_fake->email = $custom_email;
                $user_fake->notifyOrFail($notifiable);

                // If this works, no need to do other attempts. Immediately return.
                return;
            } catch(\Exception $e) {
                Log::warning(sprintf("%s: will retry with original email", $e->getMessage()));
            }
        }

        // As last resource, try on the original user email.
        // This is the last attempt so this MAY throw, and it
        // SHOULD throw in case of any mailserver failure so you can catch this failure.
        $this->notifyOrFail($notifiable);
    }

    /**
     * Send a notification using the standard channel ($this->email) or fail violently.
     * If no email was sent, an Exception is thrown, so you should try-catch this.
     */
    public function notifyOrFail(\Illuminate\Notifications\Notification $notifiable): void
    {
        try {
            $this->notify($notifiable);
        } catch(\Exception $e) {
            // Wrap into a nicer error message.
            // Scream violently, since this may be a critical communication.
            // Please do not suppress this exception from here. Instead, catch.
            throw $this->createCannotContactUserException($e, $notifiable, $this->email);
        }
    }

    /**
     * Wrap a native email/notification exception into something slightly more useful and talking
     * that mentions the user ID and the original mailable/notification class name.
     * The original exception cause is preserved and reported for the glory of your stack trace.
     * @return \Exception
     */
    private function createCannotContactUserException(\Exception $e, $notifiable, $address): \Exception
    {
        $message = sprintf(
            "Unable to send notification to user address '%s' to contact user ID %d for %s: %s",
            $address,
            $this->id,
            get_class($notifiable),
            $e->getMessage()
        );
        return new \Exception($message, $e->getCode(), $e);
    }

    public function canLogin($clientId = null): bool
    {
        $isActive = $this->status === 'active';
        $isPendingWithFees = $this->status === 'pending' && $this->fees()->exists();
        $isImported = $this->status === 'limited';

        if ($clientId) {
            $clientName = DB::table('oauth_clients')->where('id', $clientId)->value('name');
            if (str_ends_with($clientName, '-all')) {
                return true;
            } else {
                return $isActive || $isPendingWithFees;
            }
        }

        return $isActive || $isPendingWithFees || $isImported;
    }

    public function findForPassport($username)
    {
        $request = app(Request::class);
        $user = $this->where('email', $username)->first();

        if ($user && !$user->canLogin($request->client_id)) {
            return null;
        }

        return $user;
    }

    /**
     * Create a notification by its name. It's already persisted.
     */
    public function insertNotification(string $command_name)
    {
        return Notification::insertForUser($this->id, $command_name);
    }
}
