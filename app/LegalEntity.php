<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class LegalEntity extends Model
{
    use ILSModel;

    /**
     * Constructor
     *
     * @param  string|null  $slug  Identifier, traditionally in kebab-case-slug.
     * @param  string|null  $name  Name
     */
    public function __construct(
        ?string $slug = null,
        ?string $name = null,
        ?string $name_plural = null,
        ?string $short_name = null,
        ?int $order = null
    ) {
        $this->slug = $slug;
        $this->name = $name;
        $this->name_plural = $name_plural;
        $this->name_short = $short_name;
        $this->order;
    }

    /**
     * Get the default values, not persisted.
     */
    public static function getDefaults(): array
    {
        $values = [];
        $values[] = new self('individual', 'persona fisica', 'persone fisiche', 'pers. fis.', 0);
        $values[] = new self('it-aps', 'Associazione di Promozione Sociale - APS', 'Associazioni di Promozione Sociale - APS', 'APS', 1);
        $values[] = new self('it-odv', 'Organizzazione di Volontariato - ODV', 'Organizzazioni di Volontariato - ODV', 'OdV', 1);

        // http://www.comune.torino.it/torinogiovani/volontariato/tipologie-di-associazione
        $values[] = new self('it-associazione-generica-o-culturale', 'Associazioni generiche o culturali', 'Associazione generica o culturale', 'ass. gen.', 1);

        $values[] = new self('it-associazione-non-riconosciuta', 'Associazione non riconosciuta', 'Associazioni non riconosciute', 'ass. n.r.', 1);

        return $values;
    }

    /**
     * Get the special Legal Entity that describes an Individual,
     * that is, recognized by the slug "individual".
     * This is cached.
     */
    public static function getIndividual(): ?LegalEntity
    {
        static $cache = false;
        if ($cache === false) {
            $cache = self::ofSlug('individual')->first();
            if (!$cache) {
                \Log::warning("System seems not configured correctly. Cannot find LegalEntity with slug=individual.");
            }
        }
        return $cache;
    }

    /**
     * Get the special Legal Entity ID that describes an Individual,
     * that is, recognized by the slug "individual".
     * This is cached.
     */
    public static function getIndividualId(): ?int
    {
        $individual = self::getIndividual();

        return $individual ? $individual->id : null;
    }

    public function getEntityNameSingular(): string
    {
        return 'Entità legale';
    }

    public function getEntityNamePlural(): string
    {
        return 'Entità legali';
    }

    /**
     * Scope a query to select a specific slug.
     */
    public function scopeOfSlug(Builder $query, string $slug): void
    {
        $query->where('slug', $slug);
    }

    /**
     * Scope a query to order by slug.
     * Note that the name is probably not indexed.
     */
    public function scopeOrderByDefault(Builder $query): void
    {
        $query->orderBy('order') // Allow to manually order them (this is indexed).
              ->orderBy('slug'); // In doubt, order by name (slug is also indexed).
    }
}
