<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assembly;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $max_members = User::selectRaw('COUNT(id) AS count')->where('status', 'active')->groupBy('address_prov')->get()->max('count');

        // Show the next Assembly, that is, the open one, or the pending one.
        $assembly_next = Assembly::where('status', 'open')->first();
        if (!$assembly_next) {
            $assembly_next = Assembly::where('status', 'pending')->first();
        }

        return view('home', [
            'max_members' => $max_members,
            'assembly_next' => $assembly_next,
        ] );
    }
}
