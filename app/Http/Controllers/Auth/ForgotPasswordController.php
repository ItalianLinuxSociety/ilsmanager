<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use Password;

use App\Config;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $username = $request->input('username');
        $email = $request->input('email');

        if (!empty($username)) {
            $response = $this->broker()->sendResetLink($request->only('username'));

            if ($response === Password::RESET_LINK_SENT) {
                return back()->with('status', trans($response));
            }
        }
        else if (!empty($email)) {
            $data = null;

            if (Config::getConfig('custom_email_aliases') == '1') {
                $parts = explode('@', $email);
                if (!empty($parts)) {
                    $user = User::where('username', $parts[0])->first();
                    if ($user)
                        $email = $user->email;
                }
            }

            $data = ['email' => $email];
            $response = $this->broker()->sendResetLink($data);

            if ($response === Password::RESET_LINK_SENT) {
                return back()->with('status', trans($response));
            }
        }
        else {
            return back();
        }

        return back()->withErrors(['email' => trans($response)]);
    }
}
