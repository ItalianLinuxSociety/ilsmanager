<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Config;
use App\User;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function howToPay()
    {
        $user = null;
        if (Auth::check()) {
            $user = Auth::user();
        }

        $fee_amount = $user
            ? $user->feeAmount
            : Config::getConfig('regular_annual_fee');

        $user_example = $user ?? $this->createExampleUser();

        $banks = Bank::all();

        // Generic page showing:
        //   - how to pay the first time
        //   - how to renew
        return view('page.how-to-pay', [
            'banks' => $banks,
            'fee_amount' => $fee_amount,
            'user' => $user,
            'user_example' => $user_example,
        ]);
    }

    private function createExampleUser(): User
    {
        $user = new User;
        $user->name = 'Nome';
        $user->surname = 'Cognome';

        return $user;
    }
}
