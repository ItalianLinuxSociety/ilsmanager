<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Section;
use App\User;
use App\Role;
use App\Volunteer;

class SectionController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Section::class,
            'view_folder' => 'section'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'city' => 'required|max:255',
            'prov' => 'required|max:2',
        ];
    }

    protected function requestToObject(Request $request, $object): void
    {
        $object->city = $request->input('city');
        $object->prov = $request->input('prov');
        $object->website = $request->input('website');
    }

    protected function defaultSortingColumn()
    {
        return 'city';
    }

    public function update(Request $request, $id)
    {
        $currentuser = $request->user();
        $section = Section::find($id);
        $this->authorize('update', $section);

        if ($request->has('user')) {
            $users = $request->input('user');
            $roles = $request->input('role');
            $referent_role = Role::where('name', 'referent')->first();

            for ($i = 0; $i < count($users); $i++) {
                $u = $users[$i];
                $r = $roles[$i];

                $user = User::find($u);

                if ($currentuser->hasRole('admin') || $user->section_id == $currentuser->section_id) {
                    if ($r == 'referent') {
						if ($user->hasRole('referent') == false) {
							$user->roles()->attach($referent_role->id);
						}
                    }
                    else {
                        $user->roles()->detach($referent_role->id);
                    }
                }
            }

            return redirect()->route($this->view_folder . '.edit', $id);
        }
        else if ($request->has('volunteer')) {
            $volunteers = $request->input('volunteer');
            $names = $request->input('name');
            $surnames = $request->input('surname');
            $emails = $request->input('email');
            $to_remove = $request->input('remove', []);

            for ($i = 0; $i < count($volunteers); $i++) {
                $v = $volunteers[$i];
                $n = $names[$i];
                $s = $surnames[$i];
                $e = $emails[$i];

                $volunteer = Volunteer::find($v);

                if (in_array($v, $to_remove)) {
                    if ($currentuser->can('delete', $volunteer))
                        $volunteer->delete();
                }
                else {
                    if ($currentuser->can('update', $volunteer)) {
                        $volunteer->name = $n;
                        $volunteer->surname = $s;
                        $volunteer->email = $e;
                        $volunteer->save();
                    }
                }
            }

            return redirect()->route($this->view_folder . '.edit', $id);
        }
        else {
            return parent::update($request, $id);
        }
    }

    public function balance(Request $request, $id)
    {
        $section = Section::find($id);
        $this->authorize('view', $section);

        $year = $request->input('year', date('Y'));
        $rows = $section->economic_logs()->where(DB::raw('YEAR(created_at)'), $year)->orderBy('created_at', 'desc')->get();

        return view('section.balance', [ "section" => $section, "year" => $year, "rows" => $rows ]);
    }

    /**
     * Generate a CSV file from the visible members
     * This can be useful, for example, to bulk-email people
     * So, for commodity reasons, a public e-mail address is exported too.
     */
    public function exportMembers(Request $request, $id) {
        $section = Section::find($id);

        // this is probably too much strict for just viewing some public info
        // but I don't want to overthing
        $this->authorize('update', $section);

        // avoid to break the CSV
        \Debugbar::disable();

        // create a temporary file to build a nice CSV file
        $csv_handle = fopen('php://memory', 'r+');
        if($csv_handle === false) {
            throw new Exception("Cannot create file");
        }

        // HTTP headers to render a CSV file
        header("Content-Type: text/csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=members.csv");

        $csv_header = [
            'UserID',
            'UserNick',
            'UserPublicMail',
            'UserName',
            'UserSurname',
            'UserIsReferent',
        ];
        fputcsv($csv_handle, $csv_header);

        foreach($section->users as $user) {

            $is_referent = $user->hasRole('referent');
            $is_referent_human = $is_referent ? 'Y' : 'N';

            // CSV row
            // all of these are public info that can be disclosed to section coordinators
            $csv_row = [
                $user->id,
                $user->username,
                // The Custom Email is a Public Email that can be shared at least to section coordinators.
                // For privacy reasons we do not export the private e-mail here, but that could change after further discussions about data protection.
                $user->custom_email,
                $user->name,
                $user->surname,
                $is_referent_human,
            ];
            fputcsv($csv_handle, $csv_row);
        }

        // Render the output
        // I copy-pasted this from something else,
        // I have not a strong opinion about this,
        // so, feel free to replace if you have a better approach
        // This seems horrible to me but hey, it works.
        // -- boz, 2023-05-18
        rewind($csv_handle);
        $output = stream_get_contents($csv_handle);
        fclose($csv_handle);

        return $output;
    }

}
