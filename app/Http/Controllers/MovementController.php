<?php
/**
    ILS Manager
    Copyright (C) 2021-2024 Italian Linux Society, contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace App\Http\Controllers;

use App\AccountRow;
use App\Actions\ImportPayPalMovements as ActionsImportPayPalMovements;
use App\Actions\MovementUnicreditRefreshParsingNotes;
use App\Bank;
use App\Config;
use App\Fee;
use App\Movement;
use App\Refund;
use App\Section;
use App\User;
use App\Services\FinancialSummaryService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovementController extends EditController
{
    protected $classname = Movement::class;
    protected $view_folder = 'movement';

    public function index(Request $request)
    {
        $this->checkAuth();

        // force integer
        $year = (int) $request->input('year', date('Y'));
        if ($year < 1000) {
            abort(500, 'whaat');
        }

        $current_year = (int)date('Y');
        $is_current_year = $year === $current_year;

        // Filtri per utente e sezione
        $userId = $request->input('user_id');
        $sectionId = $request->input('section_id');

        $objects = AccountRow::with('movement')->whereHas('movement', function ($query) use ($year) {
            $query->whereYear('date', $year)
                ->whereNull('start_of_year');
        })
            ->when($userId, function ($query, $userId) {
                return $query->where('user_id', $userId);
            })
            ->when($sectionId, function ($query, $sectionId) {
                return $query->where('section_id', $sectionId);
            })
            ->orderBy('id', 'desc')->get()->sortByDesc(function ($a) {
                return $a->movement->date;
            });

        // TODO: expand and put in getRequestArguments
        $request_args =[
            'year' => $year,
            'user_id' => $userId,
            'section_id' => $sectionId,
        ];

        // Get incomes grouped by month and year.
        // Note that a month can be missing if nothing was done in that month.
        $incomes_by_month = $this->getAmountByMonth(
            $this->queryFilteredMovementsSumIncomes($request_args)
                ->whereNull('start_of_year')
        );
        // Get incomes from previous year.
        $incomes_start_of_year = (float)
            $this->queryFilteredMovementsSumIncomes($request_args)
                ->whereNotNull('start_of_year')
                ->value('amount');

        // Fill all the months (incomes).
        $incomes_per_month = [];
        $incomes_per_month[] = $incomes_start_of_year;
        for ($i = 1; $i <= 12; $i++) {
            $income_date = $year . '-' . str_pad($i, 2, '0', STR_PAD_LEFT);
            $incomes_per_month[] = (float) ($incomes_by_month[$income_date] ?? 0);
        }

        // Get costs grouped by month and year.
        // Note that a month can be missing if nothing was done in that month.
        $costs_by_month = $this->getAmountByMonth(
            $this->queryFilteredMovementsSumOutcomes($request_args)
                ->whereNull('start_of_year')
         );
        $costs_start_of_year = (float)
            $this->queryFilteredMovementsSumOutcomes($request_args)
                ->whereNotNull('start_of_year')
                ->value('amount');

        // Fill all the months (costs).
        $costs_per_month = [];
        $costs_per_month[] = $costs_start_of_year;
        for ($i = 1; $i <= 12; $i++) {
            $cost_date = $year . '-' . str_pad($i, 2, '0', STR_PAD_LEFT);
            $costs_per_month[] = (float) ($costs_by_month[$cost_date] ?? 0);
        }

        $fees_account = Config::feesAccount();
        $membership_forecast_date_raw = 'CONCAT((year+1), "-", date_format(date, "%m"))';
        $membership_forecast = Fee::query()
            ->selectRaw('SUM(account_rows.amount_in) AS amount, ' . $membership_forecast_date_raw . ' AS date')
            ->join('account_rows', 'fees.account_row_id', '=', 'account_rows.id')
            ->join('movements', 'account_rows.movement_id', '=', 'movements.id')
            ->when($userId, function ($query, $userId) {
                return $query->where('account_rows.user_id', $userId);
            })
            ->when($sectionId, function ($query, $sectionId) {
                return $query->where('account_rows.section_id', $sectionId);
            })
            ->where('fees.year', $year - 1)
            ->where('account_rows.account_id', $fees_account->id)
            ->groupBy(DB::raw($membership_forecast_date_raw))
            ->pluck('amount', 'date')->toArray();

        $forecast_incomes_per_month = [];
        $forecast_incomes_per_month[] = 0; // Start of year.
        for ($i = 1; $i <= 12; $i++) {
            $forecast_date = $year . '-' . str_pad($i, 2, '0', STR_PAD_LEFT);
            $forecast_incomes_per_month[] = (float) ($membership_forecast[$forecast_date] ?? 0);
        }

        $forecast_costs_per_month = [];
        $forecast_costs_per_month[] = 0; // Start of year.
        for ($i = 1; $i <= 12; $i++) {
            $forecast_date = $year . '-' . str_pad($i, 2, '0', STR_PAD_LEFT);
            $forecast_costs_per_month[] = 0;
        }

        $real_revenue_per_month = [];
        $progress = 0;
        foreach ($incomes_per_month as $period => $income) {
            if (!$is_current_year || $period <= (int) date('n')) {
                $real_revenue_per_month[$period] = $income + $costs_per_month[$period] + $progress;
                $progress = $real_revenue_per_month[$period];
            }
        }

        $forecast_revenue_per_month = [];
        $progress = 0;
        foreach ($forecast_incomes_per_month as $period => $income) {
            if (!$is_current_year || $period <= (int) date('n')) {
                $forecast_revenue_per_month[$period] = $real_revenue_per_month[$period];
                $progress = $forecast_revenue_per_month[$period];
            } else {
                $forecast_revenue_per_month[$period] = $income + $costs_per_month[$period] + $progress;
                $progress = $forecast_revenue_per_month[$period];
            }
        }

        $financialSummary = new FinancialSummaryService([
            'year' => $year,
            'selectedUserId' => $userId,
            'selectedSectionId' => $sectionId,
        ]);

        return view('movement.index', [
            'objects' => $objects,
            'year' => $year,
            'real_incomes' => $incomes_per_month,
            'forecast_incomes' => $forecast_incomes_per_month,
            'real_costs' => $costs_per_month,
            'forecast_costs' => $forecast_costs_per_month,
            'real_revenue' => $real_revenue_per_month,
            'forecast_revenue' => $forecast_revenue_per_month,
            'membership_forecast' => $membership_forecast,
            'users' => User::all(),
            'sections' => Section::all(),
            'selected_user_id' => $userId,
            'selected_section_id' => $sectionId,
            'financialSummary' => $financialSummary,
        ]);
    }

    private function queryFilteredMovements(array $request_args)
    {
        $year = $request_args['year'];
        $userId = $request_args['user_id'];
        $sectionId = $request_args['section_id'];
        return DB::table('movements')
            ->join('account_rows', 'movements.id', '=', 'account_rows.movement_id')
            ->whereYear('movements.date', $year)
            ->when($userId, function ($query, $userId) {
                return $query->where('account_rows.user_id', $userId);
            })
            ->when($sectionId, function ($query, $sectionId) {
                return $query->where('account_rows.section_id', $sectionId);
            });
    }

    private function queryFilteredMovementsSumIncomes(array $request_args)
    {
        return $this->queryFilteredMovements($request_args)
            ->selectRaw('sum(account_rows.amount_in) as amount')
            ->where('amount_in', '>', 0);
    }

    private function queryFilteredMovementsSumOutcomes(array $request_args)
    {
        return $this->queryFilteredMovements($request_args)
            ->selectRaw('-sum(account_rows.amount_out) as amount')
            ->where('amount_out', '>', 0);
    }

    private function getAmountByMonth($query)
    {
            return $query
                ->selectRaw('date_format(movements.date, "%Y-%m") as date')
                ->groupBy(DB::raw('date_format(movements.date, "%Y-%m")'))
                ->pluck('amount', 'date')->toArray();
    }

    /**
     * Parse and store the PayPal file with movements
     *
     * @TODO: rename as "import"
     */
    public function store(Request $request)
    {
        $this->checkAuth();

        if ($request->input('store_from') === 'paypal_api') {
            // eventually upload via PayPal API
            $this->storeFromPayPalAPI($request);
        } else {
            // load from file
            $this->storeFromFile($request);
        }

        return redirect()->route('movement.review');
    }

    /**
     * Refresh data from remote server, if possible.
     *
     * @param  int  $id  Movement ID
     */
    public function refresh($id)
    {
        $this->checkAuth();
        $movement = Movement::findOrFail($id);
        if (! $movement->supportsRefresh()) {
            abort(400, 'This Movement cannot be refreshed.');
        }

        // Refresh data of this exact Movement.
        $action = new ActionsImportPayPalMovements;
        $action->handle([
            'movement' => $movement,
        ]);

        return back();
    }

    public function review()
    {
        $this->checkAuth();
        $pendings = Movement::where('reviewed', 0)->orderBy('date', 'asc')->paginate(20);

        return view('movement.review', [
            'pendings' => $pendings,
        ]);
    }

    public function attach(Request $request, $id)
    {
        $this->checkAuth();
        $movement = Movement::find($id);
        $movement->attachFile($request->file('file'));

        return redirect()->route('movement.edit', $id);
    }

    public function refund(Request $request, $id)
    {
        $this->checkAuth();

        $ids = $request->input('refund_id', []);
        $refund = null;

        DB::beginTransaction();

        foreach ($ids as $refund_id) {
            $refund = Refund::find($refund_id);
            $refund->movement_id = $id;
            $refund->refunded = true;
            $refund->save();
        }

        if ($refund && $refund->section) {
            $movement = Movement::find($id);
            foreach ($movement->account_rows as $ar) {
                $ar->section_id = $refund->section_id;
                $ar->save();
            }
        }

        DB::commit();

        return redirect()->route('movement.edit', $id);
    }

    public function download(Request $request, string $id, string $file)
    {
        // Require to be an administrator
        $this->checkAuth();

        // Administrators can see whatever movement, just by ID.
        $query = Movement::query()->where('id', $id);
        $movement = $query->firstOrFail();

        // Do not allow to specify a directory.
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/125
        $file_safe = basename($file);

        // Generate the complete path to this Movement's file and cause download.
        $movement_safe_path = storage_path("accounting/{$movement->id}/{$file_safe}");

        return response()->download($movement_safe_path);
    }

    protected function defaultValidations($object)
    {
        $validations = [];
        if (! $object->id) {
            $validations['amount'] = 'required';
            $validations['date'] = 'required|date';
            $validations['bank_id'] = 'required|int';
            $validations['notes'] = 'required|string';
            $validations['start_of_year'] = 'int';
        }

        return $validations;
    }

    protected function findObjectById($id)
    {
        $m = parent::findObjectById($id);
        if ($m->id && ! $m->reviewed) {
            $m->guessRows();
        }

        return $m;
    }

    protected function requestToObject(Request $request, $m): void
    {
        // Some attributes should not be changed after creation in normal conditions.
        if (! $m->id) {
            $m->bank_id = $request->input('bank_id');
            $m->start_of_year = $request->input('start_of_year', null);
            $m->date = $request->input('date');
            $m->amount = $request->input('amount');
            $m->notes = $request->input('notes');

            if (empty($m->bank) && $m->bank_id) {
                $m->bank = Bank::where('id', $m->bank_id)->firstOrFail();
            }
        }
    }

    protected function afterSaving($request, $object)
    {
        $m = $object;

        $fees_account = Config::feesAccount();

        // These are arrays.
        $account_rows = $request->input('account_row');
        $accounts = $request->input('account');
        $amounts = $request->input('ar_amount');
        $users = $request->input('user');
        $sections = $request->input('section');
        $notes = $request->input('ar_notes');

        // The user may click "Delete" on an AccountRow.
        // This is a flat array, and the order of deletion is not important.
        // This comes from an hidden input created by JavaScript.
        // This has only sense if the Movement already exists (and in afterSaving() this is always true).
        $account_rows_delete = $request->input('delete_account_row', []);
        foreach ($account_rows_delete as $account_row_delete_id) {
            AccountRow::query()
                ->lockForUpdate()
                ->where('movement_id', $m->id)
                ->findOrFail($account_row_delete_id)
                ->delete();
        }

        foreach ($account_rows as $index => $account_row_id) {
            $ar_account_id = $accounts[$index] ?? null;
            $ar_section_id = $sections[$index] ?? null;
            $ar_user_id = $users[$index] ?? null;
            $ar_notes = $notes[$index] ?: '';
            $ar_amount = $amounts[$index];

            if ($account_row_id === 'new') {
                // Create a new AccountRow.

                // Skip empty templates.
                if (empty($ar_notes) && empty($ar_account_id)) {
                    continue;
                }

                $ar = new AccountRow;
                $ar->movement_id = $m->id;
            } else {
                $ar = AccountRow::query()
                    ->lockForUpdate()
                    ->find($account_row_id);

                if (! $ar) {
                    continue;
                }
            }

            $ar->account_id = $ar_account_id;
            $ar->user_id = $ar_user_id;
            $ar->section_id = $ar_section_id;
            $ar->notes = $ar_notes;
            $ar->amount = $ar_amount;
            $ar->save();
        }

        $m->generateReceipt();
    }

    /**
     * Load movements from file
     */
    private function storeFromFile(Request $request)
    {
        DB::beginTransaction();
        $bank = Bank::find($request->input('bank_id'));

        // no file no party
        if (! $request->file('file')) {
            abort(400, 'Please pick a file');
        }

        if ($request->file('file')->isValid()) {
            switch ($bank->type) {
                case 'paypal':
                    throw new Exception('Please do not import PayPal movements from file. Import from API instead.');
                    break;

                case 'unicredit':
                    $header_skipped = false;

                    $unicredit_content = file_get_contents($request->file->path());
                    $unicredit_rows = explode("\n", $unicredit_content);

                    // IMPORTANT: Do not try to consider the Unicredit's CSV like a CSV.
                    // Unicredit is not aware of RFC 4180. Don't use fgetcsv().
                    // So we try to be aggressive on first and last rows,
                    // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/58
                    // https://regex101.com/r/Vw4mK2/1
                    //                                                                                                   /- Group 3
                    //                                                                                                  /   Malicious user's summary
                    //                                       /- Group 1                   /- Group 2                   /
                    //                                      /   dd/mm/yyyy               /  date dd/mm/yyyy           /     /- Group 4   /- Group 5
                    //                                     /                            /                            /     /   Amount   /   Unicredit payment ID
                    $UNICREDIT_INSANE_COLUMN_PATTERN = '@^([0-9]{2}/[0-9]{2}/[0-9]{4});([0-9]{2}/[0-9]{2}/[0-9]{4});(.+?);(-?[0-9.,]+);([0-9]+)$@';
                    $UNICREDIT_EXPECTED_COLUMNS = 5;
                    $unicredit_something = true;
                    $unicredit_heading_found = false;
                    foreach ($unicredit_rows as $i => $unicredit_row) {
                        $unicredit_row_clean = trim($unicredit_row);
                        if (empty($unicredit_row_clean)) {
                            continue;
                        }

                        // Avoid completely nonsense rows starging with this phrase.
                        if (strpos($unicredit_row_clean, 'Situazione al ') === 0) {
                            continue;
                        }

                        // Avoid completely nonsense data. This only happens sometime (!)
                        if (strpos($unicredit_row_clean, 'Saldo Euro') === 0) {
                            continue;
                        }

                        // Avoid completely nonsense data. This also only happens sometime (!)
                        if (strpos($unicredit_row_clean, 'Ultimi movimenti') === 0) {
                            continue;
                        }

                        // Avoid partially nonsense data. This has valid columns but invalid data (heading).
                        if (strpos($unicredit_row_clean, 'Data;Valuta;Descrizione') === 0) {
                            $unicredit_heading_found = true;

                            continue;
                        }

                        // Insanely parse the row.
                        $unicredit_insane_matches = [];
                        $n_matches = preg_match_or_throw($UNICREDIT_INSANE_COLUMN_PATTERN, $unicredit_row_clean, $unicredit_insane_matches);

                        // Be very strict if we have not found any heading yet. So we catch Unicredit nonsenses and we can improve the parser
                        // without causing silent import nonsenses.
                        if ($n_matches !== 1) {
                            throw new Exception(sprintf("Unexpected Unicredit row: '%s' not matching '%s'", $unicredit_row, $UNICREDIT_INSANE_COLUMN_PATTERN));
                        }

                        // The frist array row from preg_match() is just the whole line. Strip that.
                        array_shift($unicredit_insane_matches);

                        // The "$c" is alias for "columns".
                        $c = $unicredit_insane_matches;

                        $unicredit_something = true;

                        try {
                            $movement = new Movement;
                            $movement->amount = parse_unicredit_number($c[3]);
                            $movement->bank_id = $bank->id;
                            $movement->date = implode('-', array_reverse(explode('/', $c[0])));
                            $movement->notes = $c[2];

                            // Eventually populate the "amount_original".
                            // Eventually create an AccountRow for the bank fees.
                            // Eventually update already-existing data.
                            $action = new MovementUnicreditRefreshParsingNotes($movement);
                            $action->handle();
                            $action->pleaseExpandExistingDuplicate();
                            $action->save();

                        } catch (Exception $e) {
                            echo $e->getMessage() . '<br>';
                            print_r($c);
                            DB::rollBack();
                            exit();
                        }
                    }

                    // No data? No heading? WTF.
                    if (! $unicredit_something && ! $unicredit_heading_found) {
                        throw new Exception('We have not found a valid Unicredit file with the expected heading.');
                    }

                    break;
            }
        }

        DB::commit();
    }

    /**
     * Load movements from PayPal API
     *
     * @author Marco Acorte
     */
    private function storeFromPayPalAPI(Request $request)
    {
        $date_start = $request->input('date_from');
        $date_to = $request->input('date_to');

        $action = new ActionsImportPayPalMovements;
        $action->handle([
            'date_start' => $date_start,
            'date_to' => $date_to,
        ]);
    }
}
