<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Votation;

class VotationController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Votation::class,
            'view_folder' => 'votation'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'assembly_id' => 'required',
            'question' => 'required',
            'status' => 'required|max:255',
            'max_options' => 'required',
        ];
    }

    protected function requestToObject(Request $request, $object): void
    {
        // No need to check "authorize". The EditController already protects this method.
        // Check the VotationPolicy class.

        $object->assembly_id = $request->input('assembly_id');
        $object->question = $request->input('question');
        $object->status = $request->input('status');
        $object->max_options = $request->input('max_options');
        $object->options = array_filter($request->input('option', []));
    }

    protected function getRouteAfterSave($votation)
    {
        return redirect()->route('assembly.edit', $votation->assembly->id);
    }

    public function index(Request $request)
    {
        return redirect()->route('assembly.index');
    }

    public function edit($id)
    {
        return redirect()->route('assembly.edit', $id);
    }
}
