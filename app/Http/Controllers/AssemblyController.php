<?php

namespace App\Http\Controllers;

use App\Assembly;
use App\AssemblyUser;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AssemblyController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Assembly::class,
            'view_folder' => 'assembly'
        ]);
    }

    protected function requestToObject(Request $request, $object): void
    {
        // The EditController already protects the "edit" action calling AssemblyPolicy#edit().

        $fields = ['date', 'status', 'announce'];
        $this->fitObject($object, $fields, $request);
    }

    protected function defaultValidations($object)
    {
        return [
            'date' => 'required',
            'status' => 'required|in:pending,open,closed',
            'announce' => 'required',
        ];
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function defaultSortingDirection()
    {
        return 'desc';
    }

    /**
     * @return array
     */
    protected function getEditArguments($id)
    {
        $data = parent::getEditArguments($id);

        // Get the current Assembly.
        $assembly = $data['object'];

        // Get the users that are participating (with or without delegation).
        $data['assembly_users'] =
            $assembly
                ->users()
                ->orderByUserPrintableName()
//              ->chaperone()      // Micro-optimization for N+1 problem on Assembly. TODO: adopt this to replace setAttribute() after Laravel 11.22.
                ->with('user')     // Micro-optimization for N+1 problem on AssemblyUser.user
                ->with('delegate') // Micro-optimization for N+1 problem on AssemblyUser.delegate
                ->get();

        // Micro-optimization for N+1 problem on Assembly.
        // TODO: use just chaperone() after we upgrade to Laravel 11.22.
        foreach ($data['assembly_users'] as $assembly_user) {
            $assembly_user->setAttribute('assembly', $assembly);
        }

        // Check if the current user is somehow participating (with or without delegation).
        // Micro-optimization: we avoid a query by just filtering previous results.
        $data['user_participation'] =
            $data['assembly_users']
                ->filter( function ($assembly_user) use (&$data) {
                    if ($assembly_user->user->id === Auth::user()->id) {
                        return $assembly_user->user;
                    }
                } )
                ->first();

        // Check if the current user has delegated to somebody else. Get that somebody else.
        $data['user_voting_for_me'] = $data['user_participation']->delegate ?? null;

        return $data;
    }

    /**
     * Add myself as direct participant (and potential voter) in one Assembly.
     */
    public function partecipate(Request $request, $assembly_id)
    {
        // Get the Assembly, or page not found.
        $assembly = Assembly::findOrFail($assembly_id);

        // Make sure you have enough permissions to append your participation in this Assembly,
        // calling AssemblyPolicy#participate().
        // Do not allow to participate in closed assemblies, etc.
        $this->authorize('participate', $assembly);

        // Update everything, or rollback everything on error.
        DB::transaction(function () use ($request, $assembly) {
            // Cleanup my (old?) direct participation and my delegation(s) (if any).
            $this->removeDirectParticipationIfAuthorized($request, $assembly);
            $this->removeDelegationsAssignedToMeIfAuthorized($request, $assembly);

            // Add my direct participation.
            // Delegate my vote to the selected user.
            $delegate = new AssemblyUser;
            $delegate->assembly_id = $assembly->id;
            $delegate->user_id = Auth::user()->id;
            $delegate->delegate_id = 0; // TODO: make 'delegate_id' nullable.
            $delegate->save();
        });

        // Return back to the single Assembly.
        return redirect()->route('assembly.show', $assembly_id);
    }

    /**
     * Remove myself as participant in one Assembly,
     * eventually nuking the delegations on myself.
     */
    public function unpartecipate(Request $request, $assembly_id)
    {
        // Get the Assembly, or page not found.
        $assembly = Assembly::findOrFail($assembly_id);

        // Update everything, or rollback everything on error.
        DB::transaction(function () use ($request, $assembly) {
            // Detach my direct participation and my delegation(s),
            // only if I'm authorized, if the assembly is not closed, etc.
            $this->removeDirectParticipationIfAuthorized($request, $assembly);
            $this->removeDelegationsAssignedToMeIfAuthorized($request, $assembly);
        });

        // Return back to the single Assembly.
        return redirect()->route('assembly.show', $assembly_id);
    }

    public function delegate(Request $request, $assembly_id, $delegate_id)
    {
        // Get the Assembly and the target User, or page not found.
        $assembly = Assembly::findOrFail($assembly_id);
        $delegate_user = User::findOrfail($delegate_id);

        // Make sure you have enough permissions to set your delegation in this Assembly,
        // calling AssemblyPolicy#delegate().
        $this->authorize('delegate', [$assembly, $delegate_user]);

        // Update everything, or rollback everything on error.
        DB::transaction(function () use ($request, $assembly, $delegate_user) {
            // First, remove my direct participation, if any,
            // only if I'm authorized, if the assembly is not closed, etc.
            $this->removeDirectParticipationIfAuthorized($request, $assembly);

            // Delegate my vote to the selected user.
            $delegate = new AssemblyUser;
            $delegate->assembly_id = $assembly->id;
            $delegate->user_id = Auth::user()->id;
            $delegate->delegate_id = $delegate_user->id;
            $delegate->save();
        });

        // Return back to the single Assembly.
        return redirect()->route('assembly.show', $assembly_id);
    }

    public function undelegate(Request $request, $assembly_id)
    {
        // Get the Assembly, or page not found.
        $assembly = Assembly::findOrFail($assembly_id);

        // Make sure you have enough permissions to remove my delegation,
        // calling AssemblyPolicy#undelegate.
        $this->authorize('undelegate', $assembly);

        // Update everything, or rollback everything on error.
        DB::transaction(function () use ($request, $assembly) {
            $this->removeDirectParticipationIfAuthorized($request, $assembly);
        });

        // Return back to the single Assembly.
        return redirect()->route('assembly.show', $assembly_id);
    }

    /**
     * Drop my direct participation, if set, if I'm authorized to so so.
     * This method does NOT open a transaction.
     * You may want to manually do it.
     */
    private function removeDirectParticipationIfAuthorized(Request $request, Assembly $assembly)
    {
        // Make sure you have enough permissions to append your participation in this Assembly,
        // calling AssemblyPolicy#unparticipate().
        $this->authorize('unparticipate', $assembly);

        // Drop my direct participation (if any).
        $assembly->users()->whereUserIsMe()->delete();
    }

    /**
     * Drop my delegations (people who delegated to me), if any, if I'm authorized to do so.
     * This method does NOT open a transaction.
     * You may want to manually do it.
     */
    private function removeDelegationsAssignedToMeIfAuthorized(Request $request, Assembly $assembly)
    {
        // Make sure you have enough permissions to append your participation in this Assembly,
        // calling AssemblyPolicy#unparticipate().
        $this->authorize('undelegate', $assembly);

        // Drop all the delegations I have (if any).
        $assembly->users()->whereDelegateUserIsMe()->delete();
    }
}
