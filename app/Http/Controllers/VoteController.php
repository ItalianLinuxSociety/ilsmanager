<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Votation;
use App\Vote;

class VoteController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Vote::class,
            'view_folder' => 'vote'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'votation_id' => 'required',
        ];
    }

    /**
     * In this hook, you should put your request into the object.
     * DO **NOT** SAVE() INSIDE THIS METHOD PLEASE.
     * Inside this method, your object may not have an ID.
     * If you need an ID, check afterSaving() instead.
     */
    protected function requestToObject(Request $request, $vote): void
    {
        // This will create your personal Vote.
        // Here we do NOT create the vote of the delegations.
        $vote->votation_id = $request->input('votation_id');
        $vote->user_id = Auth::user()->id;
        $vote->answer = $this->getRequestAnswer($request, $vote->user_id);
    }

    /**
     * In this hook, your entity has the ID.
     * @param type $request
     * @param type $object
     * @return type
     */
    protected function afterSaving($request, $vote)
    {
        // This will create the vote fot the delegations.
        // Here we do NOT create the personal vote.

        $user = $request->user();
        $votation_id = $vote->votation_id;

        // Make sure you have enough permissions to vote in this Votation.
        $votation = Votation::find($votation_id);
        $this->authorize( 'vote', $votation );

        // Assign the same vote to each delegated user.
        // Note that this also include the current User.
        foreach($votation->assembly->delegations($user) as $voter) {
            $vote = Vote::where('user_id', $voter->id)->where('votation_id', $votation_id)->first();
            if (is_null($vote)) {
                $vote = new Vote();
                $vote->user_id = $voter->id;
                $vote->votation_id = $votation->id;
            }

            $vote->answer = $this->getRequestAnswer($request, $voter->id);

            if (count($vote->answer) > $votation->max_options) {
                Log::error('Non salvo voto: numero di scelte superiore a quello concesso');
            } else {
                $vote->save();
            }
        }
    }

    protected function getRouteAfterSave($vote)
    {
        return redirect()->route('assembly.show', $vote->votation->assembly->id);
    }

    private function getRequestAnswer($request, $user_id)
    {
        return array_filter($request->input('vote_' . $user_id, []));
    }

    public function index(Request $request)
    {
        return redirect()->route('assembly.index');
    }

    public function edit($id)
    {
        return redirect()->route('assembly.edit', $id);
    }
}
