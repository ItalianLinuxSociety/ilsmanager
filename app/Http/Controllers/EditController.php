<?php

namespace App\Http\Controllers;

use BadMethodCallException;

use DB;

use Illuminate\Http\Request;

use Auth;

use App\User;

use Log;

abstract class EditController extends Controller
{
    protected $classname;
    protected $view_folder;

    /**
     * This is just a cache.
     * This is an associative array of identifiers and objects.
     * @return array
     */
    private $objects = [];

    /**
     * Get the page default elements limit.
     * So, if the user has not specified anything, this is the default limit.
     * @return int
     */
    protected function getPageElementsDefaultLimit()
    {
        return 20;
    }

    /**
     * Get the page maximum allowed limit.
     * @return int
     */
    protected function getPageElementsMaxLimit()
    {
        // The default maximum limit depends on the current limit.
        // If you are admin, the maximum limit is bigger. lol
        $limit = $this->getPageElementsDefaultLimit();
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            $max_limit = $limit * 10;
        } else {
            $max_limit = $limit * 2;
        }
        return $max_limit;
    }

    /**
     * Get the page elements limit. This is user-customized.
     * @return int
     */
    protected final function getPageElementsLimit(Request $request)
    {
        // Allow the user to set a custom limit.
        if ($request->input('limit') > 0) {
            $limit = $request->input('limit');
        } else {
            $limit = $this->getPageElementsDefaultLimit($request);
        }

        // But not over a specific threshold.
        $max = $this->getPageElementsMaxLimit();
        if ($limit > $max) {
            Log::warning("Trying to request bigger page limit - requested {$limit} allowed {$max}");
        }

        return min($limit, $this->getPageElementsMaxLimit());
    }

    /**
     * Get the generic page title of this controller.
     * This may be shown in the breadcrumb.
     * @return string
     */
    protected function getIndexPageTitle() {
        // Just a sane default. Please override me.
        $example = $this->getObject(0);
        try {
            return $example->getEntityNamePlural();
        } catch (BadMethodCallException $ex) {
            Log::debug(sprintf("The entity %s should extend %s, or the controller %s should override protected function getIndexPageTitle()",
                $example::class,
                ILSModel::class,
                $this::class));

            // Show something.
            return class_basename($example);
        }
     }

    /**
     * Get the page title of this controller when you are seeing a single object.
     * This may be shown in the breadcrumb.
     * @return string
     */
    protected function getSingleObjectPageTitle($id) {
        // Just a sane default. Please override me.
        $object = $this->getObject($id);
        return $this->getEntityAndObjectName($object);
    }

    /**
     * Get the entity and the object name, together.
     * This may be shown in a breadcrumb.
     * @param wild $object
     * @return string
     */
    protected function getEntityAndObjectName($object)
    {
        try {
            $entity_name = $object->getEntityNameSingular();
            $object_name = $object->getObjectShortName();
        } catch(\BadMethodCallException $e) {
            $entity_name = get_class($object);
            $entity_name = basename(str_replace( '\\', '/', $entity_name));
            $object_name = $object->id;
            Log::debug(sprintf("The entity %s should have trait %s with protected function getEntityAndObjectName() and getObjectShortName()", get_class($object), ILSModel::class));
        }
        return sprintf($this->getEntityAndObjectNameFormat(),
            $entity_name,
            $object_name
        );
    }

    /**
     * Get the format that prints the entity and object name, in a page title.
     * @return string
     */
    protected function getEntityAndObjectNameFormat()
    {
        // entity name, space, object name
        return '%1$s: %2$s';
    }

    protected function init($options)
    {
        $this->classname = $options['classname'];
        $this->view_folder = $options['view_folder'];
    }

    /**
     * Sometime you need some Request arguments, with some custom defaults.
     * Use this method!
     * @param Request $request
     * @return array
     */
    protected function getIndexRequestArgs(Request $request): array
    {
        return [
            // This limit is read by the file 'commons/limit.blade.php'.
            'limit' => $this->getPageElementsLimit($request),
            // This order_dir is read by the file 'commons/orderby.blade.php'.
            'order_dir' => $this->getOrderDirection($request),
        ];
    }

    protected function getIndexArguments(Request $request)
    {
        $query = $this->queryList($request);
        $args = [
            'query' => $query,
            'objects' => $this->getPaginatedObjects($request, $query),
            'default_object' => $this->createObject($request),
            'breadcrumbs' => $this->getIndexBreadcrumbs(),
            'site_title' => $this->getIndexPageTitle(),
            // This availables_order_dir is read by the file 'commons/orderby.blade.php'.
            'availables_order_dir' => $this->getAvailablesOrderDirections(),
        ];
        // Additionally, expose the arguments that are supposed to be changed
        // from request to request, as default, since it's usually useful
        // in the index view.
        return array_replace($this->getIndexRequestArgs($request), $args);
    }

    protected final function getPaginatedObjects(Request $request, $query)
    {
        return $query->paginate($this->getPageElementsLimit($request))
            ->withQueryString();
    }

    public function index(Request $request)
    {
        $this->authorize('index', $this->classname);
        $args = $this->getIndexArguments($request);
        return view($this->view_folder . '.index', $args);
    }

    /**
     * Create a new, un-persisted, object.
     */
    protected function createObject(Request $request)
    {
        $object = new $this->classname;
        $this->requestToObject($request, $object);
        return $object;
    }

    /**
     * Quickly create an entity.
     * This is just a shortcut for the edit view,
     * but double-checking for the "create" policy.
     * @param Request $request
     * @return type
     */
    public function create(Request $request)
    {
        $edit_args = $this->getEditArguments(0);
        $object = $edit_args['object'];
        $this->authorize('create', $object);
        return view($this->view_folder . '.edit', $edit_args);
    }

    /**
     * Persist a new entity from scratch.
     */
    public function store(Request $request)
    {
        // Check the Policy against the class name.
        // Later we will also check the object instance.
        // This is probably not necessary but very fast and safer,
        // so, why not :D
        $this->authorize('create', $this->classname);

        // Prepare the unpersisted object from the current request.
        $object = $this->createObject($request);
        $validations = $this->defaultValidations(null);
        $request->validate($validations);

        // Check if you are allowed to persist it.
        $this->authorize('create', $object);

        // TODO: refactor and unify this logic with the same logic in update().
        DB::beginTransaction();
        $object->save();
        $this->afterSaving($request, $object);
        DB::commit();

        // Redirect to the created element.
        return $this->getRouteAfterSave($object);
    }

    /**
     * Show an already existing entity, from its primary identifier.
     */
    public function show($id)
    {
        // The page 'show' is very similar to the 'edit',
        // but it just checks whenever you can see this single object.
        $object = $this->getObject($id);
        $this->authorize('view', $object);

        // Get same page arguments of the 'edit' view, including 'object'.
        $args = $this->getEditArguments($id);
        return view($this->view_folder . '.edit', $args);
    }

    /**
     * Get or fetch a specific object, by its identifier.
     * This is safe to be called multiple times.
     * @param wild $id This can be zero to create an element.
     * @return wild
     */
    public final function getObject($id)
    {
        if (!isset($this->objects[$id])) {
            $this->objects[$id] = $this->findObjectById($id);
        }
        return $this->objects[$id];
    }

    /**
     * Fetch a new fresh object, on the fly, by ID.
     * @param wild $id Identifier of the object, or 0 for a new one.
     * @return wild
     */
    protected function findObjectById($id) {
        // Allow to easily create an element.
        if ($id == 0) {
            return $this->createObject(request());
        }
        return $this->query(request())->findOrFail($id);
    }

    /**
     * @return array
     */
    protected function getEditArguments($id)
    {
        $data = [
            'object' => $this->getObject($id),
            'site_title' => $this->getSingleObjectPageTitle($id),
            'breadcrumbs' => $this->getEditBreadcrumbs($id),
        ];
        return $data;
    }

    /**
     * Open the "edit" view, but without actually submitting edits.
     * @param type $id Entity ID, or zero to create something.
     * @return type View
     */
    public function edit($id)
    {
        if ($id == 0) {
            return $this->create(request());
        }
        $edit_args = $this->getEditArguments($id);
        $this->authorize('update', $edit_args['object']);
        return view($this->view_folder . '.edit', $edit_args);
    }

    /**
     * Open the "edit" view, submitting edits.
     * @param Request $request
     * @param type $id Entity ID, or zero to create something.
     * @return type View
     */
    public function update(Request $request, $id)
    {
        $object = $this->getObject($id);
        $this->authorize('update', $object);

        // TODO: start transaction.

        // First, validate the request.
        $validations = $this->defaultValidations($object);
        $request->validate($validations);

        // Then, persist the object.
        $this->requestToObject($request, $object);
        $object->save();

        // As last step, persist other external things.
        $this->afterSaving($request, $object);

        // TODO: commit the transaction.

        return $this->getRouteAfterSave($object);
    }

    /**
     * Get the route after the store() method.
     */
    protected function getRouteAfterSave($object)
    {
        return redirect()->route($this->view_folder . '.edit', $object->id);
    }

    public function destroy($id)
    {
        $object = $this->getObject($id);
        $this->authorize('delete', $object);
        $object->delete();
        return redirect()->route($this->view_folder . '.index');
    }

    protected function fitObject($object, $fields, $request)
    {
        foreach($fields as $f) {
            if ($request->has($f)) {
                $object->$f = $request->input($f);
            }
        }

        return $object;
    }

    protected function defaultSortingColumn()
    {
        return 'name';
    }

    protected function defaultSortingDirection()
    {
        return 'asc';
    }

    /**
     * Get the sorting direction.
     * @param Request $request
     * @return string
     */
    public function getOrderDirection(Request $request): string
    {
        return $request->input('order_dir', $this->defaultSortingDirection());
    }

    /**
     * Get all the available order directions.
     * @return array
     */
    public function getAvailablesOrderDirections(): array
    {
        return [
            'asc' => 'Crescente',
            'desc' => 'Decrescente',
        ];
    }

    /**
     * Create a Query object, without order.
     */
    protected function query(Request $request)
    {
        return ($this->classname)::query();
    }

    /**
     * Create a Query object, with default order.
     */
    protected function queryList(Request $request)
    {
        return $this->query($request)->orderBy($this->defaultSortingColumn(), $this->getOrderDirection($request));
    }

    /**
     * In this hook, your entity has the ID.
     * @param type $request
     * @param type $object
     * @return type
     */
    protected function afterSaving($request, $object)
    {
        return;
    }

    protected abstract function defaultValidations($object);

    /**
     * In this hook, you should put your request into the object.
     * DO **NOT** SAVE() INSIDE THIS METHOD PLEASE.
     * Inside this method, your object may not have an ID.
     * If you need an ID, check afterSaving() instead.
     */
    protected abstract function requestToObject(Request $request, $object): void;

    /**
     * Get the breadcrumbs for the index page.
     * @return array
     */
    protected function getIndexBreadcrumbs()
    {
        // It's probably useful to see the Home.
        $breadcrumbs = [];
        $breadcrumbs[] = [
            'label' => 'Home',
            'url' => route('home'),
        ];

        // Whenever you are editing one element, or looking at their list.
        // But don't show that, if you can see that, so you don't see a 403
        // by clicking on the breadcrumb entry.
        $can_list = $this->getAuthUserOrGuest()->can('index', $this->classname);
        $breadcrumbs[] = [
            'label' => $this->getIndexPageTitle(),
            'url' => route($this->view_folder . '.index'),
            'disabled' => !$can_list,
        ];

        return $breadcrumbs;
    }

    /**
     * Get the breadcrumbs for the edit page.
     * @return array
     */
    protected function getEditBreadcrumbs($id)
    {
        // Just something. Please override me.
        $breadcrumbs = $this->getIndexBreadcrumbs();
        $breadcrumbs[] = [
            'label' => $this->getSingleObjectPageTitle($id),
            'url' => request()->url(),
        ];
        return $breadcrumbs;
    }

    /**
     * Get the Auth user, or a fake user.
     * This method is basically handy to call ->can
     * @return Always return the current User, or a dummy User.
     */
    private function getAuthUserOrGuest(): User
    {
       // Just return the normal user.
       if (Auth::check()) {
           return Auth::user();
       }

       // Anonymous! Return something.
       return new User;
    }

}

