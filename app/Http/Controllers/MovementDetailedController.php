<?php
/**
 * ILS Manager
 * Copyright (C) 2021-2025 Italian Linux Society, contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Services\FinancialSummaryService;
use App\AccountRow;
use App\Bank;
use App\Fee;
use App\Movement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use stdClass;

class MovementDetailedController extends EditController
{
    protected $classname = Movement::class;
    protected $view_folder = 'movement-detailed';

    const POSITIVE = 'in';
    const NEGATIVE = 'out';

    public function exportAllCsv(Request $request)
    {
        // Require administrators.
        $this->checkAuth();

        // Create Movement DTOs.
        // This contain all the information ready for export.
        $args = $this->getIndexArguments($request);
        $movements_dtos = $args['movements_dto'];

        // Create a temporary file.
        $tmp_file_path = tempnam(sys_get_temp_dir(), 'tempmovementdetailedcsv_');
        $tmp_file_pointer = fopen($tmp_file_path, 'w');

        // Generate CSV file structure with header.
        fputcsv($tmp_file_pointer, $this->createHeaderExportData($args));
        foreach ($movements_dtos as $movement_dto) {
            fputcsv($tmp_file_pointer, $this->entryDtoToExportData($movement_dto, $args));
        }
        fclose($tmp_file_pointer);

        // Download the CSV file and nuke the temporary file.
        return Response::download($tmp_file_path, 'movements-export-all.csv', [
            'ResponseContentType' => 'text/csv',
        ])
            ->deleteFileAfterSend(true);
    }

    /**
     * Get the page elements limit.
     *
     * @return int
     */
    protected function getPageElementsDefaultLimit()
    {
        return 1000;
    }

    protected function getIndexRequestArgs(Request $request): array
    {
        $args = parent::getIndexRequestArgs($request);

        $args['bank_id'] = $request->input('bank_id');

        $args['selected_account_ids'] = $request->input('account', []);

        $args['amount_sign'] = $request->input('amount_sign', '');

        // The default value of the "with_start_of_year" checkbox is true
        // on the default view, but we must preserve it's false value when
        // the user has submitted something but leaving this checkbox unchecked.
        $is_default_view = count($request->all()) < 2;
        $true_as_default = $is_default_view ? true : false;
        $args['with_start_of_year'] = $request->input('with_start_of_year', $true_as_default);

        // Whenever we should include AccountRow(s) or not in the results set.
        // Disabling this may be useful to have an export with only Movements,
        // to easily compare them with the ones in your bank.
        $args['with_account_rows'] =
          (bool)$request->input('with_account_rows', $true_as_default);

        // If we are going to show a subset of Account(s), skip AccountRow(s) validation,
        // since their sum will never match the Movement.amount.
        $args['is_filter_on_account_rows'] =
          $args['selected_account_ids'] ||
          $args['amount_sign'] ||
          ! $args['with_account_rows'];

        // Add a shortcut, easy to understand.
        $args['validate_all_account_rows'] =
          ! $args['is_filter_on_account_rows'];

        // Pick the current year as default one.
        // Allow to unset.
        $args['year'] = $request->input('year');
        if ($args['year'] === '-') {
            $args['year'] = null;
        } else {
            $args['year'] = (int) $args['year'];
            if ($args['year'] <= 0) {
                $args['year'] = date('Y');
            }
        }

        return $args;
    }

    protected function query(Request $request)
    {
        $movements_query = parent::query($request);

        $request_args = $this->getIndexRequestArgs($request);

        $bank_id = $request_args['bank_id'];
        if ($bank_id) {
            $movements_query->where('bank_id', $bank_id);
        }

        // Allow to exclude the balance at the start of this year, or not.
        if (! $request_args['with_start_of_year']) {
            $movements_query->whereNull('start_of_year');
        }

        // Create a query to be used twice, abount AccountRow(s)' conditions.
        $filter_account_rows = function ($query) use ($request_args) {
            // Filter AccountRow(s) by Account(s).
            if ($request_args['selected_account_ids']) {
                $query->whereIn('account_id', $request_args['selected_account_ids']);
            }

            // Filter AccountRow(s) by sign.
            $amount_sign = $request_args['amount_sign'];
            if ($amount_sign === self::POSITIVE) {
                $query->positive();
            } elseif ($amount_sign === self::NEGATIVE) {
                $query->negative();
            }
        };

        // Discard the Movements not matching the filters on AccountRow(s)' details.
        // Note that if we are NOT filtering by these details we MUST NOT
        // apply the 'whereHas', since we are interested in seeing also Movements
        // that have not any AccountRow(s).
        if ($request_args['with_account_rows'] && $request_args['is_filter_on_account_rows']) {
            $movements_query->whereHas('account_rows', $filter_account_rows);
        }

        // Filter by amount sign.
        // We should be very inclusive at this point, showing:
        //   - movements with that sign
        //   - movements with an least one AccountRow with that sign
        $amount_sign = $request_args['amount_sign'];
        if ($amount_sign) {
            if ($amount_sign === self::POSITIVE) {
                $movements_query->where(function($q) use ($request_args) {
                    $q->where('amount', '>=', 0);
                    if ($request_args['with_account_rows']) {
                        $q->orWhereHas('account_rows', function($q_ar) {
                            $q_ar->positive();
                        });
                    }
                });
            } elseif ($amount_sign === self::NEGATIVE) {
                $movements_query->where(function($q) use ($request_args) {
                    $q->where('amount', '<=', 0);
                    if ($request_args['with_account_rows']) {
                        $q->orWhereHas('account_rows', function($q_ar) {
                            $q_ar->negative();
                        });
                    }
                });
            }
        }

        // Also include AccountRow(s) in the results set, or not.
        if ($request_args['with_account_rows']) {
            // The with() is quite efficient and avoids N+1 performance issues.
            // But only include the ones matching the filters.
            // This works whenever we have these filters, or not.
            $movements_query->with('account_rows', $filter_account_rows);
            // TODO: Activate this performance improvement, after the filter by Account=Bank works again.
//            $movements_query->with([
//              'account_rows.fee',
//              'account_rows.account',
//              'account_rows.movement', // TODO: Improve Laravel framework to manage the parent entity without queries.
//                                       // https://github.com/laravel/framework/discussions/49087
//              'account_rows.section',
//           ]);
        }

        // Fix N+1 performance issue.
        // P.S. these sub-entities are always shown, with or without AccountRow(s).
        $movements_query->with([
              'bank',
        ]);

        // As default, filter by current year.
        // Allow to filter by all years, setting '-'.
        $year = $request_args['year'];
        if ($year) {
            $movements_query->whereYear('date', $year);
        }

        return $movements_query;
    }

    protected function getIndexArguments(Request $request)
    {
        $args = parent::getIndexArguments($request);

        $banks = Bank::all();

        $query = $args['query'];
        $movements = $args['objects'];

        $account_rows_amount_sum = '0.00';
        $movements_amount_sum = '0.00';

        // Create some Data Transfer Objects to simplify the construction of with Movement and (eventually) its AccountRow.
        $movements_dto = [];
        $is_first_ar = true;
        foreach ($movements as $movement) {
            $is_first_ar = true;
            $movements_amount_sum = bcadd($movements_amount_sum, $movement->amount, 2);

            // If we are going to fetch no AccountRow(s), don't validate them.
            // This is needed to avoid huge 'N+1 performance issues' in the Movement.validate()
            // method, that may cause extreme performance loss.
            // Thanks to this, in this corner case we pass from ~1019 nonsense queries to ~29 queries.
            if (! $args['with_account_rows']) {
                $movement->dontValidateEachAccountRow();
            }

            // If we are going to show a subset of Account(s), skip AccountRow(s) validation,
            // since their sum will never match the Movement.amount.
            // Thanks to this, we don't show nonsense confusing warnings on every Movement
            // when at least one of their AccountRow was successfully filtered out by your filters.
            if (! $args['validate_all_account_rows']) {
                $movement->dontValidateAllAccountRows();
            }

            if ($args['with_account_rows'] && ! $movement->account_rows->isEmpty()) {
                foreach ($movement->account_rows as $account_row) {
                    $movements_dto[] = [
                        'movement' => $movement,
                        'account_row' => $account_row,
                        'account_row_first' => $is_first_ar,
                        'fee' => $account_row->fee ?? null,
                    ];
                    $is_first_ar = false;
                    $account_rows_amount_sum = bcadd($account_rows_amount_sum, $account_row->amount, 2);
                }
            } else {
                $movements_dto[] = [
                    'movement' => $movement,
                    'account_row' => null,
                    'account_row_first' => $is_first_ar,
                    'fee' => null,
                ];
                $is_first_ar = false;
            }
        }

        // Expose all the available filter options like "Incomes", "Outcomes", etc.
        $args['available_amount_signs'] =
          $this->getAvailableAmountSigns();

        // Footer with sums.
        $financial_summary_args = [
            'year' => $args['year'],
            'selectedBankId' => $args['bank_id'],
            'selectedAccountIds' => $args['selected_account_ids'],
            'includeStartOfYear' => $args['with_start_of_year'],
        ];
        $args['financial_summary'] = new FinancialSummaryService($financial_summary_args);

        $args = array_replace($args, [
            'movements_dto' => $movements_dto,
            'movements' => $movements,
            'banks' => $banks,
            'movements_amount_sum' => $movements_amount_sum,
            'account_rows_amount_sum' => $account_rows_amount_sum,
        ]);

        return $args;
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function defaultValidations($object)
    {
        // TODO: Implement defaultValidations() method.
    }

    protected function requestToObject($request, $object): void
    {
        // TODO: Implement requestToObject() method.
    }

    private function getAvailableAmountSigns(): array
    {
        return [
            '' => __("Entrate/Uscite"),
            self::POSITIVE => __("Solo Entrate"),
            self::NEGATIVE => __("Solo Uscite"),
        ];
    }

    private function entryDtoToExportData(array $dto_line, array $args): array
    {
        $data = [];
        $data[] = $dto_line['movement']->id;
        $data[] = $dto_line['movement']->bank->id;
        $data[] = $dto_line['movement']->bank->name;
        $data[] = $dto_line['movement']->date;
        $data[] = $dto_line['movement']->identifier;
        $data[] = $dto_line['movement']->amount;
        $data[] = $dto_line['movement']->amount_original;
        $data[] = $dto_line['movement']->reviewed;
        $data[] = $dto_line['movement']->notes;

        // If the user does not want any AccountRow(s),
        // export a cleaner CSV file without confusing empty columns.
        if ($args['with_account_rows']) {
            // Note that the AccountRow(s) are optional.
            // This is expecially true for fresh Movement(s) under review.
            $data[] = $dto_line['account_row']->id ?? null;
            $data[] = $dto_line['account_row']->account->id ?? null;
            $data[] = $dto_line['account_row']->account->name ?? null;
            $data[] = $dto_line['account_row']->account->printableName ?? null;
            $data[] = $dto_line['account_row']->amount ?? null;
            $data[] = $dto_line['account_row']->notes ?? null;
        }

        return $data;
    }

    private function createHeaderExportData(array $args): array
    {
        $movement = new stdClass;
        $movement->bank = new stdClass;
        $account_row = new stdClass;
        $account_row->account = new stdClass;
        $movement->bank->id = 'Movement.Bank.ID';
        $movement->bank->name = 'Movement.Bank.Name';
        $movement->id = 'Movement.ID';
        $movement->date = 'Movement.Date';
        $movement->notes = 'Movement.Notes';
        $movement->identifier = 'Movement.Identifier';
        $movement->amount = 'Movement.Amount';
        $movement->amount_original = 'Movement.AmountOriginal';
        $movement->reviewed = 'Movement.Reviewed';
        $account_row->id = 'AccountRow.ID';
        $account_row->account->id = 'AccountRow.Account.ID';
        $account_row->account->name = 'AccountRow.Account.Name';
        $account_row->account->printableName = 'AccountRow.Account.CompleteName';
        $account_row->amount = 'AccountRow.Amount';
        $account_row->notes = 'AccountRow.Notes';

        return $this->entryDtoToExportData([
            'movement' => $movement,
            'account_row' => $account_row,
        ], $args);
    }
}
