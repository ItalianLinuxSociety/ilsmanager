<?php
/**
    ILS Manager
    Copyright (C) 2021-2024 Italian Linux Society, contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use PayPal\PayPalAPI\TransactionSearchReq;
use PayPal\PayPalAPI\TransactionSearchRequestType;
use PayPal\PayPalAPI\GetTransactionDetailsReq;
use PayPal\PayPalAPI\GetTransactionDetailsRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

/**
 * PayPal API movements reader
 *
 * Originally from:
 *   https://github.com/ItalianLinuxSociety/Donazioni/blob/master/read_paypal.php
 *
 * @author Valerio Bozzolan, Roberto Guido
 */
class PayPalAPIMovementsReader
{
    /**
     * Process incoming PayPal transactions.
     *
     * @param array $args Array of arguments. All of them:
     *   startDate:     DateTime When to start your search (default: endDate - 6 months)
     *   endDate:       DateTime When to stop your search (default: today)
     *   transactionId: PayPal transaction ID
     *   itemCallback:  callable function
     *     It will be called for each Movement with these arguments:
     *       itemCallback( $answer_data, $answer_internal_data )
     *     Where the $answer_data is an array with:
     *       id                         string   The Movement identified. Example: 'ASD12312313JHJAJKDJAS'
     *       date                       string   The Movement date - format "Y-m-d\TH:i:s\Z"
     *       amount_gross               string   Example: "25.00"
     *       amount_net                 string   Example: "23.80"
     *       amount_fee                 string   Example: "1.20"
     *       notes                      string   Example: 'BOB TI AMO PRENDI I MIEI SOLDI PER GHANOO PLUS LEENOCS'
     *       notes_xl                   string   Example: 'pippo - pippo@pippi.it - BOB TI AMO PRENDI I MIEI SOLDI PER GHANOO PLUS LEENOCS'
     *     Where the $answer_internal_data is:
     *       paypal_transaction         object   Original PayPal transaction
     *       paypal_transaction_details object   Original PayPal transaction details
     *
     *     Note that if you return FALSE the loop is interrupted, otherwise
     *     you will fire the callback on all remaining movements.
     * @author Valerio Bozzolan
     */
    public static function process($args = [])
    {
        // Declare raw filters.
        $raw_date_start = null; // string|null
        $raw_date_end   = null; // string|null

        // Accept DateTime inputs.
        $date_start     = $args['startDate']     ?? null; // DateTime|null
        $date_end       = $args['endDate']       ?? null; // DateTime|null
        $transaction_id = $args['transactionId'] ?? null; // string|null

        // We do NOT have a Transaction ID.
        // In this case we risk to return a lot of results.
        // Propose decent default dates.

        // As default, start from some days before today.
        if (!$date_start) {
            $date_start = new DateTime();
            $date_start->sub(new DateInterval('P3D'));
            $date_start->setTime(0, 0);
        }

        // As default, add very few days to the start date.
        // This is quite small to do not trigger PayPal result limits.
        if (!$date_end) {
            $date_end = clone $date_start;
            $date_end->add(new DateInterval('P1M'));
            $date_end->setTime(23, 59);
        }

        // PayPal wants this format for dates
        $raw_date_start = $date_start->format('Y-m-d\TH:i:s\Z');
        $raw_date_end   = $date_end  ->format('Y-m-d\TH:i:s\Z');

        if (!isset($args['itemCallback'])) {
            abort(500, "missing itemCallback argument");
        }

        $LOG_PREFIX = sprintf("PayPal processing period %s - %s",
          $raw_date_start,
          $raw_date_end);

        if ($transaction_id) {
            $LOG_PREFIX = sprintf("%s - for transaction ID %s",
              $LOG_PREFIX,
              $transaction_id);
        }

        Log::debug(sprintf("%s - start",
            $LOG_PREFIX));

        // Define PayPAL API request.
        // Some of these may be NULL and that's OK.
        $transactionSearchRequest = new TransactionSearchRequestType();
        $transactionSearchRequest->TransactionID = $transaction_id;
        $transactionSearchRequest->StartDate = $raw_date_start;
        $transactionSearchRequest->EndDate   = $raw_date_end;

        $tranSearchReq = new TransactionSearchReq();
        $tranSearchReq->TransactionSearchRequest = $transactionSearchRequest;

        $paypalService = new PayPalAPIInterfaceServiceService(self::configCredentialsForService());

        $transactionSearchResponse = $paypalService->TransactionSearch($tranSearchReq);
        $errors = $transactionSearchResponse->Errors ?? null;
        if (!$transactionSearchResponse || $transactionSearchResponse->Ack !== 'Success' || isset($errors)) {
            throw new Exception(sprintf("Unexpected response from PayPal. ACK: %s. Errors: %s. Date start: %s. Date end: %s.",
              $transactionSearchResponse->Ack,
              json_encode($errors),
              $raw_date_start,
              $raw_date_end));
        }
        $payment_transactions = $transactionSearchResponse->PaymentTransactions ?? [];
        foreach ($payment_transactions as $trans) {
            $result = self::processSinglePayPalTransactionExpandingDetails($paypalService, $trans, $args['itemCallback']);
            if ($result === false) {
                break;
            }
        }

        Log::debug(sprintf("%s - concluded receiving %d transactions",
          $LOG_PREFIX,
          count($payment_transactions)));
    }

    public static function processSinglePayPalTransactionExpandingDetails($paypalService, $trans, $callback)
    {
        // Some Transactions are "nonsense temporary bank stuff".
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
        $nonsense_transaction_types = [
            'Temporary Hold',
            'Authorization',
        ];

        // Some Transactions are "nonsense temporary bank stuff".
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
        if (isset($trans->Type) && in_array($trans->Type, $nonsense_transaction_types, true)) {
            Log::warning("Skipping nonsense PayPal type '{$trans->Type}'. status: '{$trans->Status}' TransactionID: {$trans->TransactionID}");
            return null;
        }

        // Skip nonsense Movements with zero amounts.
        $gross_amount_cents = self::toCents($trans->GrossAmount->value);
        $fee_amount_cents   = self::toCents($trans->FeeAmount->value);
        $net_amount_cents   = self::toCents($trans->NetAmount->value);
        if (!$gross_amount_cents && !$fee_amount_cents && !$net_amount_cents) {
            Log::warning("Skipping nonsense PayPal type with no amounts. TransactionID: {$trans->TransactionID}");
            return null;
        }

        $req = new GetTransactionDetailsRequestType();
        $req->TransactionID = $trans->TransactionID;
        $tranreq = new GetTransactionDetailsReq();
        $tranreq->GetTransactionDetailsRequest = $req;
        $details = $paypalService->GetTransactionDetails($tranreq);
        return self::processSinglePayPalTransactionRaw($trans, $details, $callback);
    }

    public static function processSinglePayPalTransactionRaw($trans, $details, $callback) {

        // TODO: read from configuration.
        $PAYPAL_SELF_EMAIL = 'direttore@linux.it';

        $payment_item_info = $details->PaymentTransactionDetails->PaymentItemInfo;
        $item = $payment_item_info->PaymentItem; // Array of purchased items

        $notes = null;
        if (is_array($item) && $item) {
            //$first_item = array_pop($item);
            $notes =
               $payment_item_info->Memo
            ?? $payment_item_info->Custom // Stickermule contains: 'Order number: R8LULZLULZ'. In Hetzner, this contains the invoice ID.
            ?? $payment_item_info->InvoiceID // Stickermule contains: 'R8LULZLULZ'. In Hetzner, this contains something that is NOT the invoice ID.
            ?? $trans->Type // Fallback for internal transfers with empty Memo. Just to show something.
            ?? null;
        }

        $date = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $trans->Timestamp ?? $details->Timestamp);

        // Check if this is a payment from a recognizable person.
        $payer = $details->PaymentTransactionDetails->PayerInfo ?? null;
        $payer_email = $payer->Payer ?? null;
        $payer_whatever_name = $payer->PayerName->FirstName ?? $payer->PayerName->LastName ?? $payer->Payer ?? null;
        if (isset($payer_whatever_name) && $payer_email !== $PAYPAL_SELF_EMAIL) {
            $notes_xl = sprintf('%s %s - %s - %s', $payer->PayerName->FirstName, $payer->PayerName->LastName, $payer->Payer, $notes);
        } else {
            $receiver = $details->PaymentTransactionDetails->ReceiverInfo->Receiver ?? null; // For Stickermule this is 'italy@printbear.net'
            $display_name = $trans->PayerDisplayName ?? $receiver; // For stickermule this is 'Sticker Mule Italy srl.
            if ($receiver) {
                $notes_xl = sprintf('%s - %s - %s', $display_name, $receiver, $notes);
            } else {
                $notes_xl = $notes;
            }
        }

        // The Fee in this field is usually negative.
        // Note that the Fee in the details, instead, is always absolute.
        // Since 2024-07-28 we adopted the negative value (instead of absolute). This is a desired breaking change.
        $amount_fee   = $trans->FeeAmount  ->value ?? throw new Exception("Cannot parse FeeAmount from Transaction {$trans->TransactionID}");
        $amount_gross = $trans->GrossAmount->value ?? throw new Exception("Cannot parse GrossAmount from Transaction {$trans->TransactionID}");
        $amount_net   = $trans->NetAmount  ->value ?? throw new Exception("Cannot parse NetAmount from Transaction {$trans->TransactionID}");
        $amount_in_original_currency = null; // Populated later.

        // Current currency.
        $currency_local = 'EUR'; // TODO: Add global configuration.

        // PayPal transaction currency (original one).
        // For example, this for Stickermule is always 'USD'.
        $currency_original = self::extractTransactionAmountsCurrency($trans);

        // Check whenever we had currency conversion.
        $currency_conversion = $currency_original !== $currency_local;
        if ($currency_conversion) {
            // This transaction was originated from a different currency.
            // Example Stickermule transaction:
            // amount_gross:   -50.0 USD
            // amount_net:     -50.0 USD
            // amount_fee:      0.00 USD
            // SettleAmount :  45.02 EUR (note: it's positive... lol)
            $amount_settle_wrongsign = $details->PaymentTransactionDetails->PaymentInfo->SettleAmount->value ?? null;
            if ($amount_settle_wrongsign === null) {
                // I have no idea what we should do if PayPal has not converted this to our desired currency.
                // Better to don't import this at the moment...
                Log::warning("Skipping PayPal transaction with currency conversion but without PaymentTransactionDetails->PaymentInfo->SettleAmount. Status: '{$trans->Status}'. TransactionID: {$trans->TransactionID}");
                return null;
            }

            // For some reasons when the 'amount_gross' is -50.0 USD,
            // the amount of the 'amount_settle' is positive. Therefore...
            // let's toggle the sign...
            $amount_settle = self::toggleAmountSign($amount_settle_wrongsign);

            // The expected currency is EUR. So, if the currency after conversion is not EUR,
            // this should be deeply inspected by a developer.
            $currency_after_conversion = $details->PaymentTransactionDetails->PaymentInfo->SettleAmount->currencyID;
            if ($currency_after_conversion !== $currency_local) {
                throw new Exception("The PayPal transaction {$trans->TransactionID} has PaymentInfo->SettleAmount->currencyID={$currency_after_conversion} instead of {$currency_local} - unexpected currency conversion.");
            }

            // I don't know what we should do if there are fees under a currency conversion.
            // Maybe better to scream violently to attract a developer.
            if ($amount_fee !== '0.00') {
                throw new Exception("The PayPal transaction {$trans->TransactionID} has bank fees {$amount_fee} {$currency_original} but shuold be affected by currency conversion from {$currency_original} to {$currency_local}. What we should do with these fees?");
            }

            // Replace the amounts from the original currency (e.g. USD),
            // to our desired local currency (e.g. EUR).
            // Expose the original amount.
            $amount_in_original_currency = $amount_gross;
            $amount_gross = $amount_settle;
            $amount_net = $amount_settle;
        }

        $answer_data = [
            'id'           => $trans->TransactionID,
            'date'         => $date->format('Y-m-d\TH:i:s\Z'),
            'amount_gross' => $amount_gross,
            'amount_net'   => $amount_net,
            'amount_fee'   => $amount_fee,
            'currency_conversion' => $currency_conversion,
            'currency_original' => $currency_original,
            'amount_in_original_currency' => $amount_in_original_currency,
            'notes'        => $notes,
            'notes_xl'     => $notes_xl,
        ];
        $answer_internal_data = [
            'paypal_transaction'         => $trans,
            'paypal_transaction_details' => $details,
        ];
        $result = call_user_func($callback, $answer_data, $answer_internal_data);
        if ($result === false) {
            return false;
        }
    }

    /**
     * Extract the currency of the input PayPal transaction's amounts.
     * @param object $trans PayPal transaction, got from PayPal SOAP API.
     * @return string PayPal currency ID.
     * @throws Exception
     */
    private static function extractTransactionAmountsCurrency($trans): string
    {
        // Find all the currencies. Keys are currencies.
        $types_by_currency = [];
        $amount_types = [
            'Gross', // GrossAmount
            'Fee', // FeeAmount
            'Net', // NetAmount
        ];
        foreach ($amount_types as $amount_type) {
            $attribute = "{$amount_type}Amount";
            if (isset($trans->{$attribute}->currencyID)) {
                $currency = $trans->{$attribute}->currencyID;
                $types_by_currency[$currency] = $amount_type;
            }
        }

        // If we have Gross in USD, and Net in EUR, well, that's something new
        // that deserves a little more exploration.
        if (count($types_by_currency) > 1) {
            throw new Exception(
                "Unexpected situation. This PayPal transaction has different currencies (!):\n %s",
                json_encode($trans));
        }

        // Just return the first one.
        foreach ($types_by_currency as $currency => $type) {
            return $currency;
        }

        // No currency, no party.
        throw new Exception("Cannot extract the currency from this PayPal transaction: ", json_encode($transaction));
    }


    /**
     * Get an array of credentials for PayPalAPIInterfaceServiceService
     *
     * @return array
     */
    private static function configCredentialsForService()
    {
        // no credentials no party
        $config = self::validatedConfig();

        // initialize config from given credentials
        $paypal_config = [
               'mode'            => 'live',
               'acct1.UserName'  => $config['username'],
               'acct1.Password'  => $config['password'],
               'acct1.Signature' => $config['signature'],
        ];

        return $paypal_config;
    }

    /**
     * Check if you have a valid configuration
     *
     * Do not care about whatever issue about it
     *
     * @return boolean
     */
    public static function hasValidConfig()
    {
        $ok = false;

        try {

            // this can throw an exception
            self::validatedConfig();

            // yuppie!
            $ok = true;
        } catch (\Exception $e) {

            // oh now :(
        }

        return $ok;
    }

    /**
     * Get the whole validated PayPal service configuration
     *
     * Note that this throws exceptions if something is wrong.
     *
     * @return array
     */
    public static function validatedConfig()
    {

        // no PayPal info no party
        $info = config('services.paypal');
        if (!$info) {
            throw new \Exception("missing PayPal configuration - check your ENV file");
        }

        // mandatory fields with the related ENV file (check config/services.php)
        $mandatory_fields_and_env = [

            // useful to login in PayPal APIs
            'username'  => 'PAYPAL_USERNAME',
            'password'  => 'PAYPAL_PASSWORD',
            'signature' => 'PAYPAL_SIGNATURE',

            // useful to keep a relation to the Bank ID in the database
            'bankid'    => 'PAYPAL_BANKID',
        ];

        // check each mandatory field
        foreach ($mandatory_fields_and_env as $field => $env) {

            // no field no party
            if (!isset($info[ $field ])) {

                // be very verbose because I can be totally drunk when configuring this stuff
                throw new \Exception(sprintf(
                    "missing PayPal configuration field '%s'. Check %s in your .env file and read the fantastic manual",
                    $field,
                    $env
                ));
            }
        }

        return $info;
    }

    private static function toCents(string $v): int
    {
        $v = str_replace('.', '', $v);
        return (int) $v;
    }

    /**
     * Toggle the sign of an amount expressed as PayPal amount string.
     * So, if the input amount is negative, it will become positive;
     * if the input amount is positive, it will become negative.
     * @param string $v Input amount value string, from PayPal API
     * @return string Output amount value, with the sign toggled
     */
    private static function toggleAmountSign(string $v): string
    {
        $sign = '-';
        $is_negative = strpos($v, $sign) === 0;
        if ($is_negative) {
            // Make positive.
            $v = str_replace($sign, '', $v);
        } else {
            // Make negative.
            $v = $sign . $v;
        }
        return $v;
    }
}
