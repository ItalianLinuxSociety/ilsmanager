<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * An AssemblyUser is a specific User that is participating in one Assembly.
 * The user can also eventually delegate their vote to somebody else.
 */
class AssemblyUser extends Model
{

    /**
     * The table associated with the model.
     *
     * TODO: rename the table to 'assembly_users' that is the canonical generated name...
     *
     * @var string
     */
    protected $table = 'assembly_user';

    /**
     * Indicates if the model should be timestamped.
     * @TODO: Create migration script that adds the timestamps and enable this constant pls plz.
     *        https://laravel.com/docs/11.x/migrations#column-method-timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    public function assembly()
    {
        return $this->belongsTo(Assembly::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delegate()
    {
        return $this->belongsTo(User::class, 'delegate_id');
    }

    /**
     * Scope a query to only include delegations.
     */
    public function scopeWhereUser(Builder $query, User $user): void
    {
        $query->where('user_id', $user->id);
    }

    /**
     * Scope a query to only include delegations.
     */
    public function scopeWhereUserIsMe(Builder $query): void
    {
        $query->whereUser(Auth::user());
    }

    /**
     * Scope a query to only include this delegated user.
     */
    public function scopeWhereDelegateUser(Builder $query, User $delegate_user): void
    {
        $query->whereDelegateUserID($delegate_user->id);
    }

    /**
     * Scope a query to only include myself as delegate user.
     * So, only getting rows where I can vote for somebody else.
     */
    public function scopeWhereDelegateUserIsMe(Builder $query): void
    {
        $query->whereDelegateUser(Auth::user());
    }

    /**
     * Scope a query to only include this delegated user's ID.
     */
    public function scopeWhereDelegateUserID(Builder $query, int $delegate_user_id): void
    {
        $query->where('delegate_id', $delegate_user_id);
    }

    /**
     * Scope a query to only include delegations.
     */
    public function scopeOnlyDirectParticipant(Builder $query): void
    {
        // TODO: make 'delegate_id' nullable and never zero...
        //$query->whereNull('delegate_id');
        $query->where('delegate_id', 0);
    }

    /**
     * Scope a query to only include delegations.
     */
    public function scopeOnlyDelegate(Builder $query): void
    {
        // TODO: make 'delegate_id' nullable and never zero...
        //$query->whereNotNull('delegate_id');
        $query->where('delegate_id', '!=', 0);
    }

    /**
     * Scope a query to order by the users' printable name.
     */
    public function scopeOrderByUserPrintableName(Builder $query): void
    {
        // TODO: make it works :D
    }
}
