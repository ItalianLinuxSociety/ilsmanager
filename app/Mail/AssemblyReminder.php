<?php

namespace App\Mail;

use App\User;
use App\Assembly;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class AssemblyReminder extends Mailable
{
    use Queueable, SerializesModels;

    public string $assemblyTitle;
    public string $assemblyDate;
    public string $assemblyText;
    public string $userDisplayName;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Assembly $assembly)
    {
        // We inject the User and the Assembly so the contents are managed here, not from the caller.
        // We maintain minimal attributes in this class, so there is less serialized stuff to be stored in the queue.
        $this->assemblyTitle = $assembly->title; // This is something like "Assembly YYYY-MM-DD".
        $this->assemblyDate = $assembly->date->format('Y-m-d');
        $this->assemblyText = $assembly->announceRendered; // This is already Markdown.
        $this->userDisplayName = $user->printableName;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: $this->assemblyTitle
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'email/assembly_reminder',
            with: [
                'assemblyTitle' => $this->assemblyTitle,
                'assemblyText' => $this->assemblyText,
                'assemblyDate' => $this->assemblyDate,
                'userDisplayName' => $this->userDisplayName,
                'assemblyLink' => route('assembly.index'),
            ],
        );
    }
}
