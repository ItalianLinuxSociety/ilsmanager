<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class PendingsWithoutFeeAdmins extends Mailable
{
    use Queueable, SerializesModels;

    private array $users;
    private int $registered_since_more_than_days;

    /**
     * Create a new message instance.
     */
    public function __construct(array $users, int $registered_since_more_than_days)
    {
        $this->users = $users;
        $this->registered_since_more_than_days = $registered_since_more_than_days;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: sprintf(
                'Soci in attesa senza quota (da più di %d giorni)',
                $this->registered_since_more_than_days
            )
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'email.pendings_without_fee_admins',
            with: [
                'users' => $this->users,
                'registered_since_more_than_days' => $this->registered_since_more_than_days,
            ],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
