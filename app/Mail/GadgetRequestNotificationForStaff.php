<?php

namespace App\Mail;

use App\GadgetRequest;
use App\GadgetRequestStatus;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GadgetRequestNotificationForStaff extends Mailable
{
    use Queueable, SerializesModels;

    private $gadgetRequest;

    public function __construct(GadgetRequest $gadgetRequest)
    {
        $this->gadgetRequest = $gadgetRequest;
    }

    public function build()
    {
        // Get the number of gadgets that are ready to be processed.
        // This adds a nice extra pressure to the shipper. lol
        $active_status_ids = GadgetRequestStatus::getActiveStatusIDs();
        $count = GadgetRequest::whereIn('status_id', $active_status_ids)->count();

        return $this->subject('Nuova richiesta di spedizione gadget')
            ->view('email.gadget_request_notification_for_staff', [
                'gadgetRequest' => $this->gadgetRequest,
                'count' => $count,
            ]);
    }
}