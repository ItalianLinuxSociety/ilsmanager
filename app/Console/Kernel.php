<?php

namespace App\Console;

use App\Config;
use App\Mail\RenewalFreePreNotifier;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
            Il 10 gennaio viene effettuato il primo controllo, che innesca le
            mail di notifica.
            Il 10 febbraio gli utenti non ancora in regola coi pagamenti vengono
            sospesi.
            Il 10 marzo gli utenti sospesi sono espulsi.
        */
        $schedule->command('check:fees')->cron('0 10 10 1 *');
        $schedule->command('check:suspend')->cron('0 10 10 2 *');
        $schedule->command('check:expelled')->cron('0 10 10 3 *');

        $schedule->command('check:requests')->daily();

        // Notifiche ANNUALI ai soci che ERANO in regola l'anno scorso.
        // Il 10 di febbraio mattina, tutti i soci che ERANO in regola ricevono un gentile avviso per rinnovare ancora.
        // Notare che allo stato attuale 'check:suspend' NON collide con queste notifiche, poichè 'check:suspend'
        // è pensato per sospendere chi non rinnova da anni. Questa notifica invece è pensata per chi non rinnova da un anno.
        // Mandiamo pre-notifica alla segreteria qualche giorno prima (25 gennaio) per importare i movimenti,
        // e anche la sera prima (9 di febbraio).
        $schedule->command('notify:renewal-fee-admin')->yearlyOn(1, 25, '10:30'); // 25 gennaio mattina (relax)
        // Notifica a tutte le utenze che ERANO in regola ma non lo sono più, per invitare a rinnovare.
        // Invio per: 10 di febbraio, mattina.
        //   Se cambi questa data, cambia anche il messaggio in 'app/Mail/RenewalFreePreNotifier.php',
        //   e cambia anche le notifiche sopra.
        $schedule->command('notify:renewal-fee')->yearlyOn(2, 10, '10:30');

        /*
            Il primo gennaio viene aggiornato il bilancio delle sezioni locali
        */
        $schedule->command('section:balance')->cron('0 1 1 1 *');

        //tre volte al giorno :D :D una alle 12:00, una alle 18:00, e una alle 23:00.
        // TODO: If you want to execute this more frequently, make sure to disallow overlapping.
        // TODO: this syntax seems broken
        $schedule->command('paypal:import')->cron('0 12,18,23 * * *');

        // OAuth2 token purging
        // Purge revoked and expired tokens and auth codes.
        // see https://laravel.com/docs/10.x/passport#purging-tokens
        $schedule->command('passport:purge')->hourly();

        // Notifica le utenze in pending che non sono in regola con la tassa d'iscrizione
        $schedule->command('notify:pendings-without-fee-admins')->weeklyOn(1, '10:00');
        $schedule->command('notify:pendings-without-fee')->weeklyOn(1, '10:00');

        // Notifica ai soci la presenza di un'assemblea a cui non hanno segnato la partecipazione
        // o non hanno delegato.
        // Tenta di farlo ogni 3 giorni alle 11:00.
        $schedule->command('notify:assembly')->cron('0 11 */3 * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
