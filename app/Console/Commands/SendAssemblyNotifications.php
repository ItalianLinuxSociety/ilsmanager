<?php

namespace App\Console\Commands;

use App\Mail\AssemblyReminder;
use Illuminate\Console\Command;
use App\Assembly;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Michelf\Markdown;

class SendAssemblyNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:assembly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invia una email di notifica alle utenze attive che devono ancora partecipare o delegare per la prossima assemblea aperta';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Get the first next open Assembly.
        $openAssembly = Assembly::where('status', 'open')->where('date', '>', now())->first();

        if ($openAssembly) {
            // Retrieve active users and send notification
            // Note that "status=active" means "I very probably want to know about this Assembly".
            // This could be a bit "wider" and that's OK. The right to vote is checked later in the Assembly page.
            // TODO: refactor with "WHERE NOT EXISTS" query, to avoid the N+1 performance issue.
            $users = User::where('status', 'active')->get()->filter(function ($user) use ($openAssembly) {
                // Check if you are already participating (with or without a delegate).
                $participation = $openAssembly->users()->whereUser($user)->exists();
                return !$participation;
            });

            foreach ($users as $user) {
                // Context shown in the log.
                $log_data = [
                    'user-id' => $user->id,
                    'user-name' => $user->printableName,
                ];

                // Send notification email
                try {
                    // Log successfully sent e-mail, so this is even easier to be audited later, when asked.
                    Mail::to($user->email)->send(new AssemblyReminder($user, $openAssembly));
                    Log::info("Assembly notified successfully to user", $log_data);
                } catch (\Exception $e) {
                    // Log a human-friendly error, but also a stack trace.
                    Log::error("Assembly NOT notified to user", $log_data);
                    report($e);
                }

                // Wait a random amount of time to don't flood mailservers.
                sleep(4); // chosen by fair dice roll
                          // guaranteed to be random.
                          // https://xkcd.com/221/
            }
        }
    }
}
