<?php

namespace App\Console\Commands;

use DateTime;
use Illuminate\Console\Command;
use PayPalAPIMovementsReader;
use Tests\Unit\PayPalTransactionImportTest;

class MovementPayPalDump extends Command
{
    protected $signature = 'paypal:dump {--date= : The date as Y-m-d} {--transactionid= : The PayPal transaction identifier} {--file=php://stdout : File that captures output}';
    protected $description = 'esporta un singolo movimento PayPal da una data e dal suo identificativo PayPal - utile per creare una unit test in /tests/Unit/paypal';

    public function handle(): int
    {
        $date_raw = $this->option('date');
        $transaction_id = $this->option('transactionid');
        if (! $transaction_id || ! $date_raw) {
            $this->error('Please specify both --date= and --transactionid. This is a limitation of current PayPal API.');

            return 1;
        }

        $date = DateTime::createFromFormat('Y-m-d', $date_raw);
        if ($date === false) {
            $this->error('Invalid date. Format is Y-m-d, like 2000-12-01.');

            return 1;
        }

        $content = '';
        $that = $this;
        PayPalAPIMovementsReader::process([
            'transactionId' => $transaction_id,
            'startDate' => $date,
            'endDate' => $date,
            'itemCallback' => function ($response_data, $response_internal_data) use (&$content, $that, $transaction_id) {
                $id = $response_data['id'];
                $date = $response_data['date'];
                $amount_gro = $response_data['amount_gross'];
                $amount_net = $response_data['amount_net'];
                $fee = $response_data['amount_fee'];
                $notes = $response_data['notes'];
                $notes_xl = $response_data['notes_xl'];

                $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $date);

                // Process only the desired PayPal transaction ID.
                // Note that for interesting reasons, PayPal SDK returns multiple transaction IDs,
                // even if we asked for exactly one ID... so it's better to manually filter
                // the results. lol
                if ($id === $transaction_id) {
                    $that->info("Found PayPal transaction ID {$id}. Remember to manually redact personal data!");
                    $content .= json_encode($response_internal_data['paypal_transaction'], JSON_PRETTY_PRINT) . "\n";
                    $content .= PayPalTransactionImportTest::SEPARATOR_BETWEEN_TRANSACTION_AND_DETAILS . "\n";
                    $content .= json_encode($response_internal_data['paypal_transaction_details'], JSON_PRETTY_PRINT) . "\n";
                    $content .= PayPalTransactionImportTest::SEPARATOR_BETWEEN_DETAILS_AND_RESULTS . "\n";
                    $content .= json_encode($response_data, JSON_PRETTY_PRINT) . "\n";
                } else {
                    $that->warn("Skipped PayPal transaction ID {$id} since you want the ID {$transaction_id}.");
                }
            },
        ]);

        // No transaction, no party.
        if (! $content) {
            $this->error(sprintf(
                "PayPal transaction with date '%s' and identifier '%s' not found! Are you sure this PayPal transaction exists?",
                $date->format('Y-m-d'),
                $transaction_id
            ));

            return 2;
        }

        // No file, no party.
        $file = $this->option('file');
        $wrote = file_put_contents($file, $content);
        if (! $wrote) {
            $this->error("Cannot write into {$file}");

            return 1;
        }

        // Great success!
        return 0;
    }
}
