<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PayPalAPIMovementsReader;
use App\Actions\ImportPayPalMovements as ActionsImportPayPalMovements;

class ImportPayPalMovements extends Command
{
    protected $signature = 'paypal:import {--date-from= : The date as Y-m-d} {--date-to= : The date as Y-m-d}';
    protected $description = 'importa movimenti PayPal';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // The dates are optional.
        $args = [];
        $args['date_start'] = $this->option('date-from');
        $args['date_to'] = $this->option('date-to');

        // TODO: The log messages are not printed in CLI :(
        $action = new ActionsImportPayPalMovements();
        $action->handle($args);
        return 0;
    }
}
