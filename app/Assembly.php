<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Michelf\Markdown;

use Auth;

class Assembly extends Model
{
    use ILSModel;

    /**
     * Get the attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date' => 'datetime',
    ];

    public function getEntityNameSingular(): string {
        return 'Assemblea';
    }

    public function getEntityNamePlural(): string {
        return 'Assemblee';
    }

    public function getObjectShortName()
    {
        return $this->date->format('Y-m-d');
    }

    public function users()
    {
        return $this->hasMany(AssemblyUser::class);
    }

    public function votations()
    {
        return $this->hasMany('App\Votation');
    }

    /**
     * Get my delegations, plus myself.
     * @author Roberto Guido
     */
    public function delegations()
    {
        $ret = new Collection();
        $ret->push(Auth::user());
        $delegating = $this->users()->whereDelegateUserIsMe()->with('user')->get()->pluck('user');
        return $ret->merge($delegating);
    }

    public function getMaxAllowedDelegations(): int
    {
        // TODO: other associations may need a config for this.
        return 3;
    }

    /**
     * Get the title of this Assembly.
     */
    public function getTitleAttribute(): string
    {
        // "Assemblea Y-m-d"
        return sprintf( "%s %s",
            $this->getEntityNameSingular(),
            $this->date->format('Y-m-d')
        );
    }

    /**
     * Get the description ("announce") of this Assembly, rendered in Markdown.
     */
    public function getAnnounceRenderedAttribute(): string
    {
        return Markdown::defaultTransform($this->announce);
    }

    public function isOpen(): bool
    {
        return $this->status === 'open';
    }

    public function isClosed(): bool
    {
        return $this->status === 'closed';
    }

    public function isPending(): bool
    {
        return $this->status === 'pending';
    }
}
