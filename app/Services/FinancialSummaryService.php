<?php

namespace App\Services;

use App\Account;
use App\AccountRow;
use Illuminate\View\View;
use stdClass;
use function bcadd;
use function bcsub;

class FinancialSummaryService
{
    protected int $year;
    protected ?int $selectedUserId;
    protected ?int $selectedSectionId;
    protected ?int $selectedBankId;
    protected bool $includeStartOfYear;

    /**
     * Force an array of AccountIDs.
     * @var array
     */
    protected array $selectedAccountIds;

    public function __construct(array $args)
    {
        $this->year = $args['year'];
        $this->selectedUserId = $args['selectedUserId'] ?? null;
        $this->selectedSectionId = $args['selectedSectionId'] ?? null;
        $this->selectedBankId = $args['selectedBankId'] ?? null;
        $this->includeStartOfYear = $args['includeStartOfYear'] ?? true;

        // Force a map of Account IDs for performance reasons.
        $selectedAccountIdsRaw = $args['selectedAccountIds'] ?? [];
        $this->selectedAccountIds = [];
        foreach ($selectedAccountIdsRaw as $v) {
            $this->selectedAccountIds[$v] = $v;
        }
    }

    public function render(): View
    {
        $zero = '0.00';
        $total_in = $zero;
        $total_out = $zero;
        $total_sum = $zero;

        // Eventually also consider the "start of year".
        $startofyear_in = $zero;
        $startofyear_out = $zero;
        $startofyear_sum = $zero;
        if ($this->includeStartOfYear) {
            $startofyear_in = $this->queryRowsOfStartOfYear()->sum('amount_in');
            $startofyear_out = $this->queryRowsOfStartOfYear()->sum('amount_out');
            $startofyear_sum = bcsub($startofyear_in, $startofyear_out, 2);
        }

        $totalbalance_in = $startofyear_in;
        $totalbalance_out = $startofyear_out;
        $totalbalance_sum = $startofyear_sum;

        $accountRowsQuery = Account::query()
            ->whereNull('parent_id')
            ->orderBy('name', 'asc');

        $summary_rows = [];
        foreach ($accountRowsQuery->get() as $account) {
            $row = $this->collectByAccount($account);
            if ($row) {
                $summary_rows[] = $row;
                $total_in = bcadd($total_in, $row->in, 2);
                $total_out = bcadd($total_out, $row->out, 2);
                $total_sum = bcadd($total_sum, $row->sum, 2);
            }
        }

        $totalbalance_in = bcadd($startofyear_in, $total_in, 2);
        $totalbalance_out = bcadd($startofyear_out, $total_out, 2);
        $totalbalance_sum = bcadd($startofyear_sum, $total_sum, 2);

        return view('partials.financial_summary', [
            'summary_rows' => $summary_rows,
            'total_in' => $total_in,
            'total_out' => $total_out,
            'total_sum' => $total_sum,
            'year' => $this->year,
            'include_start_of_year' => $this->includeStartOfYear,
            'startofyear_in' => $startofyear_in,
            'startofyear_out' => $startofyear_out,
            'startofyear_sum' => $startofyear_sum,
            'totalbalance_in' => $totalbalance_in,
            'totalbalance_out' => $totalbalance_out,
            'totalbalance_sum' => $totalbalance_sum,
        ]);
    }

    private function collectByAccount($account)
    {
        $zero = '0.00';

        $row = new stdClass;
        $row->name = $account->name;
        $row->in = $zero;
        $row->out = $zero;
        $row->sum = $zero;
        $row->children = [];
        $row->in = $this->queryRowsInAccount($account)
            ->sum('amount_in');

        $row->out = $this->queryRowsInAccount($account)
            ->sum('amount_out');

        $row->in = bcadd($row->in, $zero, 2); // bcround() for PHP < 8
        $row->out = bcadd($row->out, $zero, 2); // bcround() for PHP < 8
        $row->sum = bcsub($row->in, $row->out, 2);

        // Allow to easily highlight the selected entries in frontend.
        // For example with "text-muted" CSS class.
        $row->selected = $this->isSelectedAccountId($account->id);

        foreach ($account->children as $child) {
            $c = $this->collectByAccount($child);
            if ($c) {
                $row->children[] = $c;
                $row->in = bcadd($row->in, $c->in, 2);
                $row->out = bcadd($row->out, $c->out, 2);
                $row->sum = bcadd($row->sum, $c->sum, 2);
            }
        }

        // Allow to skip nonsense elements.
        if ($row->sum === $zero && $row->in === $zero && $row->out === $zero && !$row->children) {
            return null;
        }

        return $row;
    }

    /**
     * Query some AccountRow(s) from an Account.
     * @param Account $account
     */
    private function queryRowsInAccount(Account $account)
    {
        return $this->queryRows()
           ->where('account_id', $account->id);
    }

    /**
     * Query some AccountRow(s) related to the start of the year.
     * @return type
     */
    private function queryRowsOfStartOfYear()
    {
        return $this->queryRows(true);
    }

    /**
     * Query some AccountRow(s).
     * @param bool $start_of_year Whenever you want the start of year, or not. Default no.
     */
    private function queryRows(bool $start_of_year = false)
    {
        return AccountRow::query()
            ->whereHas('movement', function ($query) use ($start_of_year) {
                $query->whereYear('date', $this->year);
                if ($start_of_year) {
                    $query->whereNotNull('start_of_year');
                } else {
                    $query->whereNull('start_of_year');
                }
                if ($this->selectedUserId) {
                    $query->where('user_id', $this->selectedUserId);
                }
                if ($this->selectedSectionId) {
                    $query->where('section_id', $this->selectedSectionId);
                }
                if ($this->selectedBankId) {
                    $query->where('bank_id', $this->selectedBankId);
                }
            });
    }

    /**
     * Check whenever an Account ID was selected or not.
     * @param int $id Account ID
     * @return bool
     */
    private function isSelectedAccountId(int $id): bool
    {
        // Assume a safe yes.
        if (!$this->selectedAccountIds) {
            return true;
        }

        // Is this Account among the selecteds?
        return isset($this->selectedAccountIds[$id]);
    }
}
