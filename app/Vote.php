<?php

namespace App;

use App\Votation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * The Vote class is a single Vote (that can have 1 or more options).
 * The Vote can be expressed on a single Votation, by a single User.
 */
class Vote extends Model
{
    protected $casts = [
        'answer' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function votation()
    {
        return $this->belongsTo(Votation::class);
    }

    /**
     * Scope a query to only include votes by a specific User.
     */
    public function scopeWhereUser(Builder $query, User $user): void
    {
        $query->where('user_id', $user->id);
    }

    /**
     * Scope a query to only include votes by a specific User.
     */
    public function scopeWhereVotation(Builder $query, User $user): void
    {
        $query->where('user_id', $user->id);
    }

    /**
     * Scope a query to only include votes by a specific Assembly.
     */
    public function scopeWhereAssembly(Builder $query, Assembly $assembly): void
    {
        $query->whereHas('votation', function ($votation_query) use ($assembly) {
            $votation_query->whereAssembly($assembly);
        });
    }
}
