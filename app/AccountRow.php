<?php

namespace App;

use App\Config;
use App\Account;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * The AccountRow is a logical money transfer.
 * The AccountRow always involve an Account (example: Donations, Fees, etc.)
 * The AccountRow may involve a single User and/or a single Section.
 * The AccountRow may also represent internal money transfer.
 * When we receive a Movement, one or multiple AccountRow(s) are created.
 */
class AccountRow extends Model
{
    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function fee()
    {
//      // Under the hood is technically possible to have multiple Fees, by mistake,
//         but it's still a mistake! not a feature! So we want just one here.
//      return $this->hasMany('App\Fee')->with('user');
        return $this->hasOne('App\Fee')->with('user');
    }

    /**
     * Scope a query to only include bank fees.
     */
    public function scopeOfBankCosts(Builder $query): void
    {
        $query->where('account_id', Account::bankCostsId());
    }

    /**
     * Scope a query to only include positive amounts.
     * Zero amounts are excluded.
     */
    public function scopePositive(Builder $query): void
    {
        $query->where('amount_in', '>', 0.0);
    }

    /**
     * Scope a query to only include negative amounts.
     * Zero amounts are excluded.
     */
    public function scopeNegative(Builder $query): void
    {
        $query->where('amount_out', '>', 0.0);
    }

    public function getAmountAttribute()
    {
        return $this->amount_in - $this->amount_out;
    }

    public function getAmountFormattedAttribute()
    {
        return format_amount( $this->amount );
    }

    /**
     * Set the amount, whatever its sign.
     *
     * @param string|float $amount
     */
    public function setAmountAttribute($amount)
    {
        $positive = $amount >= 0;
        $amount = abs($amount);
        if ($positive) {
            $this->amount_in  = $amount;
            $this->amount_out = 0;
        } else {
            $this->amount_in = 0;
            $this->amount_out = $amount;
        }
    }

    /**
     * Genera una stringa che sia poi leggibile da
     * getFeeYear(). Permette di identificare i movimenti che sono stati a
     * priori identificati come pagamenti di quote di iscrizione, per poi
     * appunto poterle registrare in quanto quote.
     * Genera anche un template generico per i bonifici, omettendo l'anno.
     * P.S. per avere un template con "Cognome Nome", passate un utente
     * volatile con name/surname adeguati.
     */
    public static function createFeeNote($user, ?int $year = null): string
    {
        // Saldo quota Sempronio Caio 2025
        $parts = ["Saldo quota"];
        if ($year) {
            $parts[] = "$year";
        }
        if ($user) {
            $parts[] = $user->printable_name;
        }
        return implode( " ", $parts);
    }

    public static function createDonationNote($name)
    {
        return sprintf('Donazione per %s', $name);
    }

    /**
     * Parse the notes to extract the fee.
     * @return int|null
     */
    public function getFeeYear()
    {
        $matches = [];
        $ok = preg_match_or_throw('/^Saldo quota ([0-9]{4}) .*$/', $this->notes, $matches);
        $year = $matches[1] ?? 0;
        $year = (int)$year;
        if ($year > 3000 || $year < 1000)
        {
            $year = null;
        }
        return $year;
    }

    /**
     * Require Check if we can extract the notes.
     */
    protected function validateFeeYearNotes()
    {
        //The start of year is cumulative and not about a specific fee.
        if (isset($this->movement) && $this->movement->start_of_year) {
            return false;
        }

        if (!$this->getFeeYear()) {
            throw new Exception("Impossibile estrarre il 'Saldo quota ANNO' dalla causale.");
        }
    }

    public function validate()
    {
        if (empty( $this->account_id )) {
            throw new Exception("Compilare il tipo di Movimento Contabile");
        }

        $notes = trim($this->notes);
        if ($notes === '') {
            throw new Exception("Compilare la Causale");
        }

        if ($this->account && $this->account->isAboutMembershipFee()) {
            // Sometime I pick the section by mistake :D
            if ($this->user_id && $this->section_id) {
                throw new Exception("Nelle quote associative non va specificata la sezione locale");
            }

            // This is probably never shown to the frontend for some reasons. Anyway...
            if (!$this->user_id && !$this->movement->start_of_year) {
                throw new Exception("Nelle quote associative va compilata l'utenza");
            }

            // Require that something meaningful is saved in the notes.
            $this->validateFeeYearNotes();
        }

        // TODO: Drop this check. See the mentioned task.
        if (self::is_amount_set($this->amount_in) && self::is_amount_set($this->amount_out)) {
            throw new Exception("Questo AccountRow ha entrambi in/out popolati. Salvalo di nuovo. https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/110");
        }
    }

    /**
     * Check whenever this AccountRow has an Account about a membership fee.
     *
     * @return boolean
     */
    public function hasAccountAboutMembershipFee()
    {
        return $this->account && $this->account->isAboutMembershipFee();
    }

    /**
     * Check if a number is set.
     */
    private static function is_amount_set($v)
    {
        if ($v === null) {
            return false;
        }
        return $v > 0.001 || $v < -0.001;
    }
}
