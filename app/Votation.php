<?php

namespace App;

use App\Vote;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

use Michelf\Markdown;

class Votation extends Model
{
    protected $casts = [
        'options' => 'array',
    ];

    public function assembly()
    {
        return $this->belongsTo('App\Assembly');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    /**
     * Scope a query to only include Votation(s) of a specific Assembly.
     */
    public function scopeWhereAssembly(Builder $query, Assembly $assembly): void
    {
        $query->where('assembly_id', $assembly->id);
    }

    public function getQuestionRenderedAttribute(): string
    {
        return Markdown::defaultTransform($this->question);
    }

    /**
     * Check if a specific user voted on this specific Votation.
     */
    public function hasVoted(User $user): bool
    {
        return $this
            ->votes()
            ->whereUser($user)
            ->exists();
    }

    public function formatResults()
    {
        $final = [];

        foreach($this->options as $option) {
            $final[$option] = (object) [
                'option' => $option,
                'count' => 0,
            ];
        }

        foreach($this->votes as $vote) {
            foreach($vote->answer as $voted) {
                if (isset($final[$voted])) {
                    $final[$voted]->count += 1;
                }
                else {
                    Log::error('Errore in conteggio risultati votazione: ' . $voted . ' non è una opzione valida');
                }
            }
        }

        $final = array_values($final);
        usort($final, function($a, $b) {
            return $a->count <=> $b->count;
        });

        $final = array_reverse($final);

        return $final;
    }

    public function isRunning(): bool
    {
        return $this->status === 'running';
    }

}
