<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ERROR: Cannot use Illuminate\Support\Facades\Route as Route because the name is already in use
//use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if (Auth::check())
        return redirect()->route('home');
    else
        return redirect()->route('login');
});

// Register login, register, password-reset, and other lovely core stuff.
Auth::routes();

Route::get('register/thanks', 'Auth\RegisterController@thanks')->name('register.thanks');
Route::get('register/to/{city_slug}/{section_id}', 'Auth\RegisterController@sectionPreload')->name('register.tosection');

Route::get('thanks/member-fee/paypal', 'App\Http\Controllers\ThanksController@paidMemberFeeViaPayPal')->name('thanks.member-fee.paypal');
Route::get('thanks/donation/paypal', 'App\Http\Controllers\ThanksController@paidDonationViaPayPal')->name('thanks.donation.paypal');

// **** developer stuff ****************************

Route::prefix('dev')->group(function () {

    // Allow to easily preview some internal pages without the need of complicated reproduce steps.
    Route::get('demo/oauth/authorize',
        [App\Http\Controllers\Dev\Demos::class, 'oauthAuthorize']
    )->name('dev.preview.oauth.authorize');

    // We support useful but very-dangerous auto-login workflows,
    // but first, you MUST have:
    //  - environment: local
    //  - env UNSAFE_QUICK_LOGIN_BACKDOOR: 1
    if (!app()->environment('local') || !env('DEV_QUICK_LOGIN_BACKDOOR')) {
        return;
    }

    Route::prefix('login')->group(function () {

        Route::get('admin', function () {
            $user = User::active()->ofRole('admin')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.admin');

        Route::get('member', function () {
            $user = User::active()->ofRole('member')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.member');

        Route::get('referent', function () {
            $user = User::active()->ofRole('referent')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.referent');

        Route::get('pending', function () {
            $user = User::pendingWithPaidFee()->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.pending');

        Route::get('suspended', function () {
            $user = User::ofStatus('suspended')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.suspended');

        Route::get('expelled', function () {
            $user = User::ofStatus('expelled')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.expelled');

        Route::get('dropped', function () {
            $user = User::ofStatus('dropped')->first();
            auth()->login($user);

            return redirect()->route('home');
        })->name('dev.login.dropped');

        Route::get('association', function () {
            $user = User::actLikeMember()
              ->ofType('association')->first();
            auth()->login(User::find($user->id));

            return redirect()->route('home');
        })->name('dev.login.association');

        Route::get('limited', function () {
            $user = User::ofType('limited')->first();
            auth()->login($user);
            return redirect()->route('home');
        })->name('dev.login.limited');
    });
});

Route::middleware(['auth'])->group(function () {
    Route::get('home', 'HomeController@index')->name('home');

    Route::post('user/approve', 'UserController@approve')->name('user.approve');
    Route::get('user/bypass/{id}', 'UserController@bypass')->name('user.bypass');

    Route::get('movement/review', 'MovementController@review')->name('movement.review');
    Route::post('movement/attach/{id}', 'MovementController@attach')->name('movement.attach');
    Route::post('movement/refresh/{id}', 'MovementController@refresh')->name('movement.refresh');
    Route::post('movement/refund/{id}', 'MovementController@refund')->name('movement.refund');
    Route::get('movement/{id}/download/{file}', 'MovementController@download')->name('movement.download');

    Route::get('movement-detailed/export/all/csv', 'MovementDetailedController@exportAllCsv')->name('movement-detailed.export.all.csv');

    Route::get('section/{id}/balance', 'SectionController@balance')->name('section.balance');
    Route::post('section/{id}/exportmembers', 'SectionController@exportmembers')->name('section.exportmembers');

    Route::get('refund/{id}/download/quote/{file}', 'RefundController@downloadQuote')->name('refund.quote.download');
    Route::get('refund/{id}/download/receipt/{file}', 'RefundController@downloadReceipt')->name('refund.receipt.download');

    Route::get('sponsor/logo/{id}', 'SponsorController@logo')->name('sponsor.logo');

    Route::get('doc/download', 'DocumentationController@download')->name('doc.download');
    Route::get('doc/link', 'DocumentationController@link')->name('doc.link');
    Route::get('doc/preview', 'DocumentationController@preview')->name('doc.preview');

    Route::post('assembly/partecipate/{assembly_id}', 'AssemblyController@partecipate')->name('assembly.partecipate');
    Route::post('assembly/unpartecipate/{assembly_id}', 'AssemblyController@unpartecipate')->name('assembly.unpartecipate');
    Route::post('assembly/delegate/{assembly_id}/{delegate_id}', 'AssemblyController@delegate')->name('assembly.delegate');

   // Route::get('bank/list/', 'BankController@list')->name('bank.list_all');
   // Route::get('bank/list/{id}', 'BankController@list')->name('bank.list');

    Route::resource('user', 'UserController');
    Route::resource('volunteer', 'VolunteerController');
    Route::resource('doc', 'DocumentationController');
    Route::resource('config', 'ConfigController');
    Route::resource('bank', 'BankController');
    Route::resource('movement', 'MovementController');
    Route::resource('movement-detailed', 'MovementDetailedController');
    Route::resource('rule', 'RuleController');
    Route::resource('section', 'SectionController');
    Route::resource('receipt', 'ReceiptController');
    Route::resource('refund', 'RefundController');
    Route::resource('assembly', 'AssemblyController');
    Route::resource('votation', 'VotationController');
    Route::resource('vote', 'VoteController');
    Route::resource('sponsor', 'SponsorController');
    Route::resource('gadget-request', 'GadgetRequestController');
});

// Rotte per gli utenti ospiti
Route::get('gadget-request-track/{token}', 'GadgetRequestController@showByToken')->name('gadget-request.show-by-token');
Route::get('gadget-request-thank-you', 'GadgetRequestController@thankYou')->name('gadget-request.thank-you');
Route::get('gadget-request/create', 'GadgetRequestController@create')->name('gadget-request.create');
Route::post('gadget-request/store', 'GadgetRequestController@store')->name('gadget-request.store');

// Useful public pages
Route::get('page/how-to-pay', 'PageController@howToPay')->name('page.how-to-pay');
