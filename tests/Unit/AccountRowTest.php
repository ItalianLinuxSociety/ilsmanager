<?php

namespace Tests\Unit;

use App\Account;
use App\AccountRow;
use App\Config;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AccountRowTest extends TestCase
{
    // This allows SQLite
    use DatabaseMigrations;

    /**
     * @test
     */
    public function amounts_in_out(): void
    {
        $ar = new AccountRow;
        $ar->amount = 100.00;
        $this->assertEquals(number_format(100, 2), number_format($ar->amount_in, 2), 'Account Row 1 in amount');
        $this->assertEquals(number_format(0, 2), number_format($ar->amount_out, 2), 'Account Row 1 out amount');

        $ar = new AccountRow;
        $ar->amount = -100.00;
        $this->assertEquals(number_format(0, 2), number_format($ar->amount_in, 2), 'Account Row 2 in amount');
        $this->assertEquals(number_format(100, 2), number_format($ar->amount_out, 2), 'Account 2 Row out amount');
    }

    /**
     * @test
     */
    public function account_about_membership_fee(): void
    {
        // Create at least one example Account for user fees.
        // This is needed to call this later: Config::feesAccount()->id
        $bank_account_for_user_fees = new Account;
        $bank_account_for_user_fees->name = 'Quota associative test';
        $bank_account_for_user_fees->fees = 1;
        $bank_account_for_user_fees->save();

        $ar = new AccountRow;
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), false, 'AccountRow without Account');

        $ar = new AccountRow;
        $ar->account = new Account;
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), false, 'AccountRow with non-fee Account');

        $ar = new AccountRow;
        $ar->account = \App\Config::feesAccount();
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), true, 'AccountRow with fee Account');

        $bank_account_for_user_fees->delete();
    }
}
