<?php

namespace Tests\Unit;

use App\Assembly;
use Tests\TestCase;

class AssemblyTest extends TestCase
{
    /**
     * Test if the Assembly's description ("announce") can be rendered correctly.
     *
     * @test
     */
    public function test_announce_markdown(): void
    {
        $assembly = new Assembly;
        $assembly->announce = '_asd_';
        $this->assertEquals('<p><em>asd</em></p>', trim($assembly->announceRendered), "Test announce renderer with auto-newlines.");
    }

    /**
     * Test if the Assembly status methods work.
     *
     * @test
     */
    public function test_status(): void
    {
        $assembly = new Assembly;
        $assembly->status = 'open';
        $this->assertTrue($assembly->isOpen());
        $this->assertFalse($assembly->isClosed());
        $this->assertFalse($assembly->isPending());

        $assembly->status = 'closed';
        $this->assertFalse($assembly->isOpen());
        $this->assertTrue($assembly->isClosed());
        $this->assertFalse($assembly->isPending());

        $assembly->status = 'pending';
        $this->assertFalse($assembly->isOpen());
        $this->assertFalse($assembly->isClosed());
        $this->assertTrue($assembly->isPending());
    }
}
