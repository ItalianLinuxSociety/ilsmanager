<?php

namespace Tests\Unit;

use App\AccountRow;
use App\Movement;
use DateTime;
use Exception;
use Tests\TestCase;

class MovementTest extends TestCase
{
    /**
     * Test if the Movement amount matches by 0.01 EUR precision.
     *
     * @test
     */
    public function movement_sum_correct(): void
    {
        $movement = new Movement;
        $movement->amount = 0.10;
        for ($i = 0; $i < 10; $i++) {
            $ar = new AccountRow;
            $ar->amount_in = 0.01;
            $movement->account_rows[] = $ar;
        }
        $this->assertEquals('0.00', number_format($movement->getAccountRowsOutSum(), 2));
        $this->assertEquals('0.10', number_format($movement->getAccountRowsInSum(), 2));
        $this->assertTrue($movement->isAmountMatchingAccountRows(), 'AccountRows amounts matching Movement');

        $ar = new AccountRow;
        $ar->amount_in = 0.01;
        $movement->account_rows[] = $ar;

        $this->assertEquals('0.00', number_format($movement->getAccountRowsOutSum(), 2));
        $this->assertEquals('0.11', number_format($movement->getAccountRowsInSum(), 2));
        $this->assertFalse($movement->isAmountMatchingAccountRows(), 'AccountRows amounts not matching Movement');
    }

    /**
     * Test the Movement year virtual attribute.
     *
     * @test
     */
    public function movement_year(): void
    {
        $movement = new Movement;
        $movement->date = (new DateTime('1999-01-10'))->format('Y-m-d');
        $movement->notes = 'Something in 1999';
        $movement->amount = 1;
        $this->assertEquals(1999, $movement->dateYear, 'Test correct year');

        $crashed = false;
        try {
            $movement = new Movement;
            $year = $movement->dateYear;
        } catch (Exception $e) {
            $crashed = true;
        }
        $this->assertEquals(true, $crashed, 'Test malformed year');
    }
}
