# Installazione e configurazione di OAuth Server di ILS Manager

## Requisiti

- PHP 8.2 o superiore
- Package Laravel Passport (<https://laravel.com/docs/10.x/passport>)

## Installazione

Il service provider di Passport registra la propria directory di migrazione del database, quindi è necessario migrare il
database dopo aver installato il pacchetto. Le migrazioni di Passport creeranno le tabelle di cui l'applicazione ha
bisogno per memorizzare i client e gli access token di OAuth2:

```bash
php artisan migrate
```

Successivamente, è necessario eseguire il comando Artisan `passport:install`. Questo comando creerà le chiavi di
crittografia necessarie per generare token di accesso sicuri. Inoltre, il comando creerà client "personal access" e "
password grant" che verranno utilizzati per generare access tokens:

```bash
php artisan passport:install
```

## Deploy

Per il deploy, è necessario eseguire il comando `passport:keys` per generare le chiavi di crittografia necessarie per
generare token di accesso sicuri:

```bash
php artisan passport:keys
```

vengono creati due file di chiavi di crittografia: `oauth-public.key` e `oauth-private.key` nella directory `storage`.

## Pulizia dei Token Scaduti

Per mantenere il database pulito, è possibile eseguire manualmente il comando `passport:purge` per eliminare i token
scaduti o revocati:

```bash
php artisan passport:purge
```

Questo comando è anche configurato in un scheduled job nella classe `App\Console\Kernel` dell'applicazione per eliminare
automaticamente i token scaduti ogni ora.

```php
$schedule->command('passport:purge')->hourly();
```

## Routes

OAuth Server di ILS Manager espone le seguenti rotte per la gestione delle autorizzazioni OAuth2:

- `/oauth/token` per la generazione di token di accesso OAuth2
- `/oauth/authorize` per l'autorizzazione OAuth2
- `/oauth/revoke` per la revoca di token di accesso OAuth2
- `/oauth/token/refresh` per il rinnovo di token di accesso OAuth2
- `/oauth/tokens` per la visualizzazione dei token di accesso OAuth2
- `/oauth/clients` per la visualizzazione dei client OAuth2
- `/oauth/personal-access-tokens` per la visualizzazione dei token di accesso personali OAuth2

## Deploying Passport sul server di produzione

Per il deploy di Passport sul server di produzione, è necessario eseguire i seguenti passaggi:

1. Eseguire il comando `passport:keys` per generare le chiavi di crittografia necessarie per generare token di accesso
   sicuri

```bash
php artisan passport:keys
```

## Creazione nuovo client

Per creare un nuovo client OAuth, utilizza il comando Artisan `passport:client`, che consente di creare un client con un
nome specifico e un URI di reindirizzamento. Ad esempio:

```bash
php artisan passport:client --password --name=ClientName --redirect_uri=http://client.local.host
```

Se si crea un client il cui nome termina con `-all`, tutti gli utenti del portale, a prescindere dal loro status,
potranno autenticarsi utilizzando questo client. Ad esempio:

```bash
php artisan passport:client --password --name=ClientName-all --redirect_uri=http://client.local.host
```

## Riferimenti

- Laravel Passport: <https://laravel.com/docs/10.x/passport>
- OAuth 2.0: <https://oauth.net/2/>
- RFC 6749: <https://tools.ietf.org/html/rfc6749>
- A Guide To OAuth 2.0 Grants: <https://alexbilbie.github.io/guide-to-oauth-2-grants/>
- Understanding
  OAuth2: <https://web.archive.org/web/20210125202508/https://www.bubblecode.net/en/2016/01/22/understanding-oauth2/>
